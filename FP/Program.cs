﻿using System;
using System.Windows.Forms;

namespace FP
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var ac = new ProgrammAppCtx();
            Application.Run(ac);
        }
    }

    class ProgrammAppCtx : ApplicationContext
    {
        public ProgrammAppCtx()
        {
            var program_context = "Sistema";

            var logger = new Services.Logger(new Services.Logger.Configuration { Backlog = 10 });
            logger.Log("programma avviato", program_context);

            var fp_form = new UI.Forms.frmFP();
            fp_form.FormClosed += Fp_form_FormClosed;
            fp_form.Show();
            logger.LogEvent += (a, b, c, d) => FormatLogMessage(fp_form, a, b, c, d);
            logger.Log("form creato", program_context, Services.Logger.Levels.Debug);

            var ctx = new Data.Context();
            logger.Log("context creato", program_context, Services.Logger.Levels.Debug);

            var main = new Controllers.FP(fp_form, ctx, logger);
            logger.Log("controller principale creato", program_context, Services.Logger.Levels.Debug);
        }

        private void Fp_form_FormClosed(object sender, FormClosedEventArgs e) =>
            ExitThread();

        private void FormatLogMessage(UI.Forms.frmFP form, string message, string context, DateTime timestamp, Services.Logger.Levels level)
        {
            System.Drawing.Color color = System.Drawing.Color.Black;

            switch (level)
            {
                case Services.Logger.Levels.Debug: color = System.Drawing.Color.Blue; break;
                //case Service.Logger.Levels.Info: color = System.Drawing.Color.Black; break;
                case Services.Logger.Levels.Warning: color = System.Drawing.Color.Yellow; break;
                case Services.Logger.Levels.Error: color = System.Drawing.Color.Red; break;
                case Services.Logger.Levels.Fatal: color = System.Drawing.Color.White; break;
            }

            var ts_fff = timestamp.ToString("HH:mm:ss.fff");

            form.Log($"{ts_fff} | {context} | {message}", color);
        }
    }
}
