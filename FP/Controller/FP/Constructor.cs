﻿namespace FP.Controllers
{
    public partial class FP
    {
        public FP(UI.Forms.frmFP frmFp, Data.Context context, Services.ILogger logger)
        {
            _logger = logger;
            _context = context;
            _rnd = new System.Random();
            _stato = new Services.StatoFp.StatoFpService();

            _ui = new UI.Controllers.FpForm(frmFp, _context, _logger, _stato);
            _stato.IncontroAddedEvent = ig => _ui.StoricoIncontriAddItem(ig.Id, ig.Cella, ig.Generato, ig.Mostri.Length);
            _stato.IncontroClearedEvent = _ui.StoricoIncontriClear;

            _ui.GeneraSottotipiRandom = GeneraSottotipiRandom;
            _ui.GenerazioneTest = GenerazioneTest;
            _ui.OggettiTest = i => OggettiTest(i);
        }
    }
}
