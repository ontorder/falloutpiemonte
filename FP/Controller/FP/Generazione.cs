﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace FP.Controllers
{
    partial class FP
    {
        public void GeneraSottotipiRandom(int? seed)
        {
            var rnd = new Random(seed ?? (int)DateTime.Now.Ticks);
            var tot_p = _context.Sottotipi.TerrenoSottotipi.Select(_ => _.ProbabilitàCreazione).Sum();

            foreach (var cella in _context.Mappa.GetCelle())
            {
                var p_sottotipo = rnd.NextDouble() % tot_p;
                Data.Models.TerrenoSottotipo sottotipo = null;

                foreach (var cycle_st in _context.Sottotipi.TerrenoSottotipi)
                {
                    if (p_sottotipo <= cycle_st.ProbabilitàCreazione)
                    {
                        sottotipo = cycle_st;
                        break;
                    }

                    p_sottotipo -= cycle_st.ProbabilitàCreazione;
                }

                cella.Sottotipo = sottotipo.Nome;
                cella.SottotipoId = sottotipo.Id;
            }
        }

        public void GenerazioneTest((int X, int Y) coordCella, int lmg)
        {
            string _magicTitle = "GenerazioneTest";
            var cella = _context.Mappa.GetCella(coordCella.X, coordCella.Y);
            if (cella == null)
            {
                _logger.Log($"cella vuota!", _magicTitle, Services.Logger.Levels.Warning);
                return;
            }

            int modifGS = 0;
            switch (cella.SottotipoId)
            {
                case Repository.TerrenoSottotipo.SottotipoId.Facile: modifGS = -2; break;
                case Repository.TerrenoSottotipo.SottotipoId.Difficile: modifGS = +2; break;
            }

            float modifiedLmg = lmg + modifGS;
            Data.Models.GradoSfida realGs = new Data.Models.GradoSfida { Inverso = false, Grado = lmg };
            realGs += modifGS;

            float pTerreno = _context.Terreni.IdTerreni[cella.TerrenoId].ProbabilitaIncontri;
            float pIncontroSottotipo = 0;

            switch (cella.SottotipoId)
            {
                case Repository.TerrenoSottotipo.SottotipoId.TerrenoDiCaccia: pIncontroSottotipo = 0.5f; break;
                case Repository.TerrenoSottotipo.SottotipoId.Accampamento: pIncontroSottotipo = 1; break;
            }

            float pIncontroStruttura = 0;

            if (cella.Strutture != null)
            {
                foreach (var strutt in cella.Strutture)
                {
                    pIncontroStruttura += _context.Strutture.DicStrutture[strutt.Nome].pIncontriModif;
                }
            }

            var pTot = pTerreno + pIncontroSottotipo + pIncontroStruttura;
            var tiro = _rnd.NextDouble();

            if (tiro >= pTot)
            {
                _logger.Log($"uscito: probabilità {tiro} >= {pTot}", _magicTitle);
                return;
            }
            else
                _logger.Log($"avvenuto: probabilità {tiro} < {pTot}", _magicTitle);

            // credo ci sia da fare un caso anche per il terreno acquatico
            // segno cose che al momento non ricordo bene: terreno accampamento non esclude che ci siano anche altri mostri (ergo pve)
            // TODO la cosa dei gruppi di mostri
            IEnumerable<int> tipi_mostri = null;

            // UNDONE se sottotipo cella è Accampamento allora c'è 100% incontro coloni, predoni (e reietti), supermutanti
            // MA questo in teoria non esclude anche mostri usuali
            if (cella.SottotipoId == Repository.TerrenoSottotipo.SottotipoId.Accampamento)
            {
                // attenzione: terreni_sottotipi.xml
                tipi_mostri = new[]
                {
                    _context.TipiMostri.DicTipiMostriReverse["colono"],
                    _context.TipiMostri.DicTipiMostriReverse["predone"],
                    _context.TipiMostri.DicTipiMostriReverse["supermutante"],
                    _context.TipiMostri.DicTipiMostriReverse["reietto"]
                };
            }
            else
            {
                var mostri_terreni = _context
                    .MostriAmbienti.MostriAmbienti
                    .Where(ma =>
                        (ma.Terreni == null || ma.Terreni.Any(t => t.TerrenoId == cella.TerrenoId))
                        &&
                        (cella.Strutture == null || ma.Strutture == null || ma.Strutture.Any(s => cella.Strutture.Contains(s)))
                    )
                ;

                var tipiMostriList = new List<int>();
                do
                {
                    tipiMostriList = mostri_terreni.Select(mt => mt.MostroTipoId).ToList();
                    int randomTipoId = _rnd.Next(tipiMostriList.Count);
                    int extractedTipoMostro = tipiMostriList[randomTipoId];
                    bool anyMonster = _context.Mostri.Mostri
                        .Where(m => m.Tipo.Any(t => t.Id == extractedTipoMostro))
                        .Any(m => m.GradoSfida <= (realGs - 2))
                    ;
                    if (!anyMonster)
                    {
                        tipiMostriList.RemoveAt(randomTipoId);
                        continue;
                    }
                    tipi_mostri = new[] { extractedTipoMostro };
                } while (tipiMostriList.Count > 0 && tipi_mostri == null);

                if (tipiMostriList.Count == 0)
                {
                    _logger.Log("ATTENZIONE non trovati mostri di livello adeguato per cella selezionata!", _magicTitle, Services.Logger.Levels.Error);
                    return;
                }
            }
            var tutti_mostri = _context.Mostri.Mostri.Where(mostro => mostro.Tipo.Any(tipo_mostro => tipi_mostri.Contains(tipo_mostro.Id)));

            var mostriByGs = tutti_mostri.Where(mostro => mostro.GradoSfida <= realGs).ToArray();
            if (!_context.IncontriGs.DicGsIncontroGs.ContainsKey((float)realGs))
            {
                _logger.Log("ATTENZIONE incontro a questa difficoltà non disponibile", _magicTitle, Services.Logger.Levels.Error);
                return;
            }

            int tot_xp = _context.IncontriGs.DicGsIncontroGs[(float)realGs].Xp;
            var genMostri = new List<Data.Models.Scheda>();

            do
            {
                if (!mostriByGs.Any(mo => _context.IncontriGs.DicGsIncontroGs[(float)mo.GradoSfida].Xp <= tot_xp))
                    break;

                Data.Models.Scheda gen;
                int gen_xp;

                do
                {
                    int rnd_index = _rnd.Next(mostriByGs.Length);
                    gen = mostriByGs[rnd_index];
                    gen_xp = _context.IncontriGs.DicGsIncontroGs[(float)gen.GradoSfida].Xp;
                    // UNDONE processo generazione mostro includerebbe anche calcolo pf per istanza mostro
                } while (gen_xp > tot_xp);
                genMostri.Add(gen);

                tot_xp -= gen_xp;
            }
            while (tot_xp > 0);

            if (genMostri.Count == 0)
                Debugger.Break();

            // sì ma il tesoro viene generato in base al gs dell'incontro, non in base al livello
            // e poi si continua a generare finché avanzano soldi
            // TODO da sottrarre px mostri da px tesoro
            var tesoro = OggettiTest((int)modifiedLmg);

            _stato.AddIncontro(new Services.StatoFp.Model.IncontroGenerato
            {
                Cella = coordCella,
                Generato = DateTime.Now,
                Mostri = genMostri.ToArray(),
                Tesoro = tesoro.Select(t => (t.Oggetto.Id, t.Qta)).ToArray()
            });
        }

        private class VirtualOggettoCount
        {
            public int Qta;
            public Data.Models.Oggetto Oggetto;

            public VirtualOggettoCount(int qta, Data.Models.Oggetto oggetto)
            {
                Qta = qta;
                Oggetto = oggetto;
            }
        }

        private class StatoGenerazione
        {
            public Dictionary<string, VirtualOggettoCount> ListaOggetti = new Dictionary<string, VirtualOggettoCount>();
            public int TappiIniziali;
            public int TappiDisponibili;
            private Repository.Prezzo _prezzi;

            public StatoGenerazione(Repository.Prezzo repoPrezzi, int tappiIniziali)
            {
                TappiIniziali = tappiIniziali;
                TappiDisponibili = TappiIniziali;
                _prezzi = repoPrezzi;
            }

            public void AggiungiOggetto(Data.Models.Oggetto oggetto)
            {
                AggiungiOggetti(oggetto, 1);
            }

            public void AggiungiOggetti(IEnumerable<Data.Models.Oggetto> oggetti)
            {
                foreach (var oggetto in oggetti)
                    AggiungiOggetto(oggetto);
            }

            public void AggiungiOggetti(Data.Models.Oggetto oggetto, int nVolte)
            {
                if (ListaOggetti.ContainsKey(oggetto.Id))
                    ListaOggetti[oggetto.Id].Qta += nVolte;
                else
                    ListaOggetti.Add(oggetto.Id, new VirtualOggettoCount(nVolte, oggetto));

                int valore = _prezzi.DicPrezzi[oggetto.Id].Valore * nVolte;
                TappiDisponibili -= valore;
            }
        }

        private VirtualOggettoCount[] OggettiTest(int lmg)
        {
            int minTesoro = _context.TesoriIncontro.TesoriIncontro.Select(ti => ti.Tappi).Min();

            Data.Models.TesoroIncontro tesoro = _context.TesoriIncontro.TesoriIncontro.Single(ti => ti.LMG == lmg);
            StatoGenerazione stato = new StatoGenerazione(_context.Prezzi, tesoro.Tappi);

        continuaGenerazione:
            {
                var elsTesoro = RiordinaTesori(tesoro.Processamento, tesoro.Tesori);
                foreach (var elTesoro in elsTesoro)
                {
                    GeneraTesoro(stato, elTesoro);
                    if (stato.TappiDisponibili <= 0)
                        break;
                }

                if (stato.TappiDisponibili >= minTesoro)
                {
                    var sortedTi = _context.TesoriIncontro.TesoriIncontro.ToList();
                    sortedTi.Sort((a, b) => a.Tappi > b.Tappi ? 1 : a.Tappi == b.Tappi ? 0 : -1);
                    int id;
                    for (id = 0; sortedTi[id].Tappi <= stato.TappiDisponibili && id < sortedTi.Count; ++id) ;
                    if (id == sortedTi.Count) throw new Exception("errore");
                    tesoro = _context.TesoriIncontro.TesoriIncontro[id == 0 ? 0 : id - 1];
                    lmg = tesoro.LMG;
                    goto continuaGenerazione;
                }
            }

            return stato.ListaOggetti.Values.ToArray();
            //var logs = new List<string>();
            //foreach (var voc in stato.ListaOggetti)
            //{
            //    int valore = _context.Prezzi.DicPrezzi[voc.Value.Oggetto.Id].Valore;
            //    logs.Add($"{voc.Value.Oggetto.Nome} ({valore}) x{voc.Value.Qta}");
            //}
            //logs.Add("avanzo: " + stato.TappiDisponibili);
            //System.Windows.Forms.MessageBox.Show(String.Join("\n", logs));
        }

        private Data.Models.TesoroIncontro.ITesoro[] RiordinaTesori(Data.Models.TesoroIncontroProcessamento processamento, Data.Models.TesoroIncontro.ITesoro[] tesori)
        {
            switch (processamento)
            {
                case Data.Models.TesoroIncontroProcessamento.Casuale:
                {
                    var aTesori = tesori.ToList();
                    var rndTesori = new List<Data.Models.TesoroIncontro.ITesoro>();
                    while (aTesori.Count > 0)
                    {
                        var index = Services.FpRandom.Rnd.Next(0, aTesori.Count);
                        rndTesori.Add(aTesori[index]);
                        aTesori.RemoveAt(index);
                    }
                    return rndTesori.ToArray();
                }

                case Data.Models.TesoroIncontroProcessamento.Sequenziale:
                {
                    return tesori;
                }
            }

            throw new Exception("wtf processamento");
        }

        private void GeneraTesoro(StatoGenerazione stato, Data.Models.TesoroIncontro.ITesoro datoTesoro)
        {
            switch (datoTesoro)
            {
                case Data.Models.TesoroIncontro.TesoroCgo cgo:
                {
                    var gt = _context.GenerazioneTesori.GenerazioneTesori.Single(item => item.Id == cgo.GenerazioneTesoroId);
                    var probs = gt.Elementi.Select(item => item.ProbGenerazione);
                    bool equi_probs = probs.Distinct().Count() == 1;
                    Data.Models.AGenerazioneTesoroElemento elGenerazione = null;
                    var livCache = new Dictionary<Data.Models.GenerazioneTesoroLivello, Data.Models.Oggetto[]>();

                    for (int qta_i = 0; qta_i < cgo.Qta; ++qta_i)
                    {
                        if (cgo.MediaTappi > stato.TappiDisponibili)
                            continue;

                        if (equi_probs)
                        {
                            var index = Services.FpRandom.Rnd.Next(0, gt.Elementi.Length);
                            elGenerazione = gt.Elementi[index];
                        }
                        else
                        {
                            var pTot = probs.Sum();
                            var tiro = Services.FpRandom.Rnd.NextDouble() * pTot;

                            foreach (var tempElemento in gt.Elementi)
                            {
                                tiro -= tempElemento.ProbGenerazione;
                                if (tiro <= 0)
                                {
                                    elGenerazione = tempElemento;
                                    break;
                                }
                            }
                        }

                        if (elGenerazione == null)
                            throw new Exception("qualcosa non ha funzionato");

                        switch (elGenerazione)
                        {
                            case Data.Models.GenerazioneTesoroLivello liv:
                            {
                                Data.Models.Oggetto[] oggetti;

                                if (livCache.ContainsKey(liv))
                                    oggetti = livCache[liv];
                                else
                                {
                                    var gerarchie = _context.GerarchieOggetti.GerarchieOggetti.Where(go => go.Path == liv.GerarchiaPath).Select(g => g.Nome);
                                    oggetti = _context.Oggetti.Oggetti.Where(ogg => ogg.Livello == liv.Livello && gerarchie.Contains(ogg.Discriminator)).ToArray();
                                    livCache.Add(liv, oggetti);
                                }

                                var index = Services.FpRandom.Rnd.Next(0, oggetti.Length);
                                stato.AggiungiOggetto(oggetti[index]);
                                break;
                            }

                            case Data.Models.GenerazioneTesoroOggetto ogg:
                            {
                                stato.AggiungiOggetto(_context.Oggetti.DicOggetti[ogg.OggettoId]);
                                break;
                            }

                            default:
                                throw new Exception("wtf");
                        }
                    }

                    break;
                }

                case Data.Models.TesoroIncontro.TesoroTappi tappi:
                {
                    var tappiGen = Services.Dadi.Tira(tappi.Dadi);
                    var tappis = _context.Oggetti.DicOggetti["tappo"];
                    stato.AggiungiOggetti(tappis, tappiGen);
                    break;
                }

                default:
                    throw new Exception("wtf");
            }
        }
    }
}

// 1) però è strano, tipo i coloni dovrebbero già essere disponibili ovunque
// 2) genera un mostro del tipo corretto con gs pari o inferiore al gs totale: -2 //volendo GS -(da 0 a 4)//
// 3a) il gs sale di 2 ogni volta che il numero raddoppia
// 3b) l'algoritmo dovrebbe gia di per se evitare che il primo genarato sia di livello troppo basso
