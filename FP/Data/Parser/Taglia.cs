﻿using System;

namespace FP.Data.Parser
{
    public static class Taglia
    {
        static public Models.Taglia ParseFromString(string taglia)
        {
            switch (taglia)
            {
                case "minuscola": return Models.Taglia.Minuscola;
                case "piccola": return Models.Taglia.Piccola;
                case "media": return Models.Taglia.Media;
                case "grande": return Models.Taglia.Grande;
                case "enorme": return Models.Taglia.Enorme;
                case "mastodontica": return Models.Taglia.Mastodontica;
                case "gargantuesca": return Models.Taglia.Gargantuesca;
                case "colossale": return Models.Taglia.Colossale;
            }

            throw new FormatException();
        }
    }
}
