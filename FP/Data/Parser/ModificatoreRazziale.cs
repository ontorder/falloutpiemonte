﻿using System;
using System.Linq;
using System.Xml;

namespace FP.Data.Parser
{
    static public class ModificatoreRazziale
    {
        static public (string Abilità, sbyte Bonus)[] ParseFromXml(XmlNode nodoModif)
        {
            return (from XmlNode nodo in nodoModif.ChildNodes select (
                nodo.Attributes["abilita"].Value,
                SByte.Parse(nodo.Attributes["bonus"].Value)
            )).ToArray();
        }
    }
}
