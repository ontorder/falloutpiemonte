﻿using System;
using System.Xml;

namespace FP.Data.Parser
{
    static public class TiriSalvezza
    {
        static public Models.TiriSalvezza FromXml(XmlNode nodoTs)
        {
            return new Models.TiriSalvezza
            {
                Riflessi = SByte.Parse(nodoTs.Attributes["riflessi"].Value),
                Tempra   = SByte.Parse(nodoTs.Attributes["tempra"].Value),
                Volontà  = SByte.Parse(nodoTs.Attributes["volonta"].Value)
            };
        }
    }
}
