﻿using System.Xml;

namespace FP.Data.Parser
{
    static public class Prezzo
    {
        static public Models.Prezzo ParseFromXml(XmlNode nodePrezzo)
        {
            return new Models.Prezzo
            {
                Id = nodePrezzo.Attributes["id"].Value,
                Valore = int.Parse(nodePrezzo.Attributes["valore"].Value)
            };
        }
    }
}
