﻿using System.Collections.Generic;
using System.Xml;

namespace FP.Data.Parser
{
    static public class TerrenoSottotipo
    {
        static public Models.TerrenoSottotipo[] ParseFromXml(string filePath)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(filePath);

            var list = new List<Models.TerrenoSottotipo>();

            foreach(XmlElement nodeSottotipo in doc["terreniSottotipi"].ChildNodes)
            {
                var TerrenoSottotipo = ParseStrutturaFromXml(nodeSottotipo);
                list.Add(TerrenoSottotipo);
            }

            return list.ToArray();
        }

        static public Models.TerrenoSottotipo ParseStrutturaFromXml(XmlNode nodeSottotipo)
        {
            var TerrenoSottotipo = new Models.TerrenoSottotipo();

            TerrenoSottotipo.Descrizione = nodeSottotipo.Attributes["desc"].Value;
            TerrenoSottotipo.Id = int.Parse(nodeSottotipo.Attributes["id"].Value);
            TerrenoSottotipo.Nome = nodeSottotipo.Attributes["nome"].Value;

            TerrenoSottotipo.ProbabilitàCreazione = float.Parse(nodeSottotipo.Attributes["pCreazione"].Value, System.Globalization.CultureInfo.InvariantCulture);

            return TerrenoSottotipo;
        }
    }
}
