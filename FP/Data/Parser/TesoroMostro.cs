﻿using System.Linq;
using System.Xml;

namespace FP.Data.Parser
{
    static public class TesoroMostro
    {
        static public Models.TesoroMostro ParseFromXml(XmlNode nodoTesoro)
        {
            var oggs = (from XmlNode nodeOggetto in nodoTesoro.ChildNodes select (
                nodeOggetto.Attributes["oggetto"].Value,
                int.Parse(nodeOggetto.Attributes["quantita"].Value)
            )).ToArray();

            return new Models.TesoroMostro
            {
                Libero = int.Parse(nodoTesoro.Attributes["libero"].Value),
                Oggetti = oggs
            };
        }
    }
}
