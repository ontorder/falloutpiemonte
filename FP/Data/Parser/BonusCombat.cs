﻿using System;
using System.Xml;

namespace FP.Data.Parser
{
    static public class BonusCombat
    {
        static public (byte BAB, byte BMC, byte DMC) ParseFromXml(XmlNode nodoBonus)
        {
            var attr = nodoBonus.Attributes;
            return (
                Byte.Parse(attr["attacco_base"].Value),
                Byte.Parse(attr["manovra_combattimento"].Value),
                Byte.Parse(attr["difesa_combattimento"].Value)
            );
        }
    }
}
