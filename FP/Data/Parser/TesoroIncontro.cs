﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace FP.Data.Parser
{
    static public class TesoroIncontro
    {
        static public Models.TesoroIncontro ParseFromXml(XmlNode nodoTi)
        {
            var nodesTesoro = nodoTi.SelectNodes("tesoro");
            var ti = new Models.TesoroIncontro();

            ti.LMG = Int32.Parse(nodoTi.Attributes["lmg"].Value);
            ti.Tappi = Int32.Parse(nodoTi.Attributes["tappi"].Value);

            var attr_process = nodoTi.Attributes["processamento"];
            if (attr_process != null)
                ti.Processamento = (Models.TesoroIncontroProcessamento)Enum.Parse(typeof(Models.TesoroIncontroProcessamento), attr_process.Value);
            else
                ti.Processamento = Models.TesoroIncontroProcessamento.Casuale;

            var list = new List<Models.TesoroIncontro.ITesoro>();
            foreach(XmlNode nodoTes in nodesTesoro)
            {
                var tes = ParseTesoroFromXml(nodoTes);
                list.Add(tes);
            }
            ti.Tesori = list.ToArray();

            return ti;
        }

        static public Models.TesoroIncontro.ITesoro ParseTesoroFromXml(XmlNode nodoTes)
        {
            var attr = nodoTes.Attributes;
            Data.Models.TesoroIncontro.ITesoro tesoro;

            switch(attr["discriminator"].Value)
            {
                case "cgo":
                {
                    var t_cgo = new Models.TesoroIncontro.TesoroCgo();

                    t_cgo.Qta = int.Parse(attr["qta"].Value);
                    t_cgo.MediaTappi = int.Parse(attr["media-tappi"].Value);
                    t_cgo.GenerazioneTesoroId = attr["generazione-tesoro"].Value;
                    tesoro = t_cgo;
                    break;
                }

                case "tappi":
                {
                    tesoro = new Models.TesoroIncontro.TesoroTappi
                    {
                        Dadi = Dadi.ParseFromString(attr["qta"].Value)
                    };
                    break;
                }

                default:
                    throw new Exception("tipo tesori_incontro.tesoro sconosciuto");
            }

            return tesoro;
        }
    }
}
