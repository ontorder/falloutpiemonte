﻿namespace FP.Data.Parser
{
    public class GradoSfida
    {
        static public Models.GradoSfida ParseFromString(string gs)
        {
            bool inverso = false;
            int grado = 0;

            if (gs.Contains("/"))
            {
                inverso = true;
                var qd = gs.Split('/');
                grado = int.Parse(qd[1]);
            }
            else
                grado = int.Parse(gs);

            return new Models.GradoSfida
            {
                Grado = grado,
                Inverso = inverso
            };
        }
    }
}
