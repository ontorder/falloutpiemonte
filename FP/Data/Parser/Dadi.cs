﻿using System;

namespace FP.Data.Parser
{
    // TODO potrebbe essere una serie: (AdB+C)+(DdE+F)+...
    static public class Dadi
    {
        static public System.Text.RegularExpressions.Regex DadiRegex = new System.Text.RegularExpressions.Regex(
            "(\\d*)d(\\d+)\\+?(\\d*)",
            System.Text.RegularExpressions.RegexOptions.Compiled | System.Text.RegularExpressions.RegexOptions.ECMAScript
        );

        static public Models.Dadi ParseFromString(string serDadi)
        {
            var dv_match = DadiRegex.Match(serDadi);

            var n_dadi = 0;
            if (!String.IsNullOrEmpty(dv_match.Groups[1].Value))
                n_dadi = int.Parse(dv_match.Groups[1].Value);

            var dim_dadi = 0;
            if (!String.IsNullOrEmpty(dv_match.Groups[2].Value))
                dim_dadi = int.Parse(dv_match.Groups[2].Value);

            var costanti = 0;
            if (!String.IsNullOrEmpty(dv_match.Groups[3].Value))
                dim_dadi = int.Parse(dv_match.Groups[3].Value);

            return new Models.Dadi
            {
                Costanti = costanti,
                Dimensione = dim_dadi,
                Numero = n_dadi
            };
        }
    }
}
