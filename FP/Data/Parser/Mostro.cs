﻿using System.Xml;
using System.Linq;
using System;

namespace FP.Data.Parser
{
    static public class Mostro
    {
        static public Models.SchedaVelocità VelocitàFromXml(XmlNode nodeVelocità)
        {
            var attr = nodeVelocità.Attributes;

            float vBase = 0;
            float vVol  = 0;
            float vScal = 0;
            float vScav = 0;
            float vNuot = 0;

            if (attr["base"] != null)
                vBase = float.Parse(attr["base"].Value, System.Globalization.CultureInfo.InvariantCulture);
            if (attr["volare"] != null)
                vVol = float.Parse(attr["volare"].Value, System.Globalization.CultureInfo.InvariantCulture);
            if (attr["scalare"] != null)
                vScal = float.Parse(attr["scalare"].Value, System.Globalization.CultureInfo.InvariantCulture);
            if (attr["scavare"] != null)
                vScav = float.Parse(attr["scavare"].Value, System.Globalization.CultureInfo.InvariantCulture);
            if (attr["nuotare"] != null)
                vNuot = float.Parse(attr["nuotare"].Value, System.Globalization.CultureInfo.InvariantCulture);

            return new Models.SchedaVelocità
            {
                Base = vBase,
                Nuotare = vNuot,
                Scalare = vScal,
                Scavare = vScav,
                Volare = vVol
            };
        }

        static public Models.Scheda FromXml(XmlNode nodoMostro)
        {
            var mostro = new Models.Scheda();

            mostro.Id = nodoMostro.Attributes["id"].Value;
            mostro.Nome = nodoMostro.Attributes["nome"].Value;
            mostro.Modificatore = nodoMostro.Attributes["modificatore"].Value;
            mostro.GradoSfida = GradoSfida.ParseFromString(nodoMostro.Attributes["grado_sfida"].Value);

            mostro.Regno = (0, nodoMostro["regno"].InnerText);
            mostro.Taglia = Taglia.ParseFromString(nodoMostro["taglia"].InnerText);
            mostro.Tipo = (from XmlNode tipo in nodoMostro["tipi"].ChildNodes select (0, tipo.InnerText)).ToArray();
            mostro.Iniziativa = SByte.Parse(nodoMostro["iniziativa"].InnerText);
            if (nodoMostro["velocita"] != null)
                mostro.Velocità = VelocitàFromXml(nodoMostro["velocita"]);

            var nodoPuntiFerita = nodoMostro["punti_ferita"].Attributes;
            mostro.DadiVita = Dadi.ParseFromString(nodoPuntiFerita["dadi_vita"].Value);
            mostro.ClasseArmatura = Byte.Parse(nodoMostro["classe_armatura"].InnerText);

            if (nodoMostro["riduzioni"] != null)
                mostro.Riduzioni = Riduzioni.ParseFromXml(nodoMostro["riduzioni"]);

            if (nodoMostro["attacchi"] != null)
                mostro.Attacchi = (from XmlNode attacco in nodoMostro["attacchi"].ChildNodes select attacco.InnerText).ToArray();
            if (nodoMostro["attributi"] != null)
                mostro.Attributi = Attributi.FromXml(nodoMostro["attributi"]);
            if (nodoMostro["tiri_salvezza"] != null)
                mostro.TiriSalvezza = TiriSalvezza.FromXml(nodoMostro["tiri_salvezza"]);

            if (nodoMostro["bonus_combat"] != null)
                mostro.BonusCombat = BonusCombat.ParseFromXml(nodoMostro["bonus_combat"]);

            if (nodoMostro["speciali"] != null)
                mostro.Speciali = (from XmlNode speciale in nodoMostro["speciali"].ChildNodes select speciale.InnerText).ToArray();
            if (nodoMostro["talenti"] != null)
                mostro.Talenti = (from XmlNode talento in nodoMostro["talenti"].ChildNodes select talento.InnerText).ToArray();
            if (nodoMostro["abilita"] != null)
                mostro.Abilità = Abilità.ParseFromXml(nodoMostro["abilita"]);
            if (nodoMostro["modificatori_razziali"] != null)
                mostro.ModificatoriRazziali = ModificatoreRazziale.ParseFromXml(nodoMostro["modificatori_razziali"]);
            if (nodoMostro["tesori"] != null)
                mostro.Tesoro = TesoroMostro.ParseFromXml(nodoMostro["tesori"]);

            return mostro;
        }
    }
}
