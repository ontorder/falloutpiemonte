﻿using System;
using System.Linq;
using System.Xml;

namespace FP.Data.Parser
{
    public class GenerazioneTesoro
    {
        static public Data.Models.GenerazioneTesoro ParseFromXml(XmlNode nodeGt){
            var gt = new Models.GenerazioneTesoro();
            gt.Id = nodeGt.Attributes["id"].Value;
            var elementi =
                from XmlElement gtEl
                in nodeGt.ChildNodes
                where gtEl.Name == "elemento"
                select ParseElementoFromXml(gtEl)
            ;
            gt.Elementi = elementi.ToArray();
            return gt;
        }

        static public Data.Models.AGenerazioneTesoroElemento ParseElementoFromXml(XmlElement gtElement)
        {
            var prob = Single.Parse(gtElement.Attributes["pGenerazione"].Value, System.Globalization.CultureInfo.InvariantCulture);
            var elTesoro = gtElement["tesoro"];
            var discrim = (Data.Models.GenerazioneTesoroTipo)Enum.Parse(typeof(Data.Models.GenerazioneTesoroTipo), elTesoro.Attributes["tipo"].Value);

            switch (discrim)
            {
                case Models.GenerazioneTesoroTipo.Generazione:
                {
                    return new Models.GenerazioneTesoroGenerazione();
                }

                case Models.GenerazioneTesoroTipo.Livello:
                {
                    var livello = new Models.GenerazioneTesoroLivello();
                    livello.ProbGenerazione = prob;
                    livello.Livello = Int32.Parse(elTesoro.Attributes["livello"].Value);
                    livello.GerarchiaPath = elTesoro.Attributes["gerarchia-path"].Value;
                    return livello;
                }

                case Models.GenerazioneTesoroTipo.Oggetto:
                {
                    var ogg = new Models.GenerazioneTesoroOggetto();
                    ogg.ProbGenerazione = prob;
                    ogg.OggettoId = elTesoro.Attributes["id"].Value;
                    return ogg;
                }
            }

            throw new Exception("wtf");
        }
    }
}
