﻿using System.Xml;

namespace FP.Data.Parser
{
    static public class Riduzioni
    {
        static public Models.SchedaRiduzioni ParseFromXml(XmlNode nodoRiduzioni)
        {
            var attr = nodoRiduzioni.Attributes;
            return new Models.SchedaRiduzioni{
                Ballistica = RiduzioneFromString(attr["ballistica"].Value),
                Energia    = RiduzioneFromString(attr["energia"].Value),
                Radiazioni = RiduzioneFromString(attr["radiazioni"].Value)
            };
        }

        static public Models.SchedaRiduzioni.Riduzione RiduzioneFromString(string valore)
        {
            if (valore == "-")
                return new Models.SchedaRiduzioni.Riduzione{ Immune = true, Valore = 0 };

            return new Models.SchedaRiduzioni.Riduzione
            {
                Immune = false,
                Valore = System.SByte.Parse(valore)
            };
        }
    }
}
