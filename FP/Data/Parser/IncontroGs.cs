﻿using System.Xml;

namespace FP.Data.Parser
{
    public class IncontroGs
    {
        static public Data.Models.IncontroGs ParseFromXml(XmlNode nodeIncontro)
        {
            var attr = nodeIncontro.Attributes;
            return new Models.IncontroGs
            {
                GS = GradoSfida.ParseFromString(attr["gs"].Value),
                Tappi = int.Parse(attr["tappi"].Value),
                Xp = int.Parse(attr["xp"].Value)
            };
        }
    }
}
