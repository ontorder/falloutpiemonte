﻿using System.Collections.Generic;
using System.Xml;

namespace FP.Data.Parser
{
    static public class MostroAmbienti
    {
        static public Models.MostroAmbienti ParseFromXml(XmlNode nodeMa)
        {
            var ma = new Models.MostroAmbienti();

            ma.MostroTipo = nodeMa.Attributes["tipo-mostro"].Value;
            var terreni = new List<(string, int)>();

            if (nodeMa["terreni"] != null)
            {
                foreach(XmlNode nodeTerreno in nodeMa["terreni"].ChildNodes)
                {
                    if (nodeTerreno.Name != "terreno") continue;
                    terreni.Add((nodeTerreno.InnerText, -1));
                }
                ma.Terreni = terreni.ToArray();
            }

            var strutture = new List<(string, int)>();
            if (nodeMa["strutture"] != null)
            {
                foreach(XmlNode nodeStruttura in nodeMa["strutture"].ChildNodes)
                {
                    if (nodeStruttura.Name != "struttura") continue;
                    strutture.Add((nodeStruttura.InnerText, -1));
                }

                ma.Strutture = strutture.ToArray();
            }
            return ma;
        }
    }
}
