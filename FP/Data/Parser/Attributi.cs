﻿using System;
using System.Xml;

namespace FP.Data.Parser
{
    static public class Attributi
    {
        static public Models.Attributi FromXml(XmlNode nodoAttributi)
        {
            return new Models.Attributi
            {
                Carisma      = new Models.Attributo(SByte.Parse(nodoAttributi.Attributes["carisma"].Value)),
                Costituzione = new Models.Attributo(SByte.Parse(nodoAttributi.Attributes["costituzione"].Value)),
                Destrezza    = new Models.Attributo(SByte.Parse(nodoAttributi.Attributes["destrezza"].Value)),
                Forza        = new Models.Attributo(SByte.Parse(nodoAttributi.Attributes["forza"].Value)),
                Intelligenza = new Models.Attributo(SByte.Parse(nodoAttributi.Attributes["intelligenza"].Value)),
                Saggezza     = new Models.Attributo(SByte.Parse(nodoAttributi.Attributes["saggezza"].Value))
            };
        }
    }
}
