﻿using System.Xml;

namespace FP.Data.Parser
{
    class Fazione
    {
        static public Data.Models.Fazione ParseFromXml(XmlNode nodoFazione)
        {
            var attr = nodoFazione.Attributes;
            return new Data.Models.Fazione
            {
                Colore = attr["colore"].Value,
                Id = int.Parse(attr["id"].Value),
                Nome = attr["nome"].Value
            };
        }
    }
}
