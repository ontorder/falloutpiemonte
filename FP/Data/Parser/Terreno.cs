﻿using System.Collections.Generic;
using System.Xml;

namespace FP.Data.Parser
{
    static public class Terreno
    {
        static public Models.Terreno[] ParseFromXml(string filePath)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(filePath);

            var list = new List<Models.Terreno>();

            foreach(XmlElement nodeTerreno in doc["terreni"].ChildNodes)
            {
                var terreno = ParseTerrenoFromXml(nodeTerreno);
                list.Add(terreno);
            }

            return list.ToArray();
        }

        static public Models.Terreno ParseTerrenoFromXml(XmlNode nodeTerreno)
        {
            var terreno = new Models.Terreno();

            terreno.Id = int.Parse(nodeTerreno.Attributes["id"].Value);
            terreno.Nome = nodeTerreno.Attributes["nome"].Value;
            terreno.ProbabilitaIncontri = float.Parse(nodeTerreno.Attributes["pIncontri"].Value, System.Globalization.CultureInfo.InvariantCulture);
            terreno.Colore = nodeTerreno.Attributes["colore"].Value;
            return terreno;
        }
    }
}

