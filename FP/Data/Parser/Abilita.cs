﻿using System;
using System.Linq;
using System.Xml;

namespace FP.Data.Parser
{
    static public class Abilità
    {
        static public Models.SchedaAbilitàValore[] ParseFromXml(XmlNode nodoAbilità)
        {
            return (
                from XmlNode abilità
                in nodoAbilità.ChildNodes
                select new Models.SchedaAbilitàValore
                {
                    Abilità = Repository.Abilità.NomeAbilità[abilità.Attributes["nome"].Value],
                    Valore = SByte.Parse(abilità.Attributes["valore"].Value),
                    Note = abilità.Attributes["note"]?.Value,
                    Classe = abilità.Attributes["classe"] != null ? bool.Parse(abilità.Attributes["classe"].Value) : true
                }
            ).ToArray();
        }
    }
}
