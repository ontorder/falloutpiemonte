﻿using System.Xml;

namespace FP.Data.Parser
{
    public class GerarchiaOggetto
    {
        static public Models.GerarchiaOggetto ParseFromXml(XmlNode nodeGer)
        {
            var go = new Models.GerarchiaOggetto();
            go.Nome = nodeGer.Attributes["nome"].Value;
            go.Path = nodeGer.Attributes["path"].Value;
            return go;
        }
    }
}
