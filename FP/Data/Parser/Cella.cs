﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace FP.Data.Parser
{
    static public class Cella
    {
        // UNDONE crea driver per evitare di de/serializzare a mano
        static public Models.Cella FromXml(XmlNode nodoCella)
        {
            var parsedCella = new Models.Cella();
            parsedCella.X = Int32.Parse(nodoCella.Attributes["x"].Value);
            parsedCella.Y = Int32.Parse(nodoCella.Attributes["y"].Value);
            parsedCella.Ordinale = Int32.Parse(nodoCella.Attributes["ordinale"].Value);
            parsedCella.Settore = nodoCella.ParentNode.Attributes["nome"].Value;
            parsedCella.SettoreId = Int32.Parse(nodoCella.ParentNode.Attributes["id"].Value);
            parsedCella.SettoreColore = nodoCella.ParentNode.Attributes["colore"].Value;

            if (nodoCella.Attributes["nome"] != null)
                parsedCella.Nome = nodoCella.Attributes["nome"].Value;
            if (nodoCella["note"] != null)
                parsedCella.Note = NoteFromXml(nodoCella["note"]);
            if (nodoCella["sottotipo"] != null)
                parsedCella.Sottotipo = nodoCella["sottotipo"].InnerText;
            if (nodoCella["strutture"] != null)
                parsedCella.Strutture = StruttureFromXml(nodoCella["strutture"]);
            if (nodoCella["terreno"] != null)
                parsedCella.Terreno = nodoCella["terreno"].InnerText;

            var nodeProprietario = nodoCella["proprietario"];
            if (nodeProprietario != null & !String.IsNullOrEmpty(nodeProprietario.InnerText))
                parsedCella.Proprietario = nodeProprietario.InnerText;

            parsedCella.Livello = Int32.Parse(nodoCella.Attributes["livello"].Value);

            return parsedCella;
        }

        static private List<(string, int)> StruttureFromXml(XmlNode nodeStrutture)
        {
            var strutture = new List<(string, int)>();

            foreach(XmlElement nodeStruttura in nodeStrutture.ChildNodes)
                strutture.Add((nodeStruttura.InnerText, 0));

            if (strutture.Count == 0) return null;
            return strutture;
        }

        static private Models.Cella.Nota[] NoteFromXml(XmlNode nodeNote)
        {
            var note = new List<Models.Cella.Nota>();

            foreach(XmlElement nodeNota in nodeNote)
                note.Add(new Models.Cella.Nota{
                    Autore = nodeNota.Attributes["autore"].Value,
                    Testo = nodeNota.InnerText
                });

            return note.ToArray();
        }

        static public XmlNode ToXml(XmlDocument xdoc, Data.Models.Cella cella)
        {
            var nodeCella = xdoc.CreateElement("cella");

            var attrX = xdoc.CreateAttribute("x");
            var attrY = xdoc.CreateAttribute("y");
            var attrOrd = xdoc.CreateAttribute("ordinale");
            var attrNome = xdoc.CreateAttribute("nome");
            var attrLvl = xdoc.CreateAttribute("livello");

            attrX.Value = cella.X.ToString();
            attrY.Value = cella.Y.ToString();
            attrOrd.Value = cella.Ordinale.ToString();
            attrNome.Value = cella.Nome;
            attrLvl.Value = cella.Livello.ToString();

            nodeCella.Attributes.Append(attrX);
            nodeCella.Attributes.Append(attrY);
            nodeCella.Attributes.Append(attrOrd);
            nodeCella.Attributes.Append(attrNome);
            nodeCella.Attributes.Append(attrLvl);

            // ------------------

            var nodeTerr = xdoc.CreateElement("terreno");
            var nodeSubt = xdoc.CreateElement("sottotipo");
            var nodeStrt = xdoc.CreateElement("strutture");
            var nodeNote = xdoc.CreateElement("note");
            var nodeProp = xdoc.CreateElement("proprietario");

            nodeTerr.InnerText = cella.Terreno;
            nodeSubt.InnerText = cella.Sottotipo;

            if (cella.Strutture != null)
            foreach(var strt in cella.Strutture)
            {
                var nodeStruttura = xdoc.CreateElement("struttura");
                nodeStruttura.InnerText = strt.Nome;
                nodeStrt.AppendChild(nodeStruttura);
            }

            if (cella.Note != null)
            foreach(Models.Cella.Nota nota in cella.Note)
            {
                var nodeNota = xdoc.CreateElement("nota");
                nodeNota.InnerText = nota.Testo;

                var attrAutore = xdoc.CreateAttribute("autore");
                attrAutore.Value = nota.Autore;
                nodeNota.Attributes.Append(attrAutore);

                nodeNote.AppendChild(nodeNota);
            }

            if (cella.Proprietario != null)
                nodeProp.InnerText = cella.Proprietario;

            nodeCella.AppendChild(nodeTerr);
            nodeCella.AppendChild(nodeSubt);
            nodeCella.AppendChild(nodeStrt);
            nodeCella.AppendChild(nodeNote);
            nodeCella.AppendChild(nodeProp);

            return nodeCella;
        }
    }
}
