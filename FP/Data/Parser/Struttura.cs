﻿using System.Collections.Generic;
using System.Xml;

namespace FP.Data.Parser
{
    static public class Struttura
    {
        static public Models.Struttura[] ParseFromXml(string filePath)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(filePath);

            var list = new List<Models.Struttura>();

            foreach(XmlElement nodeStruttura in doc["strutture"].ChildNodes)
            {
                var struttura = ParseStrutturaFromXml(nodeStruttura);
                list.Add(struttura);
            }

            return list.ToArray();
        }

        static public Models.Struttura ParseStrutturaFromXml(XmlNode nodeStruttura)
        {
            var struttura = new Models.Struttura();

            struttura.Descrizione = nodeStruttura.Attributes["desc"].Value;
            struttura.Id = int.Parse(nodeStruttura.Attributes["id"].Value);
            struttura.Nome = nodeStruttura.Attributes["nome"].Value;

            if (nodeStruttura.Attributes["pIncontriModif"] != null)
                struttura.pIncontriModif = float.Parse(nodeStruttura.Attributes["pIncontriModif"].Value, System.Globalization.CultureInfo.InvariantCulture);
            else
                struttura.pIncontriModif = 0;

            return struttura;
        }
    }
}
