﻿using System;
using System.Xml;

namespace FP.Data.Parser
{
    static public class Oggetto
    {
        static public Models.Oggetto ParseFromXml(XmlElement elOgg)
        {
            string discrim = elOgg.Attributes["discriminator"].Value;
            Models.Oggetto oggetto;

            switch (discrim)
            {
                case "Rivista"            : oggetto = new Models.Rivista(); break;
                case "Permanente"         : oggetto = new Models.Permanente(); break;
                case "Alcolico"           : oggetto = new Models.Alcolico(); break;
                case "Medicina"           : oggetto = new Models.Medicina(); break;
                case "Cibo"               : oggetto = new Models.Cibo(); break;
                case "Droghe"             : oggetto = new Models.Droghe(); break;
                case "Altro"              : oggetto = new Models.Altro(); break;
                case "Granate"            : oggetto = new Models.Granate(); break;
                case "ArmaMischiaLeggera" : oggetto = new Models.ArmaMischiaLeggera(); break;
                case "ArmaMischiaUnaMano" : oggetto = new Models.ArmaMischiaUnaMano(); break;
                case "ArmaMischiaDueMani" : oggetto = new Models.ArmaMischiaDueMani(); break;
                case "Munizione"          : oggetto = new Models.Munizione(); break;
                case "ArmaDaFuocoLeggera" : oggetto = new Models.ArmaDaFuocoLeggera(); break;
                case "ArmaDaFuocoUnaMano" : oggetto = new Models.ArmaDaFuocoUnaMano(); break;
                case "ArmaDaFuocoDueMani" : oggetto = new Models.ArmaDaFuocoDueMani(); break;
                case "ArmaDaFuocoPesante" : oggetto = new Models.ArmaDaFuocoPesante(); break;
                case "ArmaturaLeggera"    : oggetto = new Models.ArmaturaLeggera(); break;
                case "ArmaturaMedia"      : oggetto = new Models.ArmaturaMedia(); break;
                case "ArmaturaPesante"    : oggetto = new Models.ArmaturaPesante(); break;
                case "ArmaturaAtomica"    : oggetto = new Models.ArmaturaAtomica(); break;
                case "RisorsaArtigianato" : oggetto = new Models.RisorsaArtigianato(); break;
                case "RisorsaCostruzione" : oggetto = new Models.RisorsaCostruzione(); break;

                default:
                    //throw new System.Exception("tipologia oggetto inesistente");
                    oggetto = new Models.Oggetto();
                    break;
            }

            oggetto.Discriminator = discrim;
            oggetto.Id = elOgg.Attributes["id"].Value;
            oggetto.Nome = elOgg.Attributes["nome"].Value;
            oggetto.Descrizione = elOgg.Attributes["descrizione"].Value;
            if (elOgg.Attributes["peso"] != null)
                oggetto.Peso = Single.Parse(elOgg.Attributes["peso"].Value, System.Globalization.CultureInfo.InvariantCulture);

            if (oggetto is Models.Permanente oggperm)
            if (elOgg.Attributes["slot"] != null)
                oggperm.Slot = (Models.Slots)Enum.Parse(typeof(Models.Slots), elOgg.Attributes["slot"].Value);

            oggetto.Livello = Int32.Parse(elOgg.Attributes["livello"].Value);

            return oggetto;
        }
    }
}
