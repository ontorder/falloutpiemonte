﻿namespace FP.Data
{
    public class Context
    {
        public Repository.Mappa Mappa;
        public Repository.Struttura Strutture;
        public Repository.Terreno Terreni;
        public Repository.TerrenoSottotipo Sottotipi;
        public Repository.Mostro Mostri;
        public Repository.RegniMostro Regni;
        public Repository.TipiMostro TipiMostri;
        public Repository.TesoroIncontro TesoriIncontro;
        public Repository.Prezzo Prezzi;
        public Repository.Oggetto Oggetti;
        public Repository.MostroAmbienti MostriAmbienti;
        public Repository.IncontroGs IncontriGs;
        public Repository.Fazione Fazioni;
        public Repository.GerarchiaOggetto GerarchieOggetti;
        public Repository.GenerazioneTesoro GenerazioneTesori;

        public Context()
        {
            Strutture = new Repository.Struttura();
            Strutture.Init("strutture.xml");

            Sottotipi = new Repository.TerrenoSottotipo();
            Sottotipi.Init("terreni_sottotipi.xml");

            Terreni = new Repository.Terreno();
            Terreni.Init("terreni.xml");

            Fazioni = new Repository.Fazione();
            Fazioni.Init("fazioni.xml");

            TipiMostri = new Repository.TipiMostro();
            TipiMostri.Init("tipi_mostri.xml");

            GerarchieOggetti = new Repository.GerarchiaOggetto();
            GerarchieOggetti.Init("gerarchie_oggetti.xml");

            Oggetti = new Repository.Oggetto(this);
            Oggetti.Init("oggetti.xml");

            Regni = new Repository.RegniMostro();
            Regni.Init("regni.xml");

            IncontriGs = new Repository.IncontroGs();
            IncontriGs.Init("incontri.xml");

            Mappa = new Repository.Mappa(this);
            Mappa.FakeInitDriver("mappa.xml");

            GenerazioneTesori = new Repository.GenerazioneTesoro(this);
            GenerazioneTesori.Init("generazione_tesoro.xml");

            TesoriIncontro = new Repository.TesoroIncontro(this);
            TesoriIncontro.Init("tesori_incontro.xml");

            Mostri = new Repository.Mostro(this);
            Mostri.Init("mostri.xml");

            Prezzi = new Repository.Prezzo(this);
            Prezzi.Init("prezzi.xml");

            MostriAmbienti = new Repository.MostroAmbienti(this);
            MostriAmbienti.Init("mostri_ambienti.xml");
        }
    }
}
