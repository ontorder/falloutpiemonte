﻿namespace FP.Data.Models
{
    public class Terreno
    {
        public int Id;
        public string Nome;
        public float ProbabilitaIncontri;
        public string Colore;
    }
}
