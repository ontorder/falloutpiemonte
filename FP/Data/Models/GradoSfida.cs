﻿using System;

namespace FP.Data.Models
{
    public class GradoSfida
    {
        public int Grado;
        public bool Inverso = false;

        public static explicit operator float(GradoSfida gs) => gs.Inverso ? 1f / gs.Grado : gs.Grado;

        public static GradoSfida operator -(GradoSfida l, int v) =>
            l + -v;

        public static GradoSfida operator +(GradoSfida l, int v)
        {
            int modVal = Math.Abs(v);

            if (v>0 && l.Inverso)
            {
                if (l.Grado - v < 2)
                {
                    l.Grado = 2 + modVal - l.Grado;

                    return new GradoSfida
                    {
                        Grado = l.Grado,
                        Inverso = false
                    };
                }
                else
                {
                    return new GradoSfida
                    {
                        Grado = l.Grado + v,
                        Inverso = false
                    };
                }
            }

            if (v<0 && !l.Inverso)
            {
                if (l.Grado + v < 1)
                {
                    l.Grado = 1 + modVal - l.Grado;

                    return new GradoSfida
                    {
                        Grado = l.Grado,
                        Inverso = true
                    };
                }
                else
                {
                    return new GradoSfida
                    {
                        Grado = l.Grado + v,
                        Inverso = false
                    };
                }
            }

            return new GradoSfida
            {
                Grado = l.Grado + modVal,
                Inverso = l.Inverso
            };
        }

        public static bool operator <(GradoSfida l, GradoSfida r)
        {
            if (l.Inverso && !r.Inverso) return true;
            if (!l.Inverso && r.Inverso) return false;
            if (l.Grado < r.Grado) return true;
            return false;
        }

        public static bool operator >(GradoSfida l, GradoSfida r)
        {
            if (l.Inverso && !r.Inverso) return false;
            if (!l.Inverso && r.Inverso) return true;
            if (l.Grado > r.Grado) return true;
            return false;
        }

        public static bool operator <=(GradoSfida l, GradoSfida r)
        {
            if (l.Inverso && !r.Inverso) return false;
            if (!l.Inverso && r.Inverso) return true;
            if (l.Grado > r.Grado) return false;
            return true;
        }

        public static bool operator >=(GradoSfida l, GradoSfida r)
        {
            if (l.Inverso && !r.Inverso) return true;
            if (!l.Inverso && r.Inverso) return false;
            if (l.Grado >= r.Grado) return true;
            return false;
        }

        public static bool operator ==(GradoSfida l, GradoSfida r)
        {
            return l.Grado == r.Grado && l.Inverso == r.Inverso;
        }

        public static bool operator !=(GradoSfida l, GradoSfida r)
        {
            return l.Grado != r.Grado || l.Inverso != r.Inverso;
        }

        public override string ToString() =>
            Inverso ? $"1/{Grado}" : Grado.ToString();
    }
}
