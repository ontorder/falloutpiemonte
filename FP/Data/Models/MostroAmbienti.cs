﻿namespace FP.Data.Models
{
    public class MostroAmbienti
    {
        public string MostroTipo;
        public int MostroTipoId;
        public (string Terreno, int TerrenoId)[] Terreni = null;
        public (string Struttura, int StrutturaId)[] Strutture = null;
    }
}
