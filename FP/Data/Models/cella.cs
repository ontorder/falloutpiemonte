﻿using System.Collections.Generic;

namespace FP.Data.Models
{
    public class Cella
    {
        public int X;
        public int Y;
        public string Settore;
        public int SettoreId;
        public int Ordinale;
        public string SettoreColore;
        public string Nome;
        public string Terreno;
        public int TerrenoId;
        public string Sottotipo;
        public int SottotipoId;
        public List<(string Nome, int Id)> Strutture;
        public Nota[] Note;
        public int Livello;
        public EventoStorico[] EventiStorici;
        public string Proprietario;
        public int ProprietarioId;

        public class Nota
        {
            public string Autore;
            public string Testo;
        }

        public class EventoStorico
        {
            // anno o data?
            public int Anno;
            public string Descrizione;
        }
    }
}
