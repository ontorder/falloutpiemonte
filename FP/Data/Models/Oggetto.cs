﻿namespace FP.Data.Models
{
    //public class ValutaTappo { public int Valore; }

    public /*abstract*/ class Oggetto
    {
        public string Id;
        public string Nome;
        public string Descrizione;
        public float Peso = 0;
        public int Livello;
        public string Discriminator;
    }

    //public abstract class OggettoVirtuale : Oggetto { public int Conto; }
    //public class Tappo : OggettoVirtuale { }

    public abstract class Consumabile : Oggetto { }
    public class Alcolico : Consumabile { }
    public class Medicina : Consumabile { }
    public class Cibo : Consumabile { }
    public class Droghe : Consumabile { }
    public class Altro : Consumabile { }
    public class Granate : Consumabile { }

    public abstract class Arma : Oggetto { }
    public class ArmaMischiaLeggera : Arma { }
    public class ArmaMischiaUnaMano : Arma { }
    public class ArmaMischiaDueMani : Arma { }
    public class Munizione : Oggetto { }
    public class ArmaDaFuocoLeggera : Arma { }
    public class ArmaDaFuocoUnaMano : Arma { }
    public class ArmaDaFuocoDueMani : Arma { }
    public class ArmaDaFuocoPesante : Arma { }

    public abstract class Armatura : Oggetto { }
    public class ArmaturaLeggera : Armatura { }
    public class ArmaturaMedia : Armatura { }
    public class ArmaturaPesante : Armatura { }
    public class ArmaturaAtomica : Armatura { }

    public abstract class Risorsa : Oggetto { }
    public class RisorsaArtigianato : Risorsa { }
    public class RisorsaCostruzione : Risorsa { }

    public enum Slots{
        Cintura,
        Spalle,
        Nessuno,
        Piedi,
        Torace,
        Testa
    }

    public class Permanente : Oggetto {
        public Slots? Slot = null;
    }
    public class Rivista : Permanente { }
}
