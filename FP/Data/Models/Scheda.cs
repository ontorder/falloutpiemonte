﻿using System;

namespace FP.Data.Models
{
    public class Attributo
    {
        public sbyte Valore;
        public sbyte Modificatore
        {
            get
            {
                return (sbyte)Math.Floor((Valore - 10f) / 2f);
            }
        }
        public Attributo(sbyte valore) =>
            Valore = valore;
    }

    public class Attributi
    {
        public Attributo Forza;
        public Attributo Destrezza;
        public Attributo Costituzione;
        public Attributo Intelligenza;
        public Attributo Saggezza;
        public Attributo Carisma;
    }

    public class TiriSalvezza
    {
        public sbyte Tempra;
        public sbyte Volontà;
        public sbyte Riflessi;
    }

    public enum Taglia
    {
        Minuscola,
        Piccola,
        Media,
        Grande,
        Enorme,
        Mastodontica,
        Gargantuesca,
        Colossale
    }

    public class SchedaVelocità
    {
        public float Base;
        public float Volare;
        public float Scalare;
        public float Scavare;
        public float Nuotare;
    }

    public class SchedaRiduzioni
    {
        public class Riduzione
        {
            public sbyte Valore;
            public bool Immune;

            public override string ToString() =>
                Immune ? "-" : Valore.ToString();
        }

        public Riduzione Ballistica;
        public Riduzione Energia;
        public Riduzione Radiazioni;
    }

    public class Scheda
    {
        public string Id;
        public GradoSfida GradoSfida; // 1/3 1/2 1 2 ...
        public string Modificatore; // montone femmina combattimento feroce luminescente prole...
        public string Nome;
        public Taglia Taglia;
        public (int Id, string Nome) Regno; // animale umanoide costrutto vegetale ...
        public (int Id, string Nome)[] Tipo; // becapiela boq bramino colono ...

        public sbyte Iniziativa;
        public SchedaVelocità Velocità = null;
        public Dadi DadiVita;
        public byte ClasseArmatura;
        public SchedaRiduzioni Riduzioni = null;

        public string[] Attacchi;

        public Attributi Attributi = null;
        public TiriSalvezza TiriSalvezza = null;
        public (byte AttaccoBase, byte ManovraCombat, byte DifesaCombat) BonusCombat;

        public string[] Speciali = null;
        public string[] Talenti = null;
        public SchedaAbilitàValore[] Abilità = null;
        public (string Abilità, sbyte Bonus)[] ModificatoriRazziali = null;

        public TesoroMostro Tesoro = null;
    }

    public class SchedaAbilitàValore
    {
        public Abilità Abilità;
        public sbyte Valore;
        public string Note;
        public bool Classe;
    }

    public class TesoroMostro
    {
        public int Libero;
        public (string Oggetto, int Quantità)[] Oggetti;
    }
}
