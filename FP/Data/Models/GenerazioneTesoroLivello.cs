﻿namespace FP.Data.Models
{
    public class GenerazioneTesoroLivello : AGenerazioneTesoroElemento
    {
        // così facendo mi son perso la possibilità di assegnare la probabilità di generazione tra le varie gerarchie (cibo/droghe/risorse)
        public int Livello;
        public string GerarchiaPath;
    }
}
