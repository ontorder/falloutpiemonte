﻿namespace FP.Data.Models
{
    public class Dadi
    {
        public int Numero;
        public int Dimensione;
        public int Costanti;

        public override string ToString() =>
            $"{Numero}d{Dimensione}+{Costanti}";
    }
}
