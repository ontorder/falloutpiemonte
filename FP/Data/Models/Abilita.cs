﻿namespace FP.Data.Models
{
    public class Abilità
    {
        public string Nome;
        public string Descrizione;
        public string Caratteristica; // <--

        public Abilità(string pNome, string pCaratteristica, string pDescrizione = null)
        {
            Nome = pNome;
            Caratteristica = pCaratteristica;
            Descrizione = pDescrizione;
        }
    }
}
