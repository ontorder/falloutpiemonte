﻿namespace FP.Data.Models
{
    /// <summary>oggetto tirato a caso in una gerarchia al di sotto di un certo costo</summary>
    public class GenerazioneTesoroRandomTra : AGenerazioneTesoroElemento
    {
        public string Gerarchia;
        public int TappiMax;
    }
}
