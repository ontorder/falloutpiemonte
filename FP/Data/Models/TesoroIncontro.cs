﻿namespace FP.Data.Models
{
    public partial class TesoroIncontro
    {
        /// <summary>gs incontro</summary>
        public int LMG;
        public ITesoro[] Tesori;
        public TesoroIncontroProcessamento Processamento;
        public int Tappi;
    }
}
