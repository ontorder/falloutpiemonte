﻿namespace FP.Data.Models
{
    public class TerrenoSottotipo
    {
        public int Id;
        public string Nome;
        public string Descrizione;
        public float ProbabilitàCreazione;
    }
}
