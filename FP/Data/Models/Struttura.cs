﻿namespace FP.Data.Models
{
    public class Struttura
    {
        public int Id;
        public string Descrizione;
        public string Nome;
        public float pIncontriModif;
    }
}
