﻿namespace FP.UI.Controllers
{
    static public class Mapper
    {
        static public FpForm.DrawMapView MapUiToController(Forms.frmFP.DrawMapView view)
        {
            switch (view)
            {
                case Forms.frmFP.DrawMapView.Default: return FpForm.DrawMapView.Default;
                case Forms.frmFP.DrawMapView.ProbabilitaIncontro: return FpForm.DrawMapView.ProbabilitaIncontro;
                case Forms.frmFP.DrawMapView.Politica: return FpForm.DrawMapView.Politica;
            }

            throw new System.NotImplementedException();
        }
    }
}
