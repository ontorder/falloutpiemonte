﻿namespace FP.UI.Controllers
{
    public partial class FpForm
    {
        public FpForm(Forms.frmFP frmFp, Data.Context context, FP.Services.ILogger logger, FP.Services.StatoFp.StatoFpService stato)
        {
            _logger = logger;
            _context = context;
            _frmFp = frmFp;
            _mapGen = new Services.MapGeneratorStatus(_context.Mappa);
            _mapGenModifica = new Services.MapGeneratorStatus(_context.Mappa);
            _mapGenPolitica = new Services.MapGeneratorStatus(_context.Mappa);
            _mapDefault = new Services.MapDrawDefaultView(_context.Terreni);
            _mapProbIncontro = new Services.MapProbabilitaIncontroView(_context.Terreni, _context.Strutture);
            _mapPolitica = new Services.MapDrawPolitica(_context.Terreni, _context.Fazioni);
            _stato = stato;
            _storicoIncontroToIncontro = new System.Collections.Generic.Dictionary<int, int>();

            _mapDraw = _mapDefault;

            _frmFp.MappaClick = PbMappa_MouseClick;
            _frmFp.MappaMouseMove = PbMappa_MouseMove;
            _frmFp.MappaSalva = SalvaMappaToolStripMenuItem_Click;
            _frmFp.MappaModificaClick = PbMappaModifica_MouseClick;
            _frmFp.MappaPoliticaClick = PbMappaPolitica_MouseClick;
            _frmFp.RandomSottotipi = GeneraSottotipiRandom_;
            _frmFp.GenerazioneTest = GenerazioneTest_;
            _frmFp.TvMostriSelected = TvMostriSelected;
            _frmFp.OggettiTest = OggettiTest_;
            _frmFp.MappaKeyDown = MappaKeyDown;
            _frmFp.StoricoIncontroSelected = StoricoIncontriSelected;
            _frmFp.StoricoIncontroClear = StoricoIncontriClear;

            _frmFp.MapTools.ScaleChanged = Mappa_ScaleChangedEvent;
            _frmFp.MappaModificaScaleChanged = MappaModifica_ScaleChangedEvent;
            _frmFp.MappaPoliticaScaleChanged = MappaPolitica_ScaleChangedEvent;
            _frmFp.MapSwitchView = view => SwitchMapView(Mapper.MapUiToController(view));

            Mappa_ScaleChangedEvent(_mapGen.Scale);
            MappaModifica_ScaleChangedEvent(_mapGenModifica.Scale);
            MappaPolitica_ScaleChangedEvent(_mapGenPolitica.Scale);

            _frmFp.InitMostri(
                _context.Mostri.Mostri,
                _context.Regni.Regni
            );

            // TODO son abbastanza sicuro che questa non dovrebbe essere chiamata qui
            // è potenzialmente un accesso a file ecc, quindi non deve stare in un costruttore
            _frmFp.InitOggetti(_context.Oggetti.Oggetti, _context.Prezzi.DicPrezzi);
        }
    }
}
