﻿using FP.Services;
using FP.UI.Controls;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace FP.UI.Controllers
{
    partial class FpForm
    {
        public Action<int?> GeneraSottotipiRandom;
        public Action<(int X, int Y), int> GenerazioneTest;
        public Action<int> OggettiTest;

        private void PbMappa_MouseClick(MouseEventArgs e)
        {
            var translated = _mapGen.TranslateCoords(e.X, e.Y);
            var cella = _context.Mappa.GetCella(translated.X, translated.Y);

            if (_mapGen.SelectedCell.HasValue)
            {
                if (_mapGen.SelectedCell.Value.X == translated.X && _mapGen.SelectedCell.Value.Y == translated.Y)
                {
                    _mapGen.SelectedCell = null;
                    cella = null;
                }
                else
                    _mapGen.SelectedCell = new Point(translated.X, translated.Y);
            }
            else
                _mapGen.SelectedCell = new Point(translated.X, translated.Y);

            _frmFp.MapTools.SetSelezionata(cella);
            if (cella == null)
                _logger.Log($"cella selezionata: {translated.X}, {translated.Y}", "Mappa");
            else
                _logger.Log($"cella selezionata: {cella.X}, {cella.Y} {cella.Nome}", "Mappa");

            var bmp = _mapGen.DrawMap_AsService(_mapDraw);
            _frmFp.UpdatePbMappaImage(bmp);
        }

        private void PbMappa_MouseMove(MouseEventArgs e)
        {
            var worldCoord = _mapGen.TranslateCoords(e.X, e.Y);
            var cella = _context.Mappa.GetCella(worldCoord.X, worldCoord.Y);
            _frmFp.MapTools.WriteCoords(worldCoord.X, worldCoord.Y, cella?.SettoreId, cella?.Ordinale, e.X, e.Y);
        }

        private void Mappa_ScaleChangedEvent(int scale)
        {
            _mapGen.Scale = scale;

            var bmp = _mapGen.DrawMap_AsService(_mapDraw);
            _frmFp.UpdatePbMappaImage(bmp);
            _frmFp.UpdatePbMappaDimensions(bmp.Width, bmp.Height);
        }

        private void MappaModifica_ScaleChangedEvent(int scale)
        {
            _mapGenModifica.Scale = scale;

            var bmp = _mapGenModifica.DrawMap_AsService(_mapDefault);
            _frmFp.UpdatePbMappaModificaImage(bmp);
            _frmFp.UpdatePbMappaModificaDimensions(bmp.Width, bmp.Height);
        }

        private void MappaPolitica_ScaleChangedEvent(int scale)
        {
            _mapGenPolitica.Scale = scale;

            var bmp = _mapGenPolitica.DrawMap_AsService(_mapPolitica);
            _frmFp.UpdatePbMappaPoliticaImage(bmp);
            _frmFp.UpdatePbMappaPoliticaDimensions(bmp.Width, bmp.Height);
        }

        private void SalvaMappaToolStripMenuItem_Click()
        {
            _context.Mappa.Salva();
            // bisognerebbe salvare anche terreni/sottotipi/strutture?
        }

        private void PbMappaModifica_MouseClick(MouseEventArgs e)
        {
            var translated = _mapGenModifica.TranslateCoords(e.X, e.Y);
            var cella = _context.Mappa.GetCella(translated.X, translated.Y);

            if (_mapGenModifica.SelectedCell.HasValue)
            {
                if (_mapGenModifica.SelectedCell.Value.X == translated.X && _mapGenModifica.SelectedCell.Value.Y == translated.Y)
                {
                    _mapGenModifica.SelectedCell = null;
                    cella = null;
                }
                else
                    _mapGenModifica.SelectedCell = new Point(translated.X, translated.Y);
            }
            else
                _mapGenModifica.SelectedCell = new Point(translated.X, translated.Y);

            _frmFp.PutModificaCellaSelected(cella);

            var bmp = _mapGenModifica.DrawMap_AsService(_mapDefault);
            _frmFp.UpdatePbMappaModificaImage(bmp);
        }

        private void PbMappaPolitica_MouseClick(MouseEventArgs e)
        {
            var translated = _mapGenPolitica.TranslateCoords(e.X, e.Y);
            var cella = _context.Mappa.GetCella(translated.X, translated.Y);

            if (_mapGenPolitica.SelectedCell.HasValue)
            {
                if (_mapGenPolitica.SelectedCell.Value.X == translated.X && _mapGenPolitica.SelectedCell.Value.Y == translated.Y)
                {
                    _mapGenPolitica.SelectedCell = null;
                    cella = null;
                }
                else
                    _mapGenPolitica.SelectedCell = new Point(translated.X, translated.Y);
            }
            else
                _mapGenPolitica.SelectedCell = new Point(translated.X, translated.Y);

            _frmFp.PutPoliticaCellaSelected(cella);

            var bmp = _mapGenPolitica.DrawMap_AsService(_mapPolitica);
            _frmFp.UpdatePbMappaPoliticaImage(bmp);
        }

        private void GeneraSottotipiRandom_(string seed)
        {
            int? parsed = null;

            if (!String.IsNullOrWhiteSpace(seed))
                parsed = Int32.Parse(seed);

            GeneraSottotipiRandom(parsed);
        }

        private void GenerazioneTest_()
        {
            if (!_mapGen.SelectedCell.HasValue)
            {
                _logger.Log("nessuna cella selezionata", "Generazione", Logger.Levels.Warning);
                return;
            }

            GenerazioneTest((_mapGen.SelectedCell.Value.X ,_mapGen.SelectedCell.Value.Y), _frmFp.GetLmg());
        }

        private void OggettiTest_(int liv) =>
            OggettiTest(liv);

        private void SwitchMapView(DrawMapView mapView)
        {
            switch (mapView)
            {
                case DrawMapView.Default: _mapDraw = _mapDefault; break;
                case DrawMapView.ProbabilitaIncontro: _mapDraw = _mapProbIncontro; break;
                case DrawMapView.Politica: throw new NotImplementedException();
            }

            var bmp = _mapGen.DrawMap_AsService(_mapDraw);
            _frmFp.UpdatePbMappaImage(bmp);
        }

        private void TvMostriSelected(string mostroId)
        {
            var mo = _context.Mostri.IdMostro[mostroId];
            _frmFp.TabMostriSetMostro(mo);
        }

        private void MappaKeyDown(Keys k)
        {
            if (!_mapGen.SelectedCell.HasValue)
                return;

            Point coord = _mapGen.SelectedCell.Value;

            switch (k)
            {
                case Keys.Left: coord.X -= 1; break;
                case Keys.Up: coord.Y += 1; break;
                case Keys.Down: coord.Y -= 1; break;
                case Keys.Right: coord.X += 1; break;
                default: return;
            }
            _mapGen.SelectedCell = coord;
            var cella = _context.Mappa.GetCella(coord.X, coord.Y);

            _frmFp.MapTools.SetSelezionata(cella);
            if (cella == null)
                _logger.Log($"cella selezionata: {coord.X}, {coord.Y}", "Mappa");
            else
                _logger.Log($"cella selezionata: {cella.X}, {cella.Y} {cella.Nome}", "Mappa");

            var bmp = _mapGen.DrawMap_AsService(_mapDraw);
            _frmFp.UpdatePbMappaImage(bmp);
        }

        public void StoricoIncontriAddItem(int incontroId, (int X, int Y) cella, DateTime generato, int countMostri)
        {
            string timestamp = generato.ToString("yyyy-MM-dd HH:mm:ss");
            var storicoIncontroId = _frmFp.lbStoricoGenerazione.Items.Add($"{timestamp} @ {cella.X}/{cella.Y}, lmg {_frmFp.GetLmg()}, mostri: {countMostri}");
            _storicoIncontroToIncontro.Add(storicoIncontroId, incontroId);
            _frmFp.lbStoricoGenerazione.SelectedIndex = storicoIncontroId;
        }

        public void StoricoIncontriClear()
        {
            _storicoIncontroToIncontro.Clear();
            _frmFp.lbStoricoGenerazione.Items.Clear();
            _frmFp.dgvTesoroGenerato.Rows.Clear();
            _frmFp.flpMostriGenerati.Controls.Clear();
        }

        public void StoricoIncontriSelected(int storicoIncontroId)
        {
            int incontroId = _storicoIncontroToIncontro[storicoIncontroId];
            var incontro = _stato.GetIncontro(incontroId);

            _frmFp.flpMostriGenerati.Controls.Clear();

            var mostriById = incontro.Mostri.GroupBy(_ => _.Id);
            foreach (var mostri in mostriById)
            {
                var ucScheda = new ucSchedaBreve(mostri.ToArray());
                _frmFp.flpMostriGenerati.Controls.Add(ucScheda);
            }

            _frmFp.dgvTesoroGenerato.Rows.Clear();
            foreach (var (TesoroId, TesoroQta) in incontro.Tesoro)
            {
                var oggetto = _context.Oggetti.DicOggetti[TesoroId];
                var prezzo = _context.Prezzi.DicPrezzi[oggetto.Id];
                _frmFp.dgvTesoroGenerato.Rows.Add(new object[] { TesoroId, prezzo.Valore, TesoroQta, oggetto.Nome });
            }
        }
    }
}
