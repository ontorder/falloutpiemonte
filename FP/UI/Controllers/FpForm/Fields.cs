﻿using System.Collections.Generic;

namespace FP.UI.Controllers
{
    partial class FpForm
    {
        private Services.MapGeneratorStatus          _mapGen;
        private Services.MapGeneratorStatus          _mapGenModifica;
        private Services.MapGeneratorStatus          _mapGenPolitica;
        private Services.MapDrawDefaultView          _mapDefault;
        private Services.MapProbabilitaIncontroView  _mapProbIncontro;
        private Services.MapDrawPolitica             _mapPolitica;
        private Services.IMapDraw                    _mapDraw;
        private Forms.frmFP                          _frmFp;
        private Data.Context                         _context;
        private FP.Services.ILogger                  _logger;
        private FP.Services.StatoFp.StatoFpService   _stato;

        private Dictionary<int, int> _storicoIncontroToIncontro;
    }
}
