﻿using System.Drawing;
using FP.Data.Models;

namespace FP.UI.Services
{
    public class MapProbabilitaIncontroView : IMapDraw
    {
        private Repository.Terreno _terreni;
        private Repository.Struttura _strutture;

        private Pen _blackPen;

        public MapProbabilitaIncontroView(Repository.Terreno terreni, Repository.Struttura strutture)
        {
            _terreni = terreni;
            _strutture = strutture;
        }

        public void BeginDraw()
        {
            _blackPen = new Pen(Color.Black);
        }

        private float GetProbabilitaIncontro(Cella cella)
        {
            var pTerreno = _terreni.IdTerreni[cella.TerrenoId].ProbabilitaIncontri;
            float pSubType = 0;

            switch (cella.SottotipoId)
            {
                case 5: pSubType = 0.5f; break;
                case 6: pSubType = 1; break;
            }

            float pStrutture = 0;

            foreach(var (Nome, _) in cella.Strutture)
            {
                pStrutture += _strutture.DicStrutture[Nome].pIncontriModif;
            }

            return pTerreno + pSubType + pStrutture;
        }

        public void DrawCell(Graphics graphics, float x, float y, PointF[] points, Cella cella)
        {
            var pTot = GetProbabilitaIncontro(cella);
            Brush fillBrush;

            if (pTot < 1)
                fillBrush = new SolidBrush(Color.FromArgb((int)(255 * pTot), 0, 0));
            else
            {
                var grad = (int)(255*(pTot - 1));
                fillBrush = new SolidBrush(Color.FromArgb(255, grad, 0));
            }

            graphics.DrawPolygon(_blackPen, points);
            graphics.FillPolygon(fillBrush, points);
        }

        public void EndDraw()
        {
            _blackPen.Dispose();
        }
    }
}
