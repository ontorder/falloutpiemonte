﻿using System;

namespace FP.UI.Services
{
    public partial class MapGeneratorStatus
    {
        public (int X, int Y) TranslateCoords(int x, int y)
        {
            float kHrz = HexagonHorizScale * Scale * 0.5f;
            float kVrt = HexagonVertScale * Scale;

            var scaledX = x / kHrz;
            var xTrunc = Math.Truncate(scaledX);
            var xRemind = scaledX - xTrunc;
            var worldXtrunc = xTrunc + _mappa.MinX;
            //var worldX = (x / kHrz) + mappa.MinX;

            var scaledY = y / kVrt - (1 - HexagonVertScale) / 2;
            var yTrunc = Math.Truncate(scaledY);
            var yRemind = 1f - (scaledY - yTrunc);
            var worldYtrunc = _mappa.MaxY - yTrunc;
            int worldY = (int)worldYtrunc;

            var isYodd = Math.Abs(worldYtrunc % 2) == 1;
            var isXodd = Math.Abs(worldXtrunc % 2) == 1;

            var realHeight = 1f / HexagonVertScale;
            var zoneY = 0.25f * realHeight;
            var inTheZone = yRemind < zoneY;

            bool shiftY = false;

            if (inTheZone)
            {
                if ((isXodd && !isYodd) || (!isXodd && isYodd))
                {
                    var pointY = zoneY * xRemind;
                    shiftY = yRemind < pointY;
                }
                else
                {
                    var pointY = zoneY * (1 - xRemind);
                    shiftY = yRemind < pointY;
                }
            }

            if (shiftY)
                --worldY;

            // ---------------

            var worldXint = (int)worldXtrunc;
            var worldYeven = (worldY & 1) == 0;

            if (worldYeven)
            {
                if (isXodd)
                    --worldXint;
            }
            else
            {
                if(!isXodd)
                    --worldXint;
            }

            return (worldXint, worldY);
        }
    }
}
