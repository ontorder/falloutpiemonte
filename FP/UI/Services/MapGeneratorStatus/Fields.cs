﻿using System.Drawing;

namespace FP.UI.Services
{
    public partial class MapGeneratorStatus
    {
        private Repository.Mappa _mappa;
        // potrebbe essere un array?
        public Point? SelectedCell = null;
        //public (int X, int Y)? SelectedCell = null;
        public int Scale = 22;

        static public float HexagonHorizScale;
        static public float HexagonVertScale = 0.75f;
    }
}
