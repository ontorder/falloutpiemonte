﻿using FP.Repository;
using System;

namespace FP.UI.Services
{
    // potrebbe essere in realtà un servizio?
    public partial class MapGeneratorStatus
    {
        public MapGeneratorStatus(Mappa pMappa)
        {
            _mappa = pMappa;
        }

        static MapGeneratorStatus()
        {
            HexagonHorizScale = (float)Math.Sin(Math.PI / 3);
        }
    }
}
