﻿using System;
using System.Drawing;

namespace FP.UI.Services
{
    public partial class MapGeneratorStatus
    {
        public Bitmap DrawMap_AsService(IMapDraw cellDrawer)
        {
            var listCelle = _mappa.GetCelle();

            int bmpWidth = (Scale * (1 + _mappa.MaxX - _mappa.MinX) + 1) / 2;
            int bmpHeight = (int)((Scale * (1 + _mappa.MaxY - _mappa.MinY) + 1) * HexagonVertScale);

            float kHrz = HexagonHorizScale * Scale * 0.5f;
            float kVrt = HexagonVertScale * Scale;

            var bmp = new Bitmap(bmpWidth, bmpHeight);

            using (var gr = Graphics.FromImage(bmp))
            {
                cellDrawer.BeginDraw();

                foreach (var itemcella in listCelle)
                {
                    Brush fillBrush = Brushes.White;
                    Brush textBrush = Brushes.Black;

                    var coord_x = itemcella.X - _mappa.MinX;
                    var coord_y = _mappa.MaxY - itemcella.Y;
                    var scaled_x = kHrz * (1 + coord_x);
                    var scaled_y = kVrt * (0.5f + coord_y);

                    var points = GenerateHexagon(scaled_x, scaled_y);

                    cellDrawer.DrawCell(gr, scaled_x, scaled_y, points, itemcella);
                }

                // penso che dovrei dividere questa cosa in più layout di bitmap
                // così evito di ridisegnare la mappa se non serve
                // ...o magari usare un sistema grafico vero invece di gdi nude e crude
                if (SelectedCell.HasValue)
                {
                    var coord_x = SelectedCell.Value.X - _mappa.MinX;
                    var coord_y = _mappa.MaxY - SelectedCell.Value.Y;
                    var scaled_x = Scale * HexagonHorizScale * (1f + coord_x) / 2f;
                    var scaled_y = Scale * HexagonVertScale * (0.5f + coord_y);

                    var points = GenerateHexagon(scaled_x, scaled_y);

                    using (var selPen = new Pen(Color.Black, 3))
                        gr.DrawPolygon(selPen, points);
                }

                cellDrawer.EndDraw();
            }

            return bmp;
        }

        private PointF[] GenerateHexagon(float scaledX, float scaledY)
        {
            var points = new PointF[6];

            for (int alpha = 0; alpha < 6; ++alpha)
            {
                var angle = Math.PI * alpha / 3.0; // * 60 / 180;
                points[alpha] = new PointF(
                    scaledX + (float)(0.5f * Scale * Math.Sin(angle)),
                    scaledY + (float)(0.5f * Scale * Math.Cos(angle))
                );
            }

            return points;
        }
    }
}
