﻿using FP.Data.Models;
using System.Drawing;

namespace FP.UI.Services
{
    public interface IMapDraw
    {
        void BeginDraw();
        void EndDraw();
        void DrawCell(Graphics graphics, float x, float y, PointF[] points, Cella cella);
    }
}
