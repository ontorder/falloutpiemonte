﻿using System;
using System.Drawing;
using FP.Data.Models;

namespace FP.UI.Services
{
    public class MapDrawDefaultView : IMapDraw
    {
        public string FontFamily = "Arial";
        public float FontSize = 8;

        private Font _stdFont;
        private Pen _blackPen;
        Repository.Terreno _terreni;

        // --------------------

        public MapDrawDefaultView(Repository.Terreno terreni)
        {
            _terreni = terreni;
        }

        public void BeginDraw()
        {
            _stdFont = new Font(FontFamily, FontSize, FontStyle.Bold);
            _blackPen = new Pen(Color.Black);
        }

        public void DrawCell(Graphics graphics, float x, float y, PointF[] points, Cella cella)
        {
            Brush fillBrush = Brushes.White;
            Brush textBrush;

            if (cella.X == 0 && cella.Y == 0)
            {
                fillBrush = Brushes.Black;
                textBrush = Brushes.White;
            }
            else
            {
                switch (cella.SettoreColore)
                {
                    case "rosso" : textBrush = Brushes.Red; break;
                    case "verde" : textBrush = Brushes.Green; break;
                    case "blu"   : textBrush = Brushes.Blue; break;
                    case "viola" : textBrush = Brushes.Violet; break;
                    case "fucsia": textBrush = Brushes.Fuchsia; break;
                    default: textBrush = BrushFromColor(cella.SettoreColore); break;
                }

                if (!String.IsNullOrEmpty(cella.Terreno))
                    fillBrush = BrushFromColor(_terreni.NomeTerreni[cella.Terreno].Colore);
            }

            graphics.FillPolygon(fillBrush, points);
            graphics.DrawPolygon(_blackPen, points);
            graphics.DrawString(cella.Ordinale.ToString(), _stdFont, textBrush, x - 9, y - 7);
        }

        public void EndDraw()
        {
            _stdFont.Dispose();
            _blackPen.Dispose();
        }

        private Brush BrushFromColor(string color)
        {
            string r_ser, g_ser, b_ser;
            int r_par, g_par, b_par;

            if (color.Length == 6)
            {
                r_ser = String.Concat(color[0], color[1]);
                g_ser = String.Concat(color[2], color[3]);
                b_ser = String.Concat(color[4], color[5]);
            }

            else

            if (color.Length == 3)
            {
                r_ser = String.Concat(color[0], color[0]);
                g_ser = String.Concat(color[1], color[1]);
                b_ser = String.Concat(color[2], color[2]);
            }

            else

            throw new ArgumentException("colore dev'essere un nome o RRGGBB o RGB");

            r_par = int.Parse(r_ser, System.Globalization.NumberStyles.HexNumber);
            g_par = int.Parse(g_ser, System.Globalization.NumberStyles.HexNumber);
            b_par = int.Parse(b_ser, System.Globalization.NumberStyles.HexNumber);

            return new SolidBrush(Color.FromArgb(r_par, g_par, b_par));
        }
    }
}
