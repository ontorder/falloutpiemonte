﻿using FP.UI.Controls;

namespace FP.UI.Forms
{
    partial class frmFP
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pbMappa = new System.Windows.Forms.PictureBox();
            this.menuStripFp = new System.Windows.Forms.MenuStrip();
            this.vuotoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tstxAutore = new System.Windows.Forms.ToolStripTextBox();
            this.salvaMappaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generaSottotipiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tssTxRandomSottotipiSeed = new System.Windows.Forms.ToolStripTextBox();
            this.generaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cercaNelleNoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDivisore = new System.Windows.Forms.ToolStripMenuItem();
            this.livelloDelGruppoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tstxLmg = new System.Windows.Forms.ToolStripTextBox();
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabMappa = new System.Windows.Forms.TabPage();
            this.pnlMappa = new System.Windows.Forms.Panel();
            this.MapTools = new FP.UI.Controls.ucMapTools();
            this.popupMapTools = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.dockingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sinistraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.destraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.volanteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vistaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mappaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.probabilitàIncontroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.politicaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tbMappaFocus = new System.Windows.Forms.TextBox();
            this.tabModifica = new System.Windows.Forms.TabPage();
            this.splitModifica = new System.Windows.Forms.SplitContainer();
            this.pnlModificaMappa = new System.Windows.Forms.Panel();
            this.pbMappaModifica = new System.Windows.Forms.PictureBox();
            this.lbEventiStorici = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnSalvaNota = new System.Windows.Forms.Button();
            this.cbModificaSottotipo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txModificaNota = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lbModificaStrutture = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbModificaTerreno = new System.Windows.Forms.ComboBox();
            this.tbModificaZoom = new System.Windows.Forms.TrackBar();
            this.tabMostri = new System.Windows.Forms.TabPage();
            this.scMostri = new System.Windows.Forms.SplitContainer();
            this.tcMostri = new System.Windows.Forms.TabControl();
            this.tpMostriLista = new System.Windows.Forms.TabPage();
            this.scElenco = new System.Windows.Forms.SplitContainer();
            this.tbMostriCerca = new System.Windows.Forms.TextBox();
            this.tvMostri = new System.Windows.Forms.TreeView();
            this.tpMostriFiltri = new System.Windows.Forms.TabPage();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.ucMostro = new FP.UI.Controls.ucMostro();
            this.tabConsole = new System.Windows.Forms.TabPage();
            this.lbConsoleMessages = new System.Windows.Forms.ListBox();
            this.tabGenerazione = new System.Windows.Forms.TabPage();
            this.tcGenerazioneIncontro = new System.Windows.Forms.TabControl();
            this.tpOpzioniGenerazione = new System.Windows.Forms.TabPage();
            this.scOpzioniGenerazioneIncontro = new System.Windows.Forms.SplitContainer();
            this.btnPulisciStoricoGenerazione = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.lbStoricoGenerazione = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGeneraTest = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.tpElencoMostriGenerati = new System.Windows.Forms.TabPage();
            this.flpMostriGenerati = new System.Windows.Forms.FlowLayoutPanel();
            this.tpTesoroGenerato = new System.Windows.Forms.TabPage();
            this.dgvTesoroGenerato = new System.Windows.Forms.DataGridView();
            this.colId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPrezzo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colQta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPolitica = new System.Windows.Forms.TabPage();
            this.pnlPolitica = new System.Windows.Forms.Panel();
            this.pbPolitica = new System.Windows.Forms.PictureBox();
            this.tpOggetti = new System.Windows.Forms.TabPage();
            this.scOggetti = new System.Windows.Forms.SplitContainer();
            this.tvOggettiGerarchia = new System.Windows.Forms.TreeView();
            this.txOggettiSearch = new System.Windows.Forms.TextBox();
            this.chkOggettiFilterDesc = new System.Windows.Forms.CheckBox();
            this.chkOggettiFilterNome = new System.Windows.Forms.CheckBox();
            this.dgvOggetti = new System.Windows.Forms.DataGridView();
            this.ssFp = new System.Windows.Forms.StatusStrip();
            this.tssMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.dgvOggettiId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvOggettiTipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvOggettiNome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvOggettiSlot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvOggettirezzo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvOggettiDescrizione = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.pbMappa)).BeginInit();
            this.menuStripFp.SuspendLayout();
            this.tabMain.SuspendLayout();
            this.tabMappa.SuspendLayout();
            this.pnlMappa.SuspendLayout();
            this.popupMapTools.SuspendLayout();
            this.tabModifica.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitModifica)).BeginInit();
            this.splitModifica.Panel1.SuspendLayout();
            this.splitModifica.Panel2.SuspendLayout();
            this.splitModifica.SuspendLayout();
            this.pnlModificaMappa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbMappaModifica)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbModificaZoom)).BeginInit();
            this.tabMostri.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scMostri)).BeginInit();
            this.scMostri.Panel1.SuspendLayout();
            this.scMostri.Panel2.SuspendLayout();
            this.scMostri.SuspendLayout();
            this.tcMostri.SuspendLayout();
            this.tpMostriLista.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scElenco)).BeginInit();
            this.scElenco.Panel1.SuspendLayout();
            this.scElenco.Panel2.SuspendLayout();
            this.scElenco.SuspendLayout();
            this.tpMostriFiltri.SuspendLayout();
            this.tabConsole.SuspendLayout();
            this.tabGenerazione.SuspendLayout();
            this.tcGenerazioneIncontro.SuspendLayout();
            this.tpOpzioniGenerazione.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scOpzioniGenerazioneIncontro)).BeginInit();
            this.scOpzioniGenerazioneIncontro.Panel1.SuspendLayout();
            this.scOpzioniGenerazioneIncontro.Panel2.SuspendLayout();
            this.scOpzioniGenerazioneIncontro.SuspendLayout();
            this.tpElencoMostriGenerati.SuspendLayout();
            this.tpTesoroGenerato.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTesoroGenerato)).BeginInit();
            this.tabPolitica.SuspendLayout();
            this.pnlPolitica.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPolitica)).BeginInit();
            this.tpOggetti.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scOggetti)).BeginInit();
            this.scOggetti.Panel1.SuspendLayout();
            this.scOggetti.Panel2.SuspendLayout();
            this.scOggetti.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOggetti)).BeginInit();
            this.ssFp.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbMappa
            // 
            this.pbMappa.BackColor = System.Drawing.Color.White;
            this.pbMappa.Location = new System.Drawing.Point(0, 0);
            this.pbMappa.Name = "pbMappa";
            this.pbMappa.Size = new System.Drawing.Size(125, 93);
            this.pbMappa.TabIndex = 1;
            this.pbMappa.TabStop = false;
            this.pbMappa.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PbMappa_MouseClick);
            this.pbMappa.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PbMappa_MouseMove);
            // 
            // menuStripFp
            // 
            this.menuStripFp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vuotoToolStripMenuItem,
            this.aboutToolStripMenuItem,
            this.tsmDivisore,
            this.livelloDelGruppoToolStripMenuItem,
            this.tstxLmg});
            this.menuStripFp.Location = new System.Drawing.Point(0, 0);
            this.menuStripFp.Name = "menuStripFp";
            this.menuStripFp.Size = new System.Drawing.Size(800, 27);
            this.menuStripFp.TabIndex = 2;
            this.menuStripFp.Text = "menuStrip1";
            // 
            // vuotoToolStripMenuItem
            // 
            this.vuotoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.autoreToolStripMenuItem,
            this.salvaMappaToolStripMenuItem,
            this.generaSottotipiToolStripMenuItem,
            this.cercaNelleNoteToolStripMenuItem});
            this.vuotoToolStripMenuItem.Name = "vuotoToolStripMenuItem";
            this.vuotoToolStripMenuItem.Size = new System.Drawing.Size(50, 23);
            this.vuotoToolStripMenuItem.Text = "Menu";
            // 
            // autoreToolStripMenuItem
            // 
            this.autoreToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tstxAutore});
            this.autoreToolStripMenuItem.Name = "autoreToolStripMenuItem";
            this.autoreToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.autoreToolStripMenuItem.Text = "Autore";
            // 
            // tstxAutore
            // 
            this.tstxAutore.Name = "tstxAutore";
            this.tstxAutore.Size = new System.Drawing.Size(100, 23);
            this.tstxAutore.Text = "default";
            // 
            // salvaMappaToolStripMenuItem
            // 
            this.salvaMappaToolStripMenuItem.Name = "salvaMappaToolStripMenuItem";
            this.salvaMappaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.salvaMappaToolStripMenuItem.Text = "Salva mappa";
            this.salvaMappaToolStripMenuItem.Click += new System.EventHandler(this.SalvaMappaToolStripMenuItem_Click);
            // 
            // generaSottotipiToolStripMenuItem
            // 
            this.generaSottotipiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1,
            this.tssTxRandomSottotipiSeed,
            this.generaToolStripMenuItem});
            this.generaSottotipiToolStripMenuItem.Name = "generaSottotipiToolStripMenuItem";
            this.generaSottotipiToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.generaSottotipiToolStripMenuItem.Text = "Genera sottotipi";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(160, 22);
            this.toolStripTextBox1.Text = "Seed:";
            // 
            // tssTxRandomSottotipiSeed
            // 
            this.tssTxRandomSottotipiSeed.Name = "tssTxRandomSottotipiSeed";
            this.tssTxRandomSottotipiSeed.Size = new System.Drawing.Size(100, 23);
            this.tssTxRandomSottotipiSeed.ToolTipText = "numero intero; vuoto: DateTime";
            // 
            // generaToolStripMenuItem
            // 
            this.generaToolStripMenuItem.Name = "generaToolStripMenuItem";
            this.generaToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.generaToolStripMenuItem.Text = "Genera";
            this.generaToolStripMenuItem.Click += new System.EventHandler(this.generaToolStripMenuItem_Click);
            // 
            // cercaNelleNoteToolStripMenuItem
            // 
            this.cercaNelleNoteToolStripMenuItem.Name = "cercaNelleNoteToolStripMenuItem";
            this.cercaNelleNoteToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.cercaNelleNoteToolStripMenuItem.Text = "Cerca nelle note";
            this.cercaNelleNoteToolStripMenuItem.Click += new System.EventHandler(this.cercaNelleNoteToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 23);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
            // 
            // tsmDivisore
            // 
            this.tsmDivisore.Name = "tsmDivisore";
            this.tsmDivisore.Size = new System.Drawing.Size(22, 23);
            this.tsmDivisore.Text = "|";
            // 
            // livelloDelGruppoToolStripMenuItem
            // 
            this.livelloDelGruppoToolStripMenuItem.Name = "livelloDelGruppoToolStripMenuItem";
            this.livelloDelGruppoToolStripMenuItem.Size = new System.Drawing.Size(114, 23);
            this.livelloDelGruppoToolStripMenuItem.Text = "Livello del gruppo";
            // 
            // tstxLmg
            // 
            this.tstxLmg.AutoSize = false;
            this.tstxLmg.MaxLength = 2;
            this.tstxLmg.Name = "tstxLmg";
            this.tstxLmg.Size = new System.Drawing.Size(25, 23);
            this.tstxLmg.Text = "4";
            this.tstxLmg.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tstxLmg_KeyPress);
            // 
            // tabMain
            // 
            this.tabMain.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tabMain.Controls.Add(this.tabMappa);
            this.tabMain.Controls.Add(this.tabModifica);
            this.tabMain.Controls.Add(this.tabMostri);
            this.tabMain.Controls.Add(this.tabConsole);
            this.tabMain.Controls.Add(this.tabGenerazione);
            this.tabMain.Controls.Add(this.tabPolitica);
            this.tabMain.Controls.Add(this.tpOggetti);
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.Location = new System.Drawing.Point(0, 27);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(800, 401);
            this.tabMain.TabIndex = 3;
            // 
            // tabMappa
            // 
            this.tabMappa.BackColor = System.Drawing.Color.White;
            this.tabMappa.Controls.Add(this.pnlMappa);
            this.tabMappa.Location = new System.Drawing.Point(4, 25);
            this.tabMappa.Name = "tabMappa";
            this.tabMappa.Padding = new System.Windows.Forms.Padding(3);
            this.tabMappa.Size = new System.Drawing.Size(792, 372);
            this.tabMappa.TabIndex = 0;
            this.tabMappa.Text = "Mappa";
            // 
            // pnlMappa
            // 
            this.pnlMappa.AutoScroll = true;
            this.pnlMappa.Controls.Add(this.MapTools);
            this.pnlMappa.Controls.Add(this.pbMappa);
            this.pnlMappa.Controls.Add(this.tbMappaFocus);
            this.pnlMappa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMappa.Location = new System.Drawing.Point(3, 3);
            this.pnlMappa.Name = "pnlMappa";
            this.pnlMappa.Size = new System.Drawing.Size(786, 366);
            this.pnlMappa.TabIndex = 3;
            // 
            // MapTools
            // 
            this.MapTools.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MapTools.ContextMenuStrip = this.popupMapTools;
            this.MapTools.Location = new System.Drawing.Point(527, 54);
            this.MapTools.Name = "MapTools";
            this.MapTools.Size = new System.Drawing.Size(189, 249);
            this.MapTools.TabIndex = 2;
            // 
            // popupMapTools
            // 
            this.popupMapTools.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dockingToolStripMenuItem,
            this.vistaToolStripMenuItem});
            this.popupMapTools.Name = "popupMapTools";
            this.popupMapTools.Size = new System.Drawing.Size(119, 48);
            // 
            // dockingToolStripMenuItem
            // 
            this.dockingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sinistraToolStripMenuItem,
            this.destraToolStripMenuItem,
            this.volanteToolStripMenuItem});
            this.dockingToolStripMenuItem.Name = "dockingToolStripMenuItem";
            this.dockingToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.dockingToolStripMenuItem.Text = "Docking";
            // 
            // sinistraToolStripMenuItem
            // 
            this.sinistraToolStripMenuItem.Name = "sinistraToolStripMenuItem";
            this.sinistraToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.sinistraToolStripMenuItem.Text = "Sinistra";
            this.sinistraToolStripMenuItem.Click += new System.EventHandler(this.sinistraToolStripMenuItem_Click);
            // 
            // destraToolStripMenuItem
            // 
            this.destraToolStripMenuItem.Name = "destraToolStripMenuItem";
            this.destraToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.destraToolStripMenuItem.Text = "Destra";
            this.destraToolStripMenuItem.Click += new System.EventHandler(this.destraToolStripMenuItem_Click);
            // 
            // volanteToolStripMenuItem
            // 
            this.volanteToolStripMenuItem.Name = "volanteToolStripMenuItem";
            this.volanteToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.volanteToolStripMenuItem.Text = "Volante";
            this.volanteToolStripMenuItem.Click += new System.EventHandler(this.volanteToolStripMenuItem_Click);
            // 
            // vistaToolStripMenuItem
            // 
            this.vistaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mappaToolStripMenuItem,
            this.probabilitàIncontroToolStripMenuItem,
            this.politicaToolStripMenuItem});
            this.vistaToolStripMenuItem.Name = "vistaToolStripMenuItem";
            this.vistaToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.vistaToolStripMenuItem.Text = "Vista";
            // 
            // mappaToolStripMenuItem
            // 
            this.mappaToolStripMenuItem.Name = "mappaToolStripMenuItem";
            this.mappaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.mappaToolStripMenuItem.Text = "Default";
            this.mappaToolStripMenuItem.Click += new System.EventHandler(this.mappaToolStripMenuItem_Click);
            // 
            // probabilitàIncontroToolStripMenuItem
            // 
            this.probabilitàIncontroToolStripMenuItem.Name = "probabilitàIncontroToolStripMenuItem";
            this.probabilitàIncontroToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.probabilitàIncontroToolStripMenuItem.Text = "Probabilità Incontro";
            this.probabilitàIncontroToolStripMenuItem.Click += new System.EventHandler(this.probabilitàIncontroToolStripMenuItem_Click);
            // 
            // politicaToolStripMenuItem
            // 
            this.politicaToolStripMenuItem.Name = "politicaToolStripMenuItem";
            this.politicaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.politicaToolStripMenuItem.Text = "Politica";
            // 
            // tbMappaFocus
            // 
            this.tbMappaFocus.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbMappaFocus.Location = new System.Drawing.Point(5, 3);
            this.tbMappaFocus.Name = "tbMappaFocus";
            this.tbMappaFocus.Size = new System.Drawing.Size(41, 13);
            this.tbMappaFocus.TabIndex = 3;
            this.tbMappaFocus.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbMappaFocus_KeyDown);
            // 
            // tabModifica
            // 
            this.tabModifica.Controls.Add(this.splitModifica);
            this.tabModifica.Location = new System.Drawing.Point(4, 25);
            this.tabModifica.Name = "tabModifica";
            this.tabModifica.Padding = new System.Windows.Forms.Padding(3);
            this.tabModifica.Size = new System.Drawing.Size(792, 372);
            this.tabModifica.TabIndex = 1;
            this.tabModifica.Text = "Modifica";
            this.tabModifica.UseVisualStyleBackColor = true;
            // 
            // splitModifica
            // 
            this.splitModifica.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitModifica.Location = new System.Drawing.Point(3, 3);
            this.splitModifica.Name = "splitModifica";
            // 
            // splitModifica.Panel1
            // 
            this.splitModifica.Panel1.Controls.Add(this.pnlModificaMappa);
            // 
            // splitModifica.Panel2
            // 
            this.splitModifica.Panel2.BackColor = System.Drawing.Color.White;
            this.splitModifica.Panel2.Controls.Add(this.lbEventiStorici);
            this.splitModifica.Panel2.Controls.Add(this.label6);
            this.splitModifica.Panel2.Controls.Add(this.btnSalvaNota);
            this.splitModifica.Panel2.Controls.Add(this.cbModificaSottotipo);
            this.splitModifica.Panel2.Controls.Add(this.label5);
            this.splitModifica.Panel2.Controls.Add(this.txModificaNota);
            this.splitModifica.Panel2.Controls.Add(this.label4);
            this.splitModifica.Panel2.Controls.Add(this.button2);
            this.splitModifica.Panel2.Controls.Add(this.button1);
            this.splitModifica.Panel2.Controls.Add(this.lbModificaStrutture);
            this.splitModifica.Panel2.Controls.Add(this.label3);
            this.splitModifica.Panel2.Controls.Add(this.label2);
            this.splitModifica.Panel2.Controls.Add(this.cbModificaTerreno);
            this.splitModifica.Panel2.Controls.Add(this.tbModificaZoom);
            this.splitModifica.Size = new System.Drawing.Size(786, 366);
            this.splitModifica.SplitterDistance = 500;
            this.splitModifica.SplitterWidth = 2;
            this.splitModifica.TabIndex = 1;
            // 
            // pnlModificaMappa
            // 
            this.pnlModificaMappa.AutoScroll = true;
            this.pnlModificaMappa.Controls.Add(this.pbMappaModifica);
            this.pnlModificaMappa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlModificaMappa.Location = new System.Drawing.Point(0, 0);
            this.pnlModificaMappa.Name = "pnlModificaMappa";
            this.pnlModificaMappa.Size = new System.Drawing.Size(500, 366);
            this.pnlModificaMappa.TabIndex = 1;
            // 
            // pbMappaModifica
            // 
            this.pbMappaModifica.Location = new System.Drawing.Point(0, 0);
            this.pbMappaModifica.Name = "pbMappaModifica";
            this.pbMappaModifica.Size = new System.Drawing.Size(100, 50);
            this.pbMappaModifica.TabIndex = 0;
            this.pbMappaModifica.TabStop = false;
            this.pbMappaModifica.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pbMappaModifica_MouseClick);
            // 
            // lbEventiStorici
            // 
            this.lbEventiStorici.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbEventiStorici.FormattingEnabled = true;
            this.lbEventiStorici.Location = new System.Drawing.Point(3, 330);
            this.lbEventiStorici.Name = "lbEventiStorici";
            this.lbEventiStorici.Size = new System.Drawing.Size(282, 30);
            this.lbEventiStorici.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 317);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Eventi storici";
            // 
            // btnSalvaNota
            // 
            this.btnSalvaNota.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSalvaNota.Location = new System.Drawing.Point(208, 214);
            this.btnSalvaNota.Name = "btnSalvaNota";
            this.btnSalvaNota.Size = new System.Drawing.Size(77, 20);
            this.btnSalvaNota.TabIndex = 11;
            this.btnSalvaNota.Text = "Salva";
            this.btnSalvaNota.UseVisualStyleBackColor = true;
            // 
            // cbModificaSottotipo
            // 
            this.cbModificaSottotipo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbModificaSottotipo.FormattingEnabled = true;
            this.cbModificaSottotipo.Location = new System.Drawing.Point(4, 108);
            this.cbModificaSottotipo.Name = "cbModificaSottotipo";
            this.cbModificaSottotipo.Size = new System.Drawing.Size(281, 21);
            this.cbModificaSottotipo.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Sottotipo";
            // 
            // txModificaNota
            // 
            this.txModificaNota.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txModificaNota.Location = new System.Drawing.Point(4, 237);
            this.txModificaNota.Multiline = true;
            this.txModificaNota.Name = "txModificaNota";
            this.txModificaNota.Size = new System.Drawing.Size(281, 68);
            this.txModificaNota.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 221);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Note";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(337, 161);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(20, 22);
            this.button2.TabIndex = 6;
            this.button2.Text = "-";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(337, 182);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(20, 22);
            this.button1.TabIndex = 5;
            this.button1.Text = "+";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // lbModificaStrutture
            // 
            this.lbModificaStrutture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbModificaStrutture.FormattingEnabled = true;
            this.lbModificaStrutture.Location = new System.Drawing.Point(4, 161);
            this.lbModificaStrutture.Name = "lbModificaStrutture";
            this.lbModificaStrutture.Size = new System.Drawing.Size(281, 43);
            this.lbModificaStrutture.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 142);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Strutture";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Terreno";
            // 
            // cbModificaTerreno
            // 
            this.cbModificaTerreno.FormattingEnabled = true;
            this.cbModificaTerreno.Location = new System.Drawing.Point(3, 64);
            this.cbModificaTerreno.Name = "cbModificaTerreno";
            this.cbModificaTerreno.Size = new System.Drawing.Size(278, 21);
            this.cbModificaTerreno.TabIndex = 1;
            // 
            // tbModificaZoom
            // 
            this.tbModificaZoom.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbModificaZoom.Location = new System.Drawing.Point(0, 0);
            this.tbModificaZoom.Maximum = 100;
            this.tbModificaZoom.Minimum = 10;
            this.tbModificaZoom.Name = "tbModificaZoom";
            this.tbModificaZoom.Size = new System.Drawing.Size(284, 45);
            this.tbModificaZoom.TabIndex = 0;
            this.tbModificaZoom.Value = 22;
            this.tbModificaZoom.Scroll += new System.EventHandler(this.tbMappaModificaZoom_Scroll);
            // 
            // tabMostri
            // 
            this.tabMostri.Controls.Add(this.scMostri);
            this.tabMostri.Location = new System.Drawing.Point(4, 25);
            this.tabMostri.Name = "tabMostri";
            this.tabMostri.Size = new System.Drawing.Size(792, 372);
            this.tabMostri.TabIndex = 2;
            this.tabMostri.Text = "Mostri";
            this.tabMostri.UseVisualStyleBackColor = true;
            // 
            // scMostri
            // 
            this.scMostri.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scMostri.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.scMostri.Location = new System.Drawing.Point(0, 0);
            this.scMostri.Name = "scMostri";
            // 
            // scMostri.Panel1
            // 
            this.scMostri.Panel1.Controls.Add(this.tcMostri);
            this.scMostri.Panel1MinSize = 250;
            // 
            // scMostri.Panel2
            // 
            this.scMostri.Panel2.Controls.Add(this.ucMostro);
            this.scMostri.Size = new System.Drawing.Size(792, 372);
            this.scMostri.SplitterDistance = 250;
            this.scMostri.TabIndex = 6;
            // 
            // tcMostri
            // 
            this.tcMostri.Controls.Add(this.tpMostriLista);
            this.tcMostri.Controls.Add(this.tpMostriFiltri);
            this.tcMostri.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMostri.Location = new System.Drawing.Point(0, 0);
            this.tcMostri.Name = "tcMostri";
            this.tcMostri.SelectedIndex = 0;
            this.tcMostri.Size = new System.Drawing.Size(250, 372);
            this.tcMostri.TabIndex = 0;
            // 
            // tpMostriLista
            // 
            this.tpMostriLista.Controls.Add(this.scElenco);
            this.tpMostriLista.Location = new System.Drawing.Point(4, 22);
            this.tpMostriLista.Name = "tpMostriLista";
            this.tpMostriLista.Padding = new System.Windows.Forms.Padding(3);
            this.tpMostriLista.Size = new System.Drawing.Size(242, 346);
            this.tpMostriLista.TabIndex = 0;
            this.tpMostriLista.Text = "Elenco";
            this.tpMostriLista.UseVisualStyleBackColor = true;
            // 
            // scElenco
            // 
            this.scElenco.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scElenco.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.scElenco.IsSplitterFixed = true;
            this.scElenco.Location = new System.Drawing.Point(3, 3);
            this.scElenco.Name = "scElenco";
            this.scElenco.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // scElenco.Panel1
            // 
            this.scElenco.Panel1.Controls.Add(this.tbMostriCerca);
            this.scElenco.Panel1MinSize = 20;
            // 
            // scElenco.Panel2
            // 
            this.scElenco.Panel2.Controls.Add(this.tvMostri);
            this.scElenco.Size = new System.Drawing.Size(236, 340);
            this.scElenco.SplitterDistance = 25;
            this.scElenco.TabIndex = 0;
            // 
            // tbMostriCerca
            // 
            this.tbMostriCerca.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbMostriCerca.Location = new System.Drawing.Point(0, 0);
            this.tbMostriCerca.Name = "tbMostriCerca";
            this.tbMostriCerca.Size = new System.Drawing.Size(236, 20);
            this.tbMostriCerca.TabIndex = 3;
            // 
            // tvMostri
            // 
            this.tvMostri.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvMostri.FullRowSelect = true;
            this.tvMostri.Location = new System.Drawing.Point(0, 0);
            this.tvMostri.Name = "tvMostri";
            this.tvMostri.Size = new System.Drawing.Size(236, 311);
            this.tvMostri.TabIndex = 2;
            this.tvMostri.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvMostri_AfterSelect);
            // 
            // tpMostriFiltri
            // 
            this.tpMostriFiltri.Controls.Add(this.textBox3);
            this.tpMostriFiltri.Controls.Add(this.textBox2);
            this.tpMostriFiltri.Controls.Add(this.button4);
            this.tpMostriFiltri.Location = new System.Drawing.Point(4, 22);
            this.tpMostriFiltri.Name = "tpMostriFiltri";
            this.tpMostriFiltri.Padding = new System.Windows.Forms.Padding(3);
            this.tpMostriFiltri.Size = new System.Drawing.Size(242, 346);
            this.tpMostriFiltri.TabIndex = 1;
            this.tpMostriFiltri.Text = "Filtri";
            this.tpMostriFiltri.UseVisualStyleBackColor = true;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(8, 76);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(234, 20);
            this.textBox3.TabIndex = 2;
            this.textBox3.Text = "Descrizione";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(8, 50);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(234, 20);
            this.textBox2.TabIndex = 1;
            this.textBox2.Text = "Nome";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(25, 6);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(196, 25);
            this.button4.TabIndex = 0;
            this.button4.Text = "Aggiungi filtro (popup?)";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // ucMostro
            // 
            this.ucMostro.AutoScroll = true;
            this.ucMostro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucMostro.Location = new System.Drawing.Point(0, 0);
            this.ucMostro.Name = "ucMostro";
            this.ucMostro.Size = new System.Drawing.Size(538, 372);
            this.ucMostro.TabIndex = 5;
            // 
            // tabConsole
            // 
            this.tabConsole.Controls.Add(this.lbConsoleMessages);
            this.tabConsole.Location = new System.Drawing.Point(4, 25);
            this.tabConsole.Name = "tabConsole";
            this.tabConsole.Size = new System.Drawing.Size(792, 372);
            this.tabConsole.TabIndex = 3;
            this.tabConsole.Text = "Console";
            this.tabConsole.UseVisualStyleBackColor = true;
            // 
            // lbConsoleMessages
            // 
            this.lbConsoleMessages.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbConsoleMessages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbConsoleMessages.FormattingEnabled = true;
            this.lbConsoleMessages.IntegralHeight = false;
            this.lbConsoleMessages.Location = new System.Drawing.Point(0, 0);
            this.lbConsoleMessages.Name = "lbConsoleMessages";
            this.lbConsoleMessages.Size = new System.Drawing.Size(792, 372);
            this.lbConsoleMessages.TabIndex = 0;
            // 
            // tabGenerazione
            // 
            this.tabGenerazione.Controls.Add(this.tcGenerazioneIncontro);
            this.tabGenerazione.Location = new System.Drawing.Point(4, 25);
            this.tabGenerazione.Name = "tabGenerazione";
            this.tabGenerazione.Size = new System.Drawing.Size(792, 372);
            this.tabGenerazione.TabIndex = 4;
            this.tabGenerazione.Text = "Generazione";
            this.tabGenerazione.UseVisualStyleBackColor = true;
            // 
            // tcGenerazioneIncontro
            // 
            this.tcGenerazioneIncontro.Controls.Add(this.tpOpzioniGenerazione);
            this.tcGenerazioneIncontro.Controls.Add(this.tpElencoMostriGenerati);
            this.tcGenerazioneIncontro.Controls.Add(this.tpTesoroGenerato);
            this.tcGenerazioneIncontro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcGenerazioneIncontro.Location = new System.Drawing.Point(0, 0);
            this.tcGenerazioneIncontro.Name = "tcGenerazioneIncontro";
            this.tcGenerazioneIncontro.SelectedIndex = 0;
            this.tcGenerazioneIncontro.Size = new System.Drawing.Size(792, 372);
            this.tcGenerazioneIncontro.TabIndex = 7;
            // 
            // tpOpzioniGenerazione
            // 
            this.tpOpzioniGenerazione.Controls.Add(this.scOpzioniGenerazioneIncontro);
            this.tpOpzioniGenerazione.Location = new System.Drawing.Point(4, 22);
            this.tpOpzioniGenerazione.Name = "tpOpzioniGenerazione";
            this.tpOpzioniGenerazione.Padding = new System.Windows.Forms.Padding(3);
            this.tpOpzioniGenerazione.Size = new System.Drawing.Size(784, 346);
            this.tpOpzioniGenerazione.TabIndex = 0;
            this.tpOpzioniGenerazione.Text = "Generazione";
            this.tpOpzioniGenerazione.UseVisualStyleBackColor = true;
            // 
            // scOpzioniGenerazioneIncontro
            // 
            this.scOpzioniGenerazioneIncontro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scOpzioniGenerazioneIncontro.Location = new System.Drawing.Point(3, 3);
            this.scOpzioniGenerazioneIncontro.Name = "scOpzioniGenerazioneIncontro";
            // 
            // scOpzioniGenerazioneIncontro.Panel1
            // 
            this.scOpzioniGenerazioneIncontro.Panel1.Controls.Add(this.btnPulisciStoricoGenerazione);
            this.scOpzioniGenerazioneIncontro.Panel1.Controls.Add(this.label7);
            this.scOpzioniGenerazioneIncontro.Panel1.Controls.Add(this.lbStoricoGenerazione);
            // 
            // scOpzioniGenerazioneIncontro.Panel2
            // 
            this.scOpzioniGenerazioneIncontro.Panel2.Controls.Add(this.label1);
            this.scOpzioniGenerazioneIncontro.Panel2.Controls.Add(this.btnGeneraTest);
            this.scOpzioniGenerazioneIncontro.Panel2.Controls.Add(this.button3);
            this.scOpzioniGenerazioneIncontro.Size = new System.Drawing.Size(778, 340);
            this.scOpzioniGenerazioneIncontro.SplitterDistance = 259;
            this.scOpzioniGenerazioneIncontro.TabIndex = 7;
            // 
            // btnPulisciStoricoGenerazione
            // 
            this.btnPulisciStoricoGenerazione.Location = new System.Drawing.Point(49, -3);
            this.btnPulisciStoricoGenerazione.Name = "btnPulisciStoricoGenerazione";
            this.btnPulisciStoricoGenerazione.Size = new System.Drawing.Size(74, 19);
            this.btnPulisciStoricoGenerazione.TabIndex = 7;
            this.btnPulisciStoricoGenerazione.Text = "Pulisci";
            this.btnPulisciStoricoGenerazione.UseVisualStyleBackColor = true;
            this.btnPulisciStoricoGenerazione.Click += new System.EventHandler(this.btnPulisciStoricoGenerazione_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Storico";
            // 
            // lbStoricoGenerazione
            // 
            this.lbStoricoGenerazione.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbStoricoGenerazione.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbStoricoGenerazione.FormattingEnabled = true;
            this.lbStoricoGenerazione.IntegralHeight = false;
            this.lbStoricoGenerazione.Location = new System.Drawing.Point(0, 16);
            this.lbStoricoGenerazione.Name = "lbStoricoGenerazione";
            this.lbStoricoGenerazione.Size = new System.Drawing.Size(256, 324);
            this.lbStoricoGenerazione.TabIndex = 5;
            this.lbStoricoGenerazione.SelectedIndexChanged += new System.EventHandler(this.lbStoricoGenerazione_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 39);
            this.label1.TabIndex = 5;
            this.label1.Text = "Cella: X Y\r\nTipo: ---\r\nBla: bla";
            // 
            // btnGeneraTest
            // 
            this.btnGeneraTest.Location = new System.Drawing.Point(141, 87);
            this.btnGeneraTest.Name = "btnGeneraTest";
            this.btnGeneraTest.Size = new System.Drawing.Size(106, 41);
            this.btnGeneraTest.TabIndex = 3;
            this.btnGeneraTest.Text = "Genera (per cella)";
            this.btnGeneraTest.UseVisualStyleBackColor = true;
            this.btnGeneraTest.Click += new System.EventHandler(this.btnGeneraTest_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(141, 156);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(106, 41);
            this.button3.TabIndex = 4;
            this.button3.Text = "test oggetti x lmg";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // tpElencoMostriGenerati
            // 
            this.tpElencoMostriGenerati.Controls.Add(this.flpMostriGenerati);
            this.tpElencoMostriGenerati.Location = new System.Drawing.Point(4, 22);
            this.tpElencoMostriGenerati.Name = "tpElencoMostriGenerati";
            this.tpElencoMostriGenerati.Padding = new System.Windows.Forms.Padding(3);
            this.tpElencoMostriGenerati.Size = new System.Drawing.Size(784, 346);
            this.tpElencoMostriGenerati.TabIndex = 1;
            this.tpElencoMostriGenerati.Text = "Mostri Generati";
            this.tpElencoMostriGenerati.UseVisualStyleBackColor = true;
            // 
            // flpMostriGenerati
            // 
            this.flpMostriGenerati.AutoScroll = true;
            this.flpMostriGenerati.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpMostriGenerati.Location = new System.Drawing.Point(3, 3);
            this.flpMostriGenerati.Name = "flpMostriGenerati";
            this.flpMostriGenerati.Size = new System.Drawing.Size(778, 340);
            this.flpMostriGenerati.TabIndex = 0;
            // 
            // tpTesoroGenerato
            // 
            this.tpTesoroGenerato.Controls.Add(this.dgvTesoroGenerato);
            this.tpTesoroGenerato.Location = new System.Drawing.Point(4, 22);
            this.tpTesoroGenerato.Name = "tpTesoroGenerato";
            this.tpTesoroGenerato.Size = new System.Drawing.Size(784, 346);
            this.tpTesoroGenerato.TabIndex = 2;
            this.tpTesoroGenerato.Text = "Tesoro Generato";
            this.tpTesoroGenerato.UseVisualStyleBackColor = true;
            // 
            // dgvTesoroGenerato
            // 
            this.dgvTesoroGenerato.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTesoroGenerato.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colId,
            this.colPrezzo,
            this.colQta,
            this.colNome});
            this.dgvTesoroGenerato.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTesoroGenerato.Location = new System.Drawing.Point(0, 0);
            this.dgvTesoroGenerato.Name = "dgvTesoroGenerato";
            this.dgvTesoroGenerato.RowHeadersVisible = false;
            this.dgvTesoroGenerato.Size = new System.Drawing.Size(784, 346);
            this.dgvTesoroGenerato.TabIndex = 0;
            // 
            // colId
            // 
            this.colId.HeaderText = "Id";
            this.colId.Name = "colId";
            this.colId.ReadOnly = true;
            this.colId.Width = 75;
            // 
            // colPrezzo
            // 
            this.colPrezzo.HeaderText = "Prezzo";
            this.colPrezzo.Name = "colPrezzo";
            this.colPrezzo.ReadOnly = true;
            this.colPrezzo.Width = 50;
            // 
            // colQta
            // 
            this.colQta.HeaderText = "Qta";
            this.colQta.Name = "colQta";
            this.colQta.ReadOnly = true;
            this.colQta.Width = 50;
            // 
            // colNome
            // 
            this.colNome.HeaderText = "Nome";
            this.colNome.Name = "colNome";
            this.colNome.ReadOnly = true;
            this.colNome.Width = 350;
            // 
            // tabPolitica
            // 
            this.tabPolitica.Controls.Add(this.pnlPolitica);
            this.tabPolitica.Location = new System.Drawing.Point(4, 25);
            this.tabPolitica.Name = "tabPolitica";
            this.tabPolitica.Size = new System.Drawing.Size(792, 372);
            this.tabPolitica.TabIndex = 5;
            this.tabPolitica.Text = "Politica";
            this.tabPolitica.UseVisualStyleBackColor = true;
            // 
            // pnlPolitica
            // 
            this.pnlPolitica.AutoScroll = true;
            this.pnlPolitica.BackColor = System.Drawing.Color.White;
            this.pnlPolitica.Controls.Add(this.pbPolitica);
            this.pnlPolitica.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPolitica.Location = new System.Drawing.Point(0, 0);
            this.pnlPolitica.Name = "pnlPolitica";
            this.pnlPolitica.Size = new System.Drawing.Size(792, 372);
            this.pnlPolitica.TabIndex = 4;
            // 
            // pbPolitica
            // 
            this.pbPolitica.BackColor = System.Drawing.Color.White;
            this.pbPolitica.Location = new System.Drawing.Point(0, 0);
            this.pbPolitica.Name = "pbPolitica";
            this.pbPolitica.Size = new System.Drawing.Size(125, 93);
            this.pbPolitica.TabIndex = 1;
            this.pbPolitica.TabStop = false;
            this.pbPolitica.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pbPolitica_Click);
            // 
            // tpOggetti
            // 
            this.tpOggetti.Controls.Add(this.scOggetti);
            this.tpOggetti.Location = new System.Drawing.Point(4, 25);
            this.tpOggetti.Name = "tpOggetti";
            this.tpOggetti.Size = new System.Drawing.Size(792, 372);
            this.tpOggetti.TabIndex = 6;
            this.tpOggetti.Text = "Oggetti";
            this.tpOggetti.UseVisualStyleBackColor = true;
            // 
            // scOggetti
            // 
            this.scOggetti.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scOggetti.Location = new System.Drawing.Point(0, 0);
            this.scOggetti.Name = "scOggetti";
            // 
            // scOggetti.Panel1
            // 
            this.scOggetti.Panel1.Controls.Add(this.tvOggettiGerarchia);
            this.scOggetti.Panel1.Controls.Add(this.txOggettiSearch);
            this.scOggetti.Panel1.Controls.Add(this.chkOggettiFilterDesc);
            this.scOggetti.Panel1.Controls.Add(this.chkOggettiFilterNome);
            // 
            // scOggetti.Panel2
            // 
            this.scOggetti.Panel2.Controls.Add(this.dgvOggetti);
            this.scOggetti.Size = new System.Drawing.Size(792, 372);
            this.scOggetti.SplitterDistance = 264;
            this.scOggetti.TabIndex = 4;
            // 
            // tvOggettiGerarchia
            // 
            this.tvOggettiGerarchia.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvOggettiGerarchia.Location = new System.Drawing.Point(0, 52);
            this.tvOggettiGerarchia.Name = "tvOggettiGerarchia";
            this.tvOggettiGerarchia.Size = new System.Drawing.Size(261, 317);
            this.tvOggettiGerarchia.TabIndex = 3;
            // 
            // txOggettiSearch
            // 
            this.txOggettiSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txOggettiSearch.Location = new System.Drawing.Point(3, 3);
            this.txOggettiSearch.Name = "txOggettiSearch";
            this.txOggettiSearch.Size = new System.Drawing.Size(258, 20);
            this.txOggettiSearch.TabIndex = 0;
            // 
            // chkOggettiFilterDesc
            // 
            this.chkOggettiFilterDesc.AutoSize = true;
            this.chkOggettiFilterDesc.Location = new System.Drawing.Point(68, 29);
            this.chkOggettiFilterDesc.Name = "chkOggettiFilterDesc";
            this.chkOggettiFilterDesc.Size = new System.Drawing.Size(81, 17);
            this.chkOggettiFilterDesc.TabIndex = 2;
            this.chkOggettiFilterDesc.Text = "Descrizione";
            this.chkOggettiFilterDesc.UseVisualStyleBackColor = true;
            // 
            // chkOggettiFilterNome
            // 
            this.chkOggettiFilterNome.AutoSize = true;
            this.chkOggettiFilterNome.Checked = true;
            this.chkOggettiFilterNome.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOggettiFilterNome.Location = new System.Drawing.Point(8, 29);
            this.chkOggettiFilterNome.Name = "chkOggettiFilterNome";
            this.chkOggettiFilterNome.Size = new System.Drawing.Size(54, 17);
            this.chkOggettiFilterNome.TabIndex = 1;
            this.chkOggettiFilterNome.Text = "Nome";
            this.chkOggettiFilterNome.UseVisualStyleBackColor = true;
            // 
            // dgvOggetti
            // 
            this.dgvOggetti.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOggetti.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvOggettiId,
            this.dgvOggettiTipo,
            this.dgvOggettiNome,
            this.dgvOggettiSlot,
            this.dgvOggettirezzo,
            this.dgvOggettiDescrizione});
            this.dgvOggetti.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvOggetti.Location = new System.Drawing.Point(0, 0);
            this.dgvOggetti.Name = "dgvOggetti";
            this.dgvOggetti.Size = new System.Drawing.Size(524, 372);
            this.dgvOggetti.TabIndex = 3;
            // 
            // ssFp
            // 
            this.ssFp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssMessage});
            this.ssFp.Location = new System.Drawing.Point(0, 428);
            this.ssFp.Name = "ssFp";
            this.ssFp.Size = new System.Drawing.Size(800, 22);
            this.ssFp.TabIndex = 4;
            this.ssFp.Text = "statusStrip1";
            // 
            // tssMessage
            // 
            this.tssMessage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tssMessage.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.tssMessage.Name = "tssMessage";
            this.tssMessage.Size = new System.Drawing.Size(12, 17);
            this.tssMessage.Text = "-";
            this.tssMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dgvOggettiId
            // 
            this.dgvOggettiId.HeaderText = "Id";
            this.dgvOggettiId.Name = "dgvOggettiId";
            this.dgvOggettiId.Width = 66;
            // 
            // dgvOggettiTipo
            // 
            this.dgvOggettiTipo.HeaderText = "Tipo";
            this.dgvOggettiTipo.Name = "dgvOggettiTipo";
            this.dgvOggettiTipo.Width = 123;
            // 
            // dgvOggettiNome
            // 
            this.dgvOggettiNome.HeaderText = "Nome";
            this.dgvOggettiNome.Name = "dgvOggettiNome";
            this.dgvOggettiNome.Width = 225;
            // 
            // dgvOggettiSlot
            // 
            this.dgvOggettiSlot.HeaderText = "Slot";
            this.dgvOggettiSlot.Name = "dgvOggettiSlot";
            this.dgvOggettiSlot.Width = 75;
            // 
            // dgvOggettirezzo
            // 
            this.dgvOggettirezzo.HeaderText = "Prezzo";
            this.dgvOggettirezzo.Name = "dgvOggettirezzo";
            this.dgvOggettirezzo.Width = 50;
            // 
            // dgvOggettiDescrizione
            // 
            this.dgvOggettiDescrizione.HeaderText = "Descrizione";
            this.dgvOggettiDescrizione.Name = "dgvOggettiDescrizione";
            this.dgvOggettiDescrizione.Width = 300;
            // 
            // frmFP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabMain);
            this.Controls.Add(this.ssFp);
            this.Controls.Add(this.menuStripFp);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStripFp;
            this.Name = "frmFP";
            this.Text = "Fallout Piemonte";
            ((System.ComponentModel.ISupportInitialize)(this.pbMappa)).EndInit();
            this.menuStripFp.ResumeLayout(false);
            this.menuStripFp.PerformLayout();
            this.tabMain.ResumeLayout(false);
            this.tabMappa.ResumeLayout(false);
            this.pnlMappa.ResumeLayout(false);
            this.pnlMappa.PerformLayout();
            this.popupMapTools.ResumeLayout(false);
            this.tabModifica.ResumeLayout(false);
            this.splitModifica.Panel1.ResumeLayout(false);
            this.splitModifica.Panel2.ResumeLayout(false);
            this.splitModifica.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitModifica)).EndInit();
            this.splitModifica.ResumeLayout(false);
            this.pnlModificaMappa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbMappaModifica)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbModificaZoom)).EndInit();
            this.tabMostri.ResumeLayout(false);
            this.scMostri.Panel1.ResumeLayout(false);
            this.scMostri.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scMostri)).EndInit();
            this.scMostri.ResumeLayout(false);
            this.tcMostri.ResumeLayout(false);
            this.tpMostriLista.ResumeLayout(false);
            this.scElenco.Panel1.ResumeLayout(false);
            this.scElenco.Panel1.PerformLayout();
            this.scElenco.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scElenco)).EndInit();
            this.scElenco.ResumeLayout(false);
            this.tpMostriFiltri.ResumeLayout(false);
            this.tpMostriFiltri.PerformLayout();
            this.tabConsole.ResumeLayout(false);
            this.tabGenerazione.ResumeLayout(false);
            this.tcGenerazioneIncontro.ResumeLayout(false);
            this.tpOpzioniGenerazione.ResumeLayout(false);
            this.scOpzioniGenerazioneIncontro.Panel1.ResumeLayout(false);
            this.scOpzioniGenerazioneIncontro.Panel1.PerformLayout();
            this.scOpzioniGenerazioneIncontro.Panel2.ResumeLayout(false);
            this.scOpzioniGenerazioneIncontro.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scOpzioniGenerazioneIncontro)).EndInit();
            this.scOpzioniGenerazioneIncontro.ResumeLayout(false);
            this.tpElencoMostriGenerati.ResumeLayout(false);
            this.tpTesoroGenerato.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTesoroGenerato)).EndInit();
            this.tabPolitica.ResumeLayout(false);
            this.pnlPolitica.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbPolitica)).EndInit();
            this.tpOggetti.ResumeLayout(false);
            this.scOggetti.Panel1.ResumeLayout(false);
            this.scOggetti.Panel1.PerformLayout();
            this.scOggetti.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scOggetti)).EndInit();
            this.scOggetti.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOggetti)).EndInit();
            this.ssFp.ResumeLayout(false);
            this.ssFp.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pbMappa;
        private System.Windows.Forms.MenuStrip menuStripFp;
        private System.Windows.Forms.ToolStripMenuItem vuotoToolStripMenuItem;
        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabMappa;
        private System.Windows.Forms.TabPage tabModifica;
        private System.Windows.Forms.StatusStrip ssFp;
        public ucMapTools MapTools;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salvaMappaToolStripMenuItem;
        private System.Windows.Forms.Panel pnlMappa;
        private System.Windows.Forms.ContextMenuStrip popupMapTools;
        private System.Windows.Forms.ToolStripMenuItem dockingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sinistraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem destraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem volanteToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitModifica;
        private System.Windows.Forms.PictureBox pbMappaModifica;
        private System.Windows.Forms.TabPage tabMostri;
        private System.Windows.Forms.Panel pnlModificaMappa;
        private System.Windows.Forms.TrackBar tbModificaZoom;
        private System.Windows.Forms.ToolStripStatusLabel tssMessage;
        private System.Windows.Forms.TabPage tabConsole;
        private System.Windows.Forms.ListBox lbConsoleMessages;
        private System.Windows.Forms.TextBox txModificaNota;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox lbModificaStrutture;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbModificaTerreno;
        private System.Windows.Forms.ToolStripMenuItem autoreToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox tstxAutore;
        private System.Windows.Forms.ComboBox cbModificaSottotipo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabGenerazione;
        private System.Windows.Forms.Button btnSalvaNota;
        private System.Windows.Forms.TreeView tvMostri;
        private System.Windows.Forms.ToolStripMenuItem generaSottotipiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripTextBox1;
        private System.Windows.Forms.ToolStripTextBox tssTxRandomSottotipiSeed;
        private System.Windows.Forms.ToolStripMenuItem generaToolStripMenuItem;
        private System.Windows.Forms.Button btnGeneraTest;
        private System.Windows.Forms.ToolStripMenuItem vistaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mappaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem probabilitàIncontroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem politicaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cercaNelleNoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem livelloDelGruppoToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPolitica;
        private System.Windows.Forms.TextBox tbMostriCerca;
        private System.Windows.Forms.TabPage tpOggetti;
        private System.Windows.Forms.DataGridView dgvOggetti;
        private System.Windows.Forms.CheckBox chkOggettiFilterDesc;
        private System.Windows.Forms.CheckBox chkOggettiFilterNome;
        private System.Windows.Forms.TextBox txOggettiSearch;
        private System.Windows.Forms.ListBox lbEventiStorici;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ToolStripMenuItem tsmDivisore;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel pnlPolitica;
        private System.Windows.Forms.PictureBox pbPolitica;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.ListBox lbStoricoGenerazione;
        public System.Windows.Forms.TextBox tbMappaFocus;
        private ucMostro ucMostro;
        private System.Windows.Forms.SplitContainer scMostri;
        private System.Windows.Forms.TabControl tcMostri;
        private System.Windows.Forms.TabPage tpMostriLista;
        private System.Windows.Forms.SplitContainer scElenco;
        private System.Windows.Forms.TabPage tpMostriFiltri;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TabControl tcGenerazioneIncontro;
        private System.Windows.Forms.TabPage tpOpzioniGenerazione;
        private System.Windows.Forms.TabPage tpElencoMostriGenerati;
        private System.Windows.Forms.SplitContainer scOpzioniGenerazioneIncontro;
        private System.Windows.Forms.TabPage tpTesoroGenerato;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.DataGridView dgvTesoroGenerato;
        public System.Windows.Forms.FlowLayoutPanel flpMostriGenerati;
        private System.Windows.Forms.Button btnPulisciStoricoGenerazione;
        private System.Windows.Forms.ToolStripTextBox tstxLmg;
        private System.Windows.Forms.DataGridViewTextBoxColumn colId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPrezzo;
        private System.Windows.Forms.DataGridViewTextBoxColumn colQta;
        private System.Windows.Forms.DataGridViewTextBoxColumn colNome;
        private System.Windows.Forms.SplitContainer scOggetti;
        private System.Windows.Forms.TreeView tvOggettiGerarchia;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvOggettiId;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvOggettiTipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvOggettiNome;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvOggettiSlot;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvOggettirezzo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvOggettiDescrizione;
    }
}

