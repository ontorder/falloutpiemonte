﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace FP.UI.Forms
{
    public partial class frmFP : Form
    {
        public enum DrawMapView
        {
            Default,
            Politica,
            ProbabilitaIncontro
        }

        public Action<MouseEventArgs> MappaClick;
        public Action<MouseEventArgs> MappaModificaClick;
        public Action<MouseEventArgs> MappaPoliticaClick;
        public Action<MouseEventArgs> MappaMouseMove;
        public Action                 MappaSalva;
        public Action<int>            MappaModificaScaleChanged;
        public Action<int>            MappaPoliticaScaleChanged;
        public Action<string>         RandomSottotipi;
        public Action                 GenerazioneTest;
        public Action<DrawMapView>    MapSwitchView;
        public Action<string>         TvMostriSelected;
        public Action<int>            OggettiTest;
        public Action<Keys>           MappaKeyDown;
        public Action<int>            StoricoIncontroSelected;
        public Action                 StoricoIncontroClear;

        public frmFP()
        {
            InitializeComponent();
        }

        public void Log(string message, Color color)
        {
            tssMessage.Text = message;
            tssMessage.ForeColor = color;
            lbConsoleMessages.Items.Add(message);
        }

        public void UpdatePbMappaImage(Bitmap bmp) =>
            pbMappa.Image = bmp;

        public void UpdatePbMappaModificaImage(Bitmap bmp) =>
            pbMappaModifica.Image = bmp;

        public void UpdatePbMappaPoliticaImage(Bitmap bmp) =>
            pbPolitica.Image = bmp;

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var giturl = "https://gitlab.com/ontorder/falloutpiemonte";
            Log(giturl, Color.White);
            MessageBox.Show(giturl, "ti prego usami");
        }

        private void PbMappa_MouseClick(object sender, MouseEventArgs e)
        {
            tbMappaFocus.Focus();
            MappaClick(e);
        }

        private void PbMappa_MouseMove(object sender, MouseEventArgs e) =>
            MappaMouseMove(e);

        private void SalvaMappaToolStripMenuItem_Click(object sender, EventArgs e) =>
            MappaSalva();

        public void UpdatePbMappaDimensions(int width, int height)
        {
            pbMappa.Width = width;
            pbMappa.Height = height;
        }

        public void UpdatePbMappaModificaDimensions(int width, int height)
        {
            pbMappaModifica.Width = width;
            pbMappaModifica.Height = height;
        }

        public void UpdatePbMappaPoliticaDimensions(int width, int height)
        {
            pbPolitica.Width = width;
            pbPolitica.Height = height;
        }

        private void sinistraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MapTools.Dock = DockStyle.Left;
        }

        private void destraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MapTools.Dock = DockStyle.Right;
        }

        private void volanteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MapTools.Dock = DockStyle.None;
        }

        private void pbMappaModifica_MouseClick(object sender, MouseEventArgs e) =>
            MappaModificaClick(e);

        private void tbMappaModificaZoom_Scroll(object sender, EventArgs e) =>
            MappaModificaScaleChanged(tbModificaZoom.Value);

        public void PutModificaCellaSelected(Data.Models.Cella cella)
        {
            if (cella == null)
                return;
            // TODO autore
            //txModificaNota.Text = cella.Note;
            txModificaNota.Text = String.Empty;

            cbModificaTerreno.Text = cella.Terreno;

            cbModificaSottotipo.Text = cella.Sottotipo;

            lbModificaStrutture.Items.Clear();
            if (cella.Strutture != null)
            {
                foreach (var struttura in cella.Strutture)
                    lbModificaStrutture.Items.Add(struttura);
            }
        }

        public void PutPoliticaCellaSelected(Data.Models.Cella cella)
        {
            if (cella == null)
                return;

            //txModificaNota.Text = String.Empty;
            //cbModificaTerreno.Text = cella.Terreno;
            //cbModificaSottotipo.Text = cella.Sottotipo;
            //
            //lbModificaStrutture.Items.Clear();
            //if (cella.Strutture != null)
            //{
            //    foreach(var struttura in cella.Strutture)
            //        lbModificaStrutture.Items.Add(struttura);
            //}
        }

        private void generaToolStripMenuItem_Click(object sender, EventArgs e) =>
            RandomSottotipi(tssTxRandomSottotipiSeed.Text);

        private void btnGeneraTest_Click(object sender, EventArgs e) =>
            GenerazioneTest();

        private void mappaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MapSwitchView(DrawMapView.Default);
        }

        private void probabilitàIncontroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MapSwitchView(DrawMapView.ProbabilitaIncontro);
        }

        private void cercaNelleNoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // TODO
        }

        private void tstxLmg_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (int)Keys.Back)
                return;

            if (!Char.IsDigit(e.KeyChar))
                e.Handled = true;
        }

        public int GetLmg() =>
            String.IsNullOrWhiteSpace(tstxLmg.Text) ? 0 : Int32.Parse(tstxLmg.Text);

        public void InitMostri(
            Data.Models.Scheda[] mostri,
            (int id, string nome)[] regni
        )
        {
            foreach (var regno in regni)
                tvMostri.Nodes.Add(regno.id.ToString(), regno.nome);

            foreach (var mostro in mostri)
            {
                foreach (var tipo in mostro.Tipo)
                {
                    if (!tvMostri.Nodes[mostro.Regno.Id.ToString()].Nodes.ContainsKey(tipo.Id.ToString()))
                        tvMostri.Nodes[mostro.Regno.Id.ToString()].Nodes.Add(tipo.Id.ToString(), tipo.Nome);

                    tvMostri
                        .Nodes[mostro.Regno.Id.ToString()]
                        .Nodes[tipo.Id.ToString()]
                        .Nodes.Add(mostro.Id, $"{mostro.Nome} {mostro.Modificatore}")
                    ;
                }
            }
        }

        private void tvMostri_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Level == 2)
                TvMostriSelected(e.Node.Name);
        }

        public void TabMostriSetMostro(Data.Models.Scheda mostro) =>
            ucMostro.SetMostro(mostro);

        public void InitOggetti(Data.Models.Oggetto[] oggetti, Dictionary<string, Data.Models.Prezzo> prezzi)
        {
            var textFilter = txOggettiSearch.Text;
            bool is1 = chkOggettiFilterDesc.Checked;
            bool is2 = chkOggettiFilterNome.Checked;
            var gerarchiaFilter = tvOggettiGerarchia.SelectedNode;
            FiltraOggetti(oggetti, prezzi);
        }

        private void FiltraOggetti(Data.Models.Oggetto[] oggetti, Dictionary<string, Data.Models.Prezzo> prezzi)
        {
            foreach (var ogg in oggetti)
            {
                string slot = null;
                string prezzo;

                if (ogg is Data.Models.Permanente oggp)
                    if (oggp.Slot.HasValue)
                        slot = oggp.Slot.Value.ToString();

                prezzo = prezzi[ogg.Id].Valore.ToString();
                var tipo = ogg.GetType().Name;

                dgvOggetti.Rows.Add(new[] { ogg.Id, tipo, ogg.Nome, slot, prezzo, ogg.Descrizione });
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            OggettiTest(GetLmg());
        }

        private void pbPolitica_Click(object sender, MouseEventArgs e) =>
            MappaPoliticaClick(e);

        private void tbMappaFocus_KeyDown(object sender, KeyEventArgs e) =>
            MappaKeyDown(e.KeyCode);

        private void lbStoricoGenerazione_SelectedIndexChanged(object sender, EventArgs e) =>
            StoricoIncontroSelected(lbStoricoGenerazione.SelectedIndex);

        private void btnPulisciStoricoGenerazione_Click(object sender, EventArgs e) =>
            StoricoIncontroClear();
    }
}
