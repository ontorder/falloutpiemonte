﻿using FP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FP.UI.Controls
{
    public partial class ucSchedaBreve : UserControl
    {
        public ucSchedaBreve(Scheda[] schede)
        {
            InitializeComponent();

            txtScheda.Text = FormatScheda(schede[0]);
            for (int id = 0; id < schede.Length; ++id)
            {
                var scheda = schede[id];
                // UNDONE ovviamente non è da fare qui
                int pf = FP.Services.Dadi.Tira(scheda.DadiVita);
                dgvVita.Rows.Add(new object[] { "#" + (id + 1), pf });
            }
        }

        private string FormatScheda(Scheda pg)
        {
            //int pf = FP.Services.Dadi.Tira(pg.DadiVita);
            string titolo;

            if (String.IsNullOrEmpty(pg.Modificatore))
                titolo = $"{pg.Nome.ToUpper()} (GS {pg.GradoSfida})";
            else
                titolo = $"{pg.Nome.ToUpper()}, {pg.Modificatore.ToUpper()} (GS {pg.GradoSfida})";

            string tipo = String.Join(", ", pg.Tipo?.Select(_ => _.Nome));

            List<string> lines = new List<string>
            {
                titolo,
                $"{pg.Regno.Nome} {pg.Taglia}",
                $"Tipo: {tipo}",
                String.Empty,
                $"I: {pg.Iniziativa} Vel: {pg.Velocità.Base}",
              //  $"PF: {pf} ({pg.DadiVita}) CA: {pg.ClasseArmatura} ({pg.Attributi?.Destrezza.Modificatore} DES) B/E/R: {pg.Riduzioni.Ballistica}/{pg.Riduzioni.Energia}/{pg.Riduzioni.Radiazioni}"
                $"PF: {pg.DadiVita} CA: {pg.ClasseArmatura} ({pg.Attributi?.Destrezza.Modificatore} DES) B/E/R: {pg.Riduzioni.Ballistica}/{pg.Riduzioni.Energia}/{pg.Riduzioni.Radiazioni}"
            };

            if (pg.Attacchi?.Length > 0)
            {
                lines.Add(String.Empty);
                lines.Add("ATTACCO");
                lines.AddRange(pg.Attacchi);
            }

            lines.Add(String.Empty);
            lines.Add("FOR DES COS");
            lines.Add($"{pg.Attributi?.Forza,3} {pg.Attributi?.Destrezza,3} {pg.Attributi?.Costituzione,3}");
            lines.Add("INT SAG CAR");
            lines.Add($"{pg.Attributi?.Intelligenza,3} {pg.Attributi?.Saggezza,3} {pg.Attributi?.Carisma,3}");
            lines.Add("Tem Rif Vol");
            lines.Add($"{pg.TiriSalvezza?.Tempra,3} {pg.TiriSalvezza?.Riflessi,3} {pg.TiriSalvezza?.Volontà,3}");
            lines.Add("BAB BMC DMC");
            lines.Add($"{pg?.BonusCombat.AttaccoBase,3} {pg?.BonusCombat.ManovraCombat,3} {pg?.BonusCombat.DifesaCombat,3}");

            if (pg.Speciali?.Length > 0)
            {
                lines.Add(String.Empty);
                lines.Add("Speciali");
                lines.AddRange(pg.Speciali);
            }

            // talenti
            // abilità

            lines.Add(String.Empty);
            lines.Add($"Tesoro: {pg.Tesoro.Libero}");

            return String.Join("\r\n", lines);
        }
    }
}
