﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace FP.UI.Controls
{
    public partial class ucMapTools : UserControl
    {
        private bool isMoving = false;
        private Point movingMousePos;

        // ----------------

        public ucMapTools()
        {
            InitializeComponent();
        }

        private void gpSelected_MouseMove(object sender, MouseEventArgs e) =>
            FormMouseMove(e.X, e.Y);

        private void gpSelected_MouseUp(object sender, MouseEventArgs e) =>
            StopFormMove(e.Button);

        private void gpSelected_MouseDown(object sender, MouseEventArgs e) =>
            StartFormMove(e.Button, e.Location);

        private void UcMapTools_MouseDown(object sender, MouseEventArgs e) =>
            StartFormMove(e.Button, e.Location);

        private void StartFormMove(MouseButtons mb, Point location)
        {
            if (mb == MouseButtons.Left)
            {
                isMoving = true;
                movingMousePos = location;
            }
        }

        private void StopFormMove(MouseButtons mb)
        {
            if (mb == MouseButtons.Left)
                isMoving = false;
        }

        private void FormMouseMove(int x, int y)
        {
            if (isMoving){
                Top += y - movingMousePos.Y;
                Left += x - movingMousePos.X;
            }
        }

        private void UcMapTools_MouseUp(object sender, MouseEventArgs e) =>
            StopFormMove(e.Button);

        private void UcMapTools_MouseMove(object sender, MouseEventArgs e) =>
            FormMouseMove(e.X, e.Y);

        private void TbScala_Scroll(object sender, EventArgs e){
            gpScala.Text = $"Scala ({tbScala.Value})";
            ScaleChanged?.Invoke(tbScala.Value);
        }

        public void WriteCoords(int x, int y, int? settore, int? ordinale, int pixelX, int pixelY){
            txtCellaXp.Text = pixelX.ToString();
            txtCellaYp.Text = pixelY.ToString();
            txtX.Text = x.ToString();
            txtY.Text = y.ToString();
            txtCellaSettore.Text = settore.ToString();
            txtCellaOrdinale.Text = ordinale.ToString();
        }

        public Action<int> ScaleChanged = null;

        public void SetSelezionata(Data.Models.Cella cella){
            txtSelOrdinale.Text = cella?.Ordinale.ToString();
            txtSelSettore.Text = cella?.SettoreId.ToString();

            if (cella != null)
            {
                var strutture = cella.Strutture == null ? null : String.Join(", ", cella.Strutture.Select(strutt => strutt.Item1));
                string[] lines = new []
                {
                    "Nome: " + cella.Nome,
                    "Terreno: " + cella.Terreno,
                    "Sottotipo: " + cella.Sottotipo,
                    "Strutture: " + strutture
                };
                lblTerreno.Text = String.Join("\n", lines);
            }
            else
            {
                lblTerreno.Text = "-";
            }
        }
    }
}
