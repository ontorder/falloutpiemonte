﻿namespace FP.UI.Controls
{
    partial class ucSchedaBreve
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtScheda = new System.Windows.Forms.TextBox();
            this.dgvVita = new System.Windows.Forms.DataGridView();
            this.ColId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colHp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVita)).BeginInit();
            this.SuspendLayout();
            // 
            // txtScheda
            // 
            this.txtScheda.AcceptsReturn = true;
            this.txtScheda.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtScheda.Location = new System.Drawing.Point(3, 3);
            this.txtScheda.Multiline = true;
            this.txtScheda.Name = "txtScheda";
            this.txtScheda.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtScheda.Size = new System.Drawing.Size(298, 183);
            this.txtScheda.TabIndex = 0;
            // 
            // dgvVita
            // 
            this.dgvVita.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVita.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColId,
            this.colHp});
            this.dgvVita.Location = new System.Drawing.Point(300, 3);
            this.dgvVita.Name = "dgvVita";
            this.dgvVita.RowHeadersVisible = false;
            this.dgvVita.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvVita.Size = new System.Drawing.Size(83, 183);
            this.dgvVita.TabIndex = 1;
            // 
            // ColId
            // 
            this.ColId.HeaderText = "Id";
            this.ColId.Name = "ColId";
            this.ColId.Width = 28;
            // 
            // colHp
            // 
            this.colHp.HeaderText = "PF";
            this.colHp.Name = "colHp";
            this.colHp.Width = 30;
            // 
            // ucSchedaBreve
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dgvVita);
            this.Controls.Add(this.txtScheda);
            this.Name = "ucSchedaBreve";
            this.Size = new System.Drawing.Size(387, 186);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVita)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtScheda;
        private System.Windows.Forms.DataGridView dgvVita;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colHp;
    }
}
