﻿using System.Windows.Forms;

namespace FP.UI.Controls
{
    public partial class ucMostro : UserControl
    {
        public ucMostro()
        {
            InitializeComponent();
        }

        public void Clear()
        {
            lvAbilità.Items.Clear();
            lvModifRazz.Items.Clear();
            lvTesoro.Items.Clear();
            lbAttacchi.Items.Clear();
            lbSpeciali.Items.Clear();
            lbTalenti.Items.Clear();
            lbTipo.Items.Clear();

            txAttrCar.Text = null;
            txAttrCos.Text = null;
            txAttrDes.Text = null;
            txAttrFor.Text = null;
            txAttrInt.Text = null;
            txAttrSag.Text = null;

            txBAB.Text = null;
            txBMC.Text = null;
            txDMC.Text = null;

            txCA.Text = null;
            txDadiVita.Text = null;
            txGS.Text = null;
            txId.Text = null;
            txIniziativa.Text = null;
            tbModificatore.Text = null;

            tbNome.Text = null;
            txRegno.Text = null;

            txRiduzBall.Text = null;
            txRiduzEnerg.Text = null;
            txRiduzRad.Text = null;

            cbTaglia.SelectedItem = null;

            txTesoroLibero.Text = null;

            txTsRiflessi.Text = null;
            txTsTempra.Text =   null;
            txTsVolontà.Text =  null;

            txVelBase.Text    = null;
            txVelNuotare.Text = null;
            txVelScalare.Text = null;
            txVelScavare.Text = null;
            txVelVolare.Text  = null;
        }

        public void SetMostro(Data.Models.Scheda mostro)
        {
            Clear();

            if (mostro.Abilità != null)
            foreach(var abil in mostro.Abilità)
            {
                var lvi = new ListViewItem(abil.Abilità.Nome);
                lvi.SubItems.Add(abil.Valore.ToString());
                lvi.SubItems.Add(abil.Note);
                lvi.Checked = abil.Classe;

                lvAbilità.Items.Add(lvi);
            }

            if (mostro.Attacchi != null)
            foreach(var att in mostro.Attacchi)
                lbAttacchi.Items.Add(att);

            if (mostro.Attributi != null)
            {
                txAttrCar.Text = mostro.Attributi.Carisma.ToString();
                txAttrCos.Text = mostro.Attributi.Costituzione.ToString();
                txAttrDes.Text = mostro.Attributi.Destrezza.ToString();
                txAttrFor.Text = mostro.Attributi.Forza.ToString();
                txAttrInt.Text = mostro.Attributi.Intelligenza.ToString();
                txAttrSag.Text = mostro.Attributi.Saggezza.ToString();
            }

            txBAB.Text = mostro.BonusCombat.AttaccoBase.ToString();
            txBMC.Text = mostro.BonusCombat.ManovraCombat.ToString();
            txDMC.Text = mostro.BonusCombat.DifesaCombat.ToString();

            txCA.Text = mostro.ClasseArmatura.ToString();
            txDadiVita.Text = mostro.DadiVita.ToString();
            txGS.Text = (mostro.GradoSfida.Inverso ? "1/" : System.String.Empty) + mostro.GradoSfida.Grado.ToString();
            txId.Text = mostro.Id;
            txIniziativa.Text = mostro.Iniziativa.ToString();
            tbModificatore.Text = mostro.Modificatore;

            if (mostro.ModificatoriRazziali != null)
            foreach(var mr in mostro.ModificatoriRazziali)
            {
                var lvi = new ListViewItem(mr.Abilità);
                lvi.SubItems.Add(mr.Bonus.ToString());
                lvModifRazz.Items.Add(lvi);
            }

            tbNome.Text = mostro.Nome;
            txRegno.Text = mostro.Regno.Nome; // potrei assegnare nel tag l'id?

            if (mostro.Riduzioni != null)
            {
                txRiduzBall.Text = mostro.Riduzioni.Ballistica.ToString();
                txRiduzEnerg.Text = mostro.Riduzioni.Energia.ToString();
                txRiduzRad.Text = mostro.Riduzioni.Radiazioni.ToString();
            }

            if (mostro.Speciali != null)
            foreach(var spe in mostro.Speciali)
                lbSpeciali.Items.Add(spe);

            var tagliaId = -1;
            switch (mostro.Taglia)
            {
                case Data.Models.Taglia.Colossale: tagliaId = 7; break;
                case Data.Models.Taglia.Enorme: tagliaId = 4; break;
                case Data.Models.Taglia.Gargantuesca: tagliaId = 6; break;
                case Data.Models.Taglia.Grande: tagliaId = 3; break;
                case Data.Models.Taglia.Mastodontica: tagliaId = 5; break;
                case Data.Models.Taglia.Media: tagliaId = 2; break;
                case Data.Models.Taglia.Minuscola: tagliaId = 0; break;
                case Data.Models.Taglia.Piccola: tagliaId = 1; break;
            }
            cbTaglia.SelectedIndex = tagliaId;

            if (mostro.Talenti != null)
            foreach(var tal in mostro.Talenti)
                lbTalenti.Items.Add(tal);

            if (mostro.Tesoro != null)
            {
                txTesoroLibero.Text = mostro.Tesoro.Libero.ToString();
                foreach(var tes in mostro.Tesoro.Oggetti)
                {
                    var lvi = new ListViewItem(tes.Oggetto);
                    lvi.SubItems.Add(tes.Quantità.ToString());
                    lvTesoro.Items.Add(lvi);
                }
            }

            foreach(var tipo in mostro.Tipo)
                lbTipo.Items.Add(tipo.Nome);

            if (mostro.TiriSalvezza != null)
            {
                txTsRiflessi.Text = mostro.TiriSalvezza.Riflessi.ToString();
                txTsTempra.Text = mostro.TiriSalvezza.Tempra.ToString();
                txTsVolontà.Text = mostro.TiriSalvezza.Volontà.ToString();
            }

            txVelBase.Text = mostro.Velocità.Base.ToString();
            txVelNuotare.Text = mostro.Velocità.Nuotare.ToString();
            txVelScalare.Text = mostro.Velocità.Scalare.ToString();
            txVelScavare.Text = mostro.Velocità.Scavare.ToString();
            txVelVolare.Text = mostro.Velocità.Volare.ToString();
        }
    }
}
