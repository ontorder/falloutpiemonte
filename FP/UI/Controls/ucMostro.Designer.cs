﻿namespace FP.UI.Controls
{
    partial class ucMostro
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbNome = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbModificatore = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txId = new System.Windows.Forms.TextBox();
            this.txGS = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txRegno = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.lbTipo = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txIniziativa = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txDadiVita = new System.Windows.Forms.TextBox();
            this.lblCA = new System.Windows.Forms.Label();
            this.txCA = new System.Windows.Forms.TextBox();
            this.gbBase = new System.Windows.Forms.GroupBox();
            this.cbTaglia = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txVelVolare = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txVelScavare = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txVelScalare = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txVelNuotare = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txVelBase = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txTsVolontà = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txTsRiflessi = new System.Windows.Forms.TextBox();
            this.txTsTempra = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txAttrCar = new System.Windows.Forms.TextBox();
            this.txAttrSag = new System.Windows.Forms.TextBox();
            this.txAttrInt = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txAttrCos = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txAttrDes = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txAttrFor = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txRiduzRad = new System.Windows.Forms.TextBox();
            this.txRiduzEnerg = new System.Windows.Forms.TextBox();
            this.txRiduzBall = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txDMC = new System.Windows.Forms.TextBox();
            this.txBMC = new System.Windows.Forms.TextBox();
            this.txBAB = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lbSpeciali = new System.Windows.Forms.ListBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.lbAttacchi = new System.Windows.Forms.ListBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.lbTalenti = new System.Windows.Forms.ListBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.lvAbilità = new System.Windows.Forms.ListView();
            this.chAblitàNome = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chAbilitàValore = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chAbilitàNote = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.lvModifRazz = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.txTesoroLibero = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.lvTesoro = new System.Windows.Forms.ListView();
            this.chTesoroOggetto = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chTesoroQta = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gbBase.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome";
            // 
            // tbNome
            // 
            this.tbNome.Location = new System.Drawing.Point(47, 19);
            this.tbNome.Name = "tbNome";
            this.tbNome.Size = new System.Drawing.Size(123, 20);
            this.tbNome.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(176, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Modificatore";
            // 
            // tbModificatore
            // 
            this.tbModificatore.Location = new System.Drawing.Point(247, 19);
            this.tbModificatore.Name = "tbModificatore";
            this.tbModificatore.Size = new System.Drawing.Size(123, 20);
            this.tbModificatore.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Id";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(119, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Grado Sfida";
            // 
            // txId
            // 
            this.txId.Location = new System.Drawing.Point(47, 45);
            this.txId.Name = "txId";
            this.txId.Size = new System.Drawing.Size(63, 20);
            this.txId.TabIndex = 6;
            // 
            // txGS
            // 
            this.txGS.Location = new System.Drawing.Point(188, 71);
            this.txGS.Name = "txGS";
            this.txGS.Size = new System.Drawing.Size(32, 20);
            this.txGS.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Taglia";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(2, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Regno";
            // 
            // txRegno
            // 
            this.txRegno.Location = new System.Drawing.Point(47, 71);
            this.txRegno.Name = "txRegno";
            this.txRegno.Size = new System.Drawing.Size(63, 20);
            this.txRegno.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(214, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Tipo";
            // 
            // lbTipo
            // 
            this.lbTipo.FormattingEnabled = true;
            this.lbTipo.Location = new System.Drawing.Point(247, 45);
            this.lbTipo.Name = "lbTipo";
            this.lbTipo.Size = new System.Drawing.Size(98, 43);
            this.lbTipo.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(218, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Iniziativa";
            // 
            // txIniziativa
            // 
            this.txIniziativa.Location = new System.Drawing.Point(272, 19);
            this.txIniziativa.Name = "txIniziativa";
            this.txIniziativa.Size = new System.Drawing.Size(21, 20);
            this.txIniziativa.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Dadi Vita";
            // 
            // txDadiVita
            // 
            this.txDadiVita.Location = new System.Drawing.Point(64, 19);
            this.txDadiVita.Name = "txDadiVita";
            this.txDadiVita.Size = new System.Drawing.Size(60, 20);
            this.txDadiVita.TabIndex = 17;
            // 
            // lblCA
            // 
            this.lblCA.AutoSize = true;
            this.lblCA.Location = new System.Drawing.Point(185, 48);
            this.lblCA.Name = "lblCA";
            this.lblCA.Size = new System.Drawing.Size(83, 13);
            this.lblCA.TabIndex = 18;
            this.lblCA.Text = "Classe Armatura";
            // 
            // txCA
            // 
            this.txCA.Location = new System.Drawing.Point(272, 45);
            this.txCA.Name = "txCA";
            this.txCA.Size = new System.Drawing.Size(26, 20);
            this.txCA.TabIndex = 19;
            // 
            // gbBase
            // 
            this.gbBase.Controls.Add(this.tbNome);
            this.gbBase.Controls.Add(this.label1);
            this.gbBase.Controls.Add(this.label2);
            this.gbBase.Controls.Add(this.tbModificatore);
            this.gbBase.Controls.Add(this.txId);
            this.gbBase.Controls.Add(this.label3);
            this.gbBase.Controls.Add(this.label4);
            this.gbBase.Controls.Add(this.txRegno);
            this.gbBase.Controls.Add(this.label6);
            this.gbBase.Controls.Add(this.lbTipo);
            this.gbBase.Controls.Add(this.label7);
            this.gbBase.Controls.Add(this.txGS);
            this.gbBase.Location = new System.Drawing.Point(3, 3);
            this.gbBase.Name = "gbBase";
            this.gbBase.Size = new System.Drawing.Size(415, 96);
            this.gbBase.TabIndex = 20;
            this.gbBase.TabStop = false;
            this.gbBase.Text = "Tecniche";
            // 
            // cbTaglia
            // 
            this.cbTaglia.FormattingEnabled = true;
            this.cbTaglia.Items.AddRange(new object[] {
            "Minuscola",
            "Piccola",
            "Media",
            "Grande",
            "Enorme",
            "Mastodontica",
            "Gargantuesca",
            "Colossale"});
            this.cbTaglia.Location = new System.Drawing.Point(64, 45);
            this.cbTaglia.Name = "cbTaglia";
            this.cbTaglia.Size = new System.Drawing.Size(115, 21);
            this.cbTaglia.TabIndex = 14;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbTaglia);
            this.groupBox1.Controls.Add(this.txDadiVita);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txCA);
            this.groupBox1.Controls.Add(this.lblCA);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txIniziativa);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(3, 105);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(415, 74);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Base";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txVelVolare);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txVelScavare);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txVelScalare);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.txVelNuotare);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.txVelBase);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Location = new System.Drawing.Point(3, 185);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(415, 40);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Velocità";
            // 
            // txVelVolare
            // 
            this.txVelVolare.Location = new System.Drawing.Point(372, 13);
            this.txVelVolare.Name = "txVelVolare";
            this.txVelVolare.Size = new System.Drawing.Size(27, 20);
            this.txVelVolare.TabIndex = 28;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(329, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(37, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "Volare";
            // 
            // txVelScavare
            // 
            this.txVelScavare.Location = new System.Drawing.Point(296, 13);
            this.txVelScavare.Name = "txVelScavare";
            this.txVelScavare.Size = new System.Drawing.Size(27, 20);
            this.txVelScavare.TabIndex = 26;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(243, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 13);
            this.label13.TabIndex = 25;
            this.label13.Text = "Scavare";
            // 
            // txVelScalare
            // 
            this.txVelScalare.Location = new System.Drawing.Point(210, 13);
            this.txVelScalare.Name = "txVelScalare";
            this.txVelScalare.Size = new System.Drawing.Size(27, 20);
            this.txVelScalare.TabIndex = 24;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(161, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 13);
            this.label12.TabIndex = 23;
            this.label12.Text = "Scalare";
            // 
            // txVelNuotare
            // 
            this.txVelNuotare.Location = new System.Drawing.Point(124, 13);
            this.txVelNuotare.Name = "txVelNuotare";
            this.txVelNuotare.Size = new System.Drawing.Size(27, 20);
            this.txVelNuotare.TabIndex = 22;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(73, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Nuotare";
            // 
            // txVelBase
            // 
            this.txVelBase.Location = new System.Drawing.Point(43, 13);
            this.txVelBase.Name = "txVelBase";
            this.txVelBase.Size = new System.Drawing.Size(24, 20);
            this.txVelBase.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Base";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.txTsVolontà);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.txTsRiflessi);
            this.groupBox3.Controls.Add(this.txTsTempra);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Location = new System.Drawing.Point(3, 231);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(94, 97);
            this.groupBox3.TabIndex = 23;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Tiri Salvezza";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 72);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 13);
            this.label17.TabIndex = 33;
            this.label17.Text = "Volontà";
            // 
            // txTsVolontà
            // 
            this.txTsVolontà.Location = new System.Drawing.Point(62, 69);
            this.txTsVolontà.Name = "txTsVolontà";
            this.txTsVolontà.Size = new System.Drawing.Size(24, 20);
            this.txTsVolontà.TabIndex = 32;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(16, 46);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 13);
            this.label16.TabIndex = 31;
            this.label16.Text = "Riflessi";
            // 
            // txTsRiflessi
            // 
            this.txTsRiflessi.Location = new System.Drawing.Point(62, 43);
            this.txTsRiflessi.Name = "txTsRiflessi";
            this.txTsRiflessi.Size = new System.Drawing.Size(24, 20);
            this.txTsRiflessi.TabIndex = 30;
            // 
            // txTsTempra
            // 
            this.txTsTempra.Location = new System.Drawing.Point(62, 17);
            this.txTsTempra.Name = "txTsTempra";
            this.txTsTempra.Size = new System.Drawing.Size(24, 20);
            this.txTsTempra.TabIndex = 29;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(13, 20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 13);
            this.label15.TabIndex = 18;
            this.label15.Text = "Tempra";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txAttrCar);
            this.groupBox4.Controls.Add(this.txAttrSag);
            this.groupBox4.Controls.Add(this.txAttrInt);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.txAttrCos);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.txAttrDes);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.txAttrFor);
            this.groupBox4.Location = new System.Drawing.Point(106, 231);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(210, 97);
            this.groupBox4.TabIndex = 24;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Caratteristiche";
            // 
            // txAttrCar
            // 
            this.txAttrCar.Location = new System.Drawing.Point(171, 69);
            this.txAttrCar.Name = "txAttrCar";
            this.txAttrCar.Size = new System.Drawing.Size(24, 20);
            this.txAttrCar.TabIndex = 44;
            // 
            // txAttrSag
            // 
            this.txAttrSag.Location = new System.Drawing.Point(171, 43);
            this.txAttrSag.Name = "txAttrSag";
            this.txAttrSag.Size = new System.Drawing.Size(24, 20);
            this.txAttrSag.TabIndex = 43;
            // 
            // txAttrInt
            // 
            this.txAttrInt.Location = new System.Drawing.Point(171, 17);
            this.txAttrInt.Name = "txAttrInt";
            this.txAttrInt.Size = new System.Drawing.Size(24, 20);
            this.txAttrInt.TabIndex = 42;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(121, 72);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(44, 13);
            this.label23.TabIndex = 41;
            this.label23.Text = "Carisma";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(111, 46);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(54, 13);
            this.label22.TabIndex = 40;
            this.label22.Text = "Saggezza";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(105, 20);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(60, 13);
            this.label21.TabIndex = 39;
            this.label21.Text = "Intelligenza";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 72);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(64, 13);
            this.label20.TabIndex = 38;
            this.label20.Text = "Costituzione";
            // 
            // txAttrCos
            // 
            this.txAttrCos.Location = new System.Drawing.Point(76, 69);
            this.txAttrCos.Name = "txAttrCos";
            this.txAttrCos.Size = new System.Drawing.Size(24, 20);
            this.txAttrCos.TabIndex = 37;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(16, 46);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(54, 13);
            this.label19.TabIndex = 36;
            this.label19.Text = "Destrezza";
            // 
            // txAttrDes
            // 
            this.txAttrDes.Location = new System.Drawing.Point(76, 43);
            this.txAttrDes.Name = "txAttrDes";
            this.txAttrDes.Size = new System.Drawing.Size(24, 20);
            this.txAttrDes.TabIndex = 35;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(37, 20);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(33, 13);
            this.label18.TabIndex = 34;
            this.label18.Text = "Forza";
            // 
            // txAttrFor
            // 
            this.txAttrFor.Location = new System.Drawing.Point(76, 17);
            this.txAttrFor.Name = "txAttrFor";
            this.txAttrFor.Size = new System.Drawing.Size(24, 20);
            this.txAttrFor.TabIndex = 34;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txRiduzRad);
            this.groupBox5.Controls.Add(this.txRiduzEnerg);
            this.groupBox5.Controls.Add(this.txRiduzBall);
            this.groupBox5.Controls.Add(this.label26);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.label24);
            this.groupBox5.Location = new System.Drawing.Point(322, 231);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(96, 97);
            this.groupBox5.TabIndex = 25;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Riduzioni";
            // 
            // txRiduzRad
            // 
            this.txRiduzRad.Location = new System.Drawing.Point(66, 69);
            this.txRiduzRad.Name = "txRiduzRad";
            this.txRiduzRad.Size = new System.Drawing.Size(24, 20);
            this.txRiduzRad.TabIndex = 47;
            // 
            // txRiduzEnerg
            // 
            this.txRiduzEnerg.Location = new System.Drawing.Point(66, 43);
            this.txRiduzEnerg.Name = "txRiduzEnerg";
            this.txRiduzEnerg.Size = new System.Drawing.Size(24, 20);
            this.txRiduzEnerg.TabIndex = 46;
            // 
            // txRiduzBall
            // 
            this.txRiduzBall.Location = new System.Drawing.Point(66, 17);
            this.txRiduzBall.Name = "txRiduzBall";
            this.txRiduzBall.Size = new System.Drawing.Size(24, 20);
            this.txRiduzBall.TabIndex = 45;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(8, 72);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(56, 13);
            this.label26.TabIndex = 42;
            this.label26.Text = "Radiazioni";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(10, 46);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(43, 13);
            this.label25.TabIndex = 41;
            this.label25.Text = "Energia";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(10, 20);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 13);
            this.label24.TabIndex = 40;
            this.label24.Text = "Ballistica";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txDMC);
            this.groupBox6.Controls.Add(this.txBMC);
            this.groupBox6.Controls.Add(this.txBAB);
            this.groupBox6.Controls.Add(this.label29);
            this.groupBox6.Controls.Add(this.label28);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Location = new System.Drawing.Point(3, 334);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(94, 100);
            this.groupBox6.TabIndex = 26;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Combattimento";
            // 
            // txDMC
            // 
            this.txDMC.Location = new System.Drawing.Point(43, 71);
            this.txDMC.Name = "txDMC";
            this.txDMC.Size = new System.Drawing.Size(24, 20);
            this.txDMC.TabIndex = 38;
            // 
            // txBMC
            // 
            this.txBMC.Location = new System.Drawing.Point(43, 45);
            this.txBMC.Name = "txBMC";
            this.txBMC.Size = new System.Drawing.Size(24, 20);
            this.txBMC.TabIndex = 37;
            // 
            // txBAB
            // 
            this.txBAB.Location = new System.Drawing.Point(43, 19);
            this.txBAB.Name = "txBAB";
            this.txBAB.Size = new System.Drawing.Size(24, 20);
            this.txBAB.TabIndex = 34;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(9, 22);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(28, 13);
            this.label29.TabIndex = 36;
            this.label29.Text = "BAB";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 74);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(31, 13);
            this.label28.TabIndex = 35;
            this.label28.Text = "DMC";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(7, 48);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(30, 13);
            this.label27.TabIndex = 34;
            this.label27.Text = "BMC";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.lbSpeciali);
            this.groupBox7.Location = new System.Drawing.Point(103, 334);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(315, 100);
            this.groupBox7.TabIndex = 27;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Speciali";
            // 
            // lbSpeciali
            // 
            this.lbSpeciali.FormattingEnabled = true;
            this.lbSpeciali.Location = new System.Drawing.Point(6, 19);
            this.lbSpeciali.Name = "lbSpeciali";
            this.lbSpeciali.Size = new System.Drawing.Size(303, 69);
            this.lbSpeciali.TabIndex = 14;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.lbAttacchi);
            this.groupBox8.Location = new System.Drawing.Point(3, 440);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(237, 87);
            this.groupBox8.TabIndex = 28;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Attacchi";
            // 
            // lbAttacchi
            // 
            this.lbAttacchi.FormattingEnabled = true;
            this.lbAttacchi.Location = new System.Drawing.Point(6, 19);
            this.lbAttacchi.Name = "lbAttacchi";
            this.lbAttacchi.Size = new System.Drawing.Size(225, 56);
            this.lbAttacchi.TabIndex = 15;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.lbTalenti);
            this.groupBox9.Location = new System.Drawing.Point(250, 440);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(168, 87);
            this.groupBox9.TabIndex = 29;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Talenti";
            // 
            // lbTalenti
            // 
            this.lbTalenti.FormattingEnabled = true;
            this.lbTalenti.Location = new System.Drawing.Point(6, 19);
            this.lbTalenti.Name = "lbTalenti";
            this.lbTalenti.Size = new System.Drawing.Size(156, 56);
            this.lbTalenti.TabIndex = 16;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.lvAbilità);
            this.groupBox10.Location = new System.Drawing.Point(3, 533);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(415, 173);
            this.groupBox10.TabIndex = 30;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Abilità";
            // 
            // lvAbilità
            // 
            this.lvAbilità.CheckBoxes = true;
            this.lvAbilità.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chAblitàNome,
            this.chAbilitàValore,
            this.chAbilitàNote});
            this.lvAbilità.FullRowSelect = true;
            this.lvAbilità.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvAbilità.HideSelection = false;
            this.lvAbilità.Location = new System.Drawing.Point(6, 19);
            this.lvAbilità.Name = "lvAbilità";
            this.lvAbilità.Size = new System.Drawing.Size(403, 146);
            this.lvAbilità.TabIndex = 0;
            this.lvAbilità.UseCompatibleStateImageBehavior = false;
            this.lvAbilità.View = System.Windows.Forms.View.Details;
            // 
            // chAblitàNome
            // 
            this.chAblitàNome.Text = "Nome";
            this.chAblitàNome.Width = 141;
            // 
            // chAbilitàValore
            // 
            this.chAbilitàValore.Text = "#";
            this.chAbilitàValore.Width = 27;
            // 
            // chAbilitàNote
            // 
            this.chAbilitàNote.Text = "Note";
            this.chAbilitàNote.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.chAbilitàNote.Width = 203;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.lvModifRazz);
            this.groupBox11.Location = new System.Drawing.Point(3, 712);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(415, 118);
            this.groupBox11.TabIndex = 31;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Modificatori Razziali";
            // 
            // lvModifRazz
            // 
            this.lvModifRazz.CheckBoxes = true;
            this.lvModifRazz.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lvModifRazz.FullRowSelect = true;
            this.lvModifRazz.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvModifRazz.HideSelection = false;
            this.lvModifRazz.Location = new System.Drawing.Point(6, 19);
            this.lvModifRazz.Name = "lvModifRazz";
            this.lvModifRazz.Size = new System.Drawing.Size(403, 89);
            this.lvModifRazz.TabIndex = 0;
            this.lvModifRazz.UseCompatibleStateImageBehavior = false;
            this.lvModifRazz.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Nome";
            this.columnHeader1.Width = 141;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "#";
            this.columnHeader2.Width = 27;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Note";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.columnHeader3.Width = 203;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.txTesoroLibero);
            this.groupBox12.Controls.Add(this.label30);
            this.groupBox12.Controls.Add(this.lvTesoro);
            this.groupBox12.Location = new System.Drawing.Point(3, 836);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(415, 143);
            this.groupBox12.TabIndex = 32;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Tesoro";
            // 
            // txTesoroLibero
            // 
            this.txTesoroLibero.Location = new System.Drawing.Point(49, 19);
            this.txTesoroLibero.Name = "txTesoroLibero";
            this.txTesoroLibero.Size = new System.Drawing.Size(45, 20);
            this.txTesoroLibero.TabIndex = 2;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(7, 22);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(36, 13);
            this.label30.TabIndex = 1;
            this.label30.Text = "Libero";
            // 
            // lvTesoro
            // 
            this.lvTesoro.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chTesoroOggetto,
            this.chTesoroQta});
            this.lvTesoro.FullRowSelect = true;
            this.lvTesoro.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvTesoro.HideSelection = false;
            this.lvTesoro.Location = new System.Drawing.Point(6, 45);
            this.lvTesoro.Name = "lvTesoro";
            this.lvTesoro.Size = new System.Drawing.Size(403, 90);
            this.lvTesoro.TabIndex = 0;
            this.lvTesoro.UseCompatibleStateImageBehavior = false;
            this.lvTesoro.View = System.Windows.Forms.View.Details;
            // 
            // chTesoroOggetto
            // 
            this.chTesoroOggetto.Text = "Nome";
            this.chTesoroOggetto.Width = 141;
            // 
            // chTesoroQta
            // 
            this.chTesoroQta.Text = "#";
            this.chTesoroQta.Width = 27;
            // 
            // ucMostro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox12);
            this.Controls.Add(this.groupBox11);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbBase);
            this.Name = "ucMostro";
            this.Size = new System.Drawing.Size(424, 983);
            this.gbBase.ResumeLayout(false);
            this.gbBase.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbNome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbModificatore;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txId;
        private System.Windows.Forms.TextBox txGS;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txRegno;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox lbTipo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txIniziativa;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txDadiVita;
        private System.Windows.Forms.Label lblCA;
        private System.Windows.Forms.TextBox txCA;
        private System.Windows.Forms.GroupBox gbBase;
        private System.Windows.Forms.ComboBox cbTaglia;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txVelScavare;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txVelScalare;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txVelNuotare;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txVelBase;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txVelVolare;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txTsVolontà;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txTsRiflessi;
        private System.Windows.Forms.TextBox txTsTempra;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txAttrCar;
        private System.Windows.Forms.TextBox txAttrSag;
        private System.Windows.Forms.TextBox txAttrInt;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txAttrCos;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txAttrDes;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txAttrFor;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txRiduzRad;
        private System.Windows.Forms.TextBox txRiduzEnerg;
        private System.Windows.Forms.TextBox txRiduzBall;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txDMC;
        private System.Windows.Forms.TextBox txBMC;
        private System.Windows.Forms.TextBox txBAB;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ListBox lbSpeciali;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ListBox lbAttacchi;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ListBox lbTalenti;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.ListView lvAbilità;
        private System.Windows.Forms.ColumnHeader chAblitàNome;
        private System.Windows.Forms.ColumnHeader chAbilitàValore;
        private System.Windows.Forms.ColumnHeader chAbilitàNote;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.ListView lvModifRazz;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.TextBox txTesoroLibero;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ListView lvTesoro;
        private System.Windows.Forms.ColumnHeader chTesoroOggetto;
        private System.Windows.Forms.ColumnHeader chTesoroQta;
    }
}
