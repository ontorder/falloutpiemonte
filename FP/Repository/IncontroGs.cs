﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace FP.Repository
{
    public class IncontroGs
    {
        public Data.Models.IncontroGs[] IncontriGs;
        public Dictionary<float, Data.Models.IncontroGs> DicGsIncontroGs;
        private string _xmlFilePath;

        public void Init(string xmlFilePath)
        {
            _xmlFilePath = xmlFilePath;

            var xdoc = new XmlDocument();
            xdoc.Load(_xmlFilePath);

            var listaIncontro = new List<Data.Models.IncontroGs>();

            foreach (XmlNode nodeIncontro in xdoc.DocumentElement.ChildNodes)
            {
                var ogg = Data.Parser.IncontroGs.ParseFromXml(nodeIncontro);
                listaIncontro.Add(ogg);
            }

            IncontriGs = listaIncontro.ToArray();
            DicGsIncontroGs = IncontriGs.ToDictionary(igs => (float)igs.GS);
        }
    }
}
