﻿using System.Collections.Generic;
using System.Xml;

namespace FP.Repository
{
    public class Mostro
    {
        private string _xmlDocPath;
        private Data.Context _context;
        private Data.Models.Scheda[] _mostri;

        public Data.Models.Scheda[] Mostri => _mostri;
        public Dictionary<string, Data.Models.Scheda> IdMostro { private set; get; }

        public Mostro(Data.Context context) =>
            _context = context;

        public void Init(string xmlDocPath)
        {
            _xmlDocPath = xmlDocPath;

            var xdoc = new XmlDocument();
            xdoc.Load(_xmlDocPath);

            var mostri = new List<Data.Models.Scheda>();

            foreach(XmlNode nodeMostro in xdoc.DocumentElement.ChildNodes)
            {
                var mostro = Data.Parser.Mostro.FromXml(nodeMostro);

                var regnoId = _context.Regni.DicRegniReverse[mostro.Regno.Nome];
                mostro.Regno = (regnoId, mostro.Regno.Nome);

                var tipi = new List<(int, string)>();
                foreach(var tipo in mostro.Tipo)
                {
                    var tipoId = _context.TipiMostri.DicTipiMostriReverse[tipo.Nome];
                    tipi.Add((tipoId, tipo.Nome));
                }
                mostro.Tipo = tipi.ToArray();

                mostri.Add(mostro);
            }

            _mostri = mostri.ToArray();
            IdMostro = System.Linq.Enumerable.ToDictionary(_mostri, _ => _.Id);
        }
    }
}
