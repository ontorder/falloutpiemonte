﻿using System.Collections.Generic;
using System.Xml;

namespace FP.Repository
{
    public class GerarchiaOggetto
    {
        public Data.Models.GerarchiaOggetto[] GerarchieOggetti;
        private string _xmlFilePath;

        public void Init(string xmlFilePath)
        {
            _xmlFilePath = xmlFilePath;

            var xdoc = new XmlDocument();
            xdoc.Load(_xmlFilePath);

            var listaOgg = new List<Data.Models.GerarchiaOggetto>();

            foreach (XmlNode nodeOgg in xdoc.DocumentElement.ChildNodes)
            {
                if (nodeOgg.NodeType != XmlNodeType.Element) continue;
                if (nodeOgg.Name != "gerarchia") continue;
                var ogg = Data.Parser.GerarchiaOggetto.ParseFromXml(nodeOgg);
                listaOgg.Add(ogg);
            }

            GerarchieOggetti = listaOgg.ToArray();
        }
    }
}
