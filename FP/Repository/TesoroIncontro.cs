﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace FP.Repository
{
    public class TesoroIncontro
    {
        private string _xmlDocPath;
        public Data.Models.TesoroIncontro[] TesoriIncontro;
        private Data.Context _context;

        public TesoroIncontro(Data.Context context) =>
            _context = context;

        public void Init(string xmlDocPath)
        {
            _xmlDocPath = xmlDocPath;

            var xdoc = new XmlDocument();
            xdoc.Load(_xmlDocPath);

            var list = new List<Data.Models.TesoroIncontro>();
            var cgo = _context.GenerazioneTesori.GenerazioneTesori;

            foreach(XmlNode nodeTi in xdoc.DocumentElement.ChildNodes)
            {
                if (nodeTi.NodeType != XmlNodeType.Element) continue;
                if (nodeTi.Name != "tesoro_incontro") continue;

                var ti = Data.Parser.TesoroIncontro.ParseFromXml(nodeTi);

                foreach(var tes in ti.Tesori)
                {
                    switch (tes)
                    {
                        case Data.Models.TesoroIncontro.TesoroCgo tcgo:
                        {
                            if (!cgo.Any(gt => gt.Id == tcgo.GenerazioneTesoroId))
                                throw new System.Exception("generazione-tesoro inesistente");
                            break;
                        }
                    }
                }

                list.Add(ti);
            }

            TesoriIncontro = list.ToArray();
        }
    }
}
