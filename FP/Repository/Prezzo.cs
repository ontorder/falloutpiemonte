﻿using FP.Data;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace FP.Repository
{
    public class Prezzo
    {
        private string _xmlDocPath;
        private Context _context;

        public Data.Models.Prezzo[] Prezzi;
        public Dictionary<string, Data.Models.Prezzo> DicPrezzi;

        public Prezzo(Context context) =>
            _context = context;

        public void Init(string docFilePath)
        {
            _xmlDocPath = docFilePath;
            var xdoc = new XmlDocument();
            xdoc.Load(_xmlDocPath);

            Prezzi = (
                from XmlNode nodePrezzo
                in xdoc.DocumentElement.ChildNodes
                select Data.Parser.Prezzo.ParseFromXml(nodePrezzo)
            ).ToArray();

            foreach(var p in Prezzi)
            {
                if (!_context.Oggetti.DicOggetti.ContainsKey(p.Id))
                    throw new System.Exception("oggetto inesistente");
            }

            DicPrezzi = Prezzi.ToDictionary(pr => pr.Id);
        }
    }
}
