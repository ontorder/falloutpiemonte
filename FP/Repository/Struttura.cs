﻿using System.Collections.Generic;
using System.Linq;

namespace FP.Repository
{
    public class Struttura
    {
        public Data.Models.Struttura[] Strutture;
        public Dictionary<string, Data.Models.Struttura> DicStrutture => _dicStrutture;
        private Dictionary<string, Data.Models.Struttura> _dicStrutture;

        public void Init(string filePath)
        {
            Strutture = Data.Parser.Struttura.ParseFromXml(filePath);
            _dicStrutture = Strutture.ToDictionary(_ => _.Nome);
        }
    }
}
