﻿using System.Collections.Generic;
using System.Linq;

namespace FP.Repository
{
    public class Terreno
    {
        public Data.Models.Terreno[] Terreni;
        public Dictionary<string, Data.Models.Terreno> NomeTerreni;
        public Dictionary<int, Data.Models.Terreno> IdTerreni;

        public void Init(string filePath)
        {
            Terreni = Data.Parser.Terreno.ParseFromXml(filePath);

            NomeTerreni = Terreni.ToDictionary(_ => _.Nome);
            IdTerreni = Terreni.ToDictionary(_ => _.Id);
        }
    }
}
