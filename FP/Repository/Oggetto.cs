﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace FP.Repository
{
    public class Oggetto
    {
        private string _xmlFilePath;
        private Data.Context _context;

        public Data.Models.Oggetto[] Oggetti;
        public Dictionary<string, Data.Models.Oggetto> DicOggetti;

        public Oggetto(Data.Context context) =>
            _context = context;

        public void Init(string xmlFilePath)
        {
            _xmlFilePath = xmlFilePath;

            var xdoc = new XmlDocument();
            xdoc.Load(_xmlFilePath);

            var listaOgg = new List<Data.Models.Oggetto>();

            foreach (XmlNode nodeOgg in xdoc.DocumentElement.ChildNodes)
            {
                if (!(nodeOgg is XmlElement)) continue;
                XmlElement elOgg = (XmlElement)nodeOgg;
                if (elOgg.Name != "oggetto") continue;
                var ogg = Data.Parser.Oggetto.ParseFromXml(elOgg);
                if (!_context.GerarchieOggetti.GerarchieOggetti.Any(go => go.Nome == ogg.Discriminator))
                    throw new System.Exception("gerarchia inesistente");
                listaOgg.Add(ogg);
            }

            Oggetti = listaOgg.ToArray();
            DicOggetti = Oggetti.ToDictionary(o => o.Id);
        }
    }
}
