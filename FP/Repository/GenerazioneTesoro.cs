﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace FP.Repository
{
    public class GenerazioneTesoro
    {
        private string _xmlFilePath;
        private Data.Context _context;
        public Data.Models.GenerazioneTesoro[] GenerazioneTesori;

        public GenerazioneTesoro(Data.Context context) =>
            _context = context;

        public void Init(string xmlFilePath)
        {
            _xmlFilePath = xmlFilePath;

            var xdoc = new XmlDocument();
            xdoc.Load(_xmlFilePath);

            var listaGt = new List<Data.Models.GenerazioneTesoro>();

            foreach (XmlNode nodeGt in xdoc.DocumentElement.ChildNodes)
            {
                if (nodeGt.NodeType != XmlNodeType.Element) continue;
                if (nodeGt.Name != "tesoro") continue;

                var gt = Data.Parser.GenerazioneTesoro.ParseFromXml(nodeGt);

                foreach (var elem in gt.Elementi)
                {
                    switch (elem)
                    {
                        case Data.Models.GenerazioneTesoroLivello liv:
                        {
                            if (!_context.GerarchieOggetti.GerarchieOggetti.Any(go => go.Path == liv.GerarchiaPath))
                                throw new System.Exception("gerarchia sconosciuta");
                            break;
                        }

                        case Data.Models.GenerazioneTesoroOggetto ogg:
                        {
                            if (!_context.Oggetti.DicOggetti.ContainsKey(ogg.OggettoId))
                                throw new System.Exception("oggetto inesistente");
                            break;
                        }
                    }
                }

                listaGt.Add(gt);
            }

            GenerazioneTesori = listaGt.ToArray();
        }
    }
}
