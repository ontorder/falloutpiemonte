﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace FP.Repository
{
    public class RegniMostro
    {
        private string _xmlDocPath;
        private (int Id, string Nome)[] _regni;
        private Dictionary<int, string> _dicRegni;
        private Dictionary<string, int> _dicRegni2;

        public (int Id, string Nome)[] Regni => _regni;
        public Dictionary<int, string> DicRegni => _dicRegni;
        public Dictionary<string, int> DicRegniReverse => _dicRegni2;

        public void Init(string xmlDocPath)
        {
            _xmlDocPath = xmlDocPath;

            var xdoc = new XmlDocument();
            xdoc.Load(_xmlDocPath);

            _regni = (
                from XmlNode nodoRegno
                in xdoc.DocumentElement.ChildNodes
                select (int.Parse(nodoRegno.Attributes["id"].Value), nodoRegno.InnerText)
            ).ToArray();

            _dicRegni = _regni.ToDictionary(_ => _.Id, _ => _.Nome);
            _dicRegni2 = _regni.ToDictionary(_ => _.Nome, _ => _.Id);
        }
    }
}
