﻿using System.Collections.Generic;
using System.Linq;

namespace FP.Repository
{
    public class TerrenoSottotipo
    {
        public class SottotipoId
        {
            public const int Difficile = 3;
            public const int Facile = 4;
            public const int TerrenoDiCaccia = 5;
            public const int Accampamento = 6;
        }

        public Data.Models.TerrenoSottotipo[] TerrenoSottotipi;
        public Dictionary<string, Data.Models.TerrenoSottotipo> DicTerrenoSottotipi => _dicSottotipi;
        private Dictionary<string, Data.Models.TerrenoSottotipo> _dicSottotipi;

        public void Init(string filePath)
        {
            TerrenoSottotipi = Data.Parser.TerrenoSottotipo.ParseFromXml(filePath);
            _dicSottotipi = TerrenoSottotipi.ToDictionary(_ => _.Nome);
        }
    }
}
