﻿using System.Collections.Generic;

namespace FP.Repository
{
    static public class Abilità
    {
        static public Data.Models.Abilità Acrobazia = new Data.Models.Abilità("Acrobazia", "DES");
        static public Data.Models.Abilità AddestrareAnimali = new Data.Models.Abilità("Addestrare Animali", "CAR");
        static public Data.Models.Abilità Artigianato_ArmiDaMischia = new Data.Models.Abilità("Artigianato (Armi da Mischia)", "INT");
        static public Data.Models.Abilità Artigianato_ArmiDaFuoco = new Data.Models.Abilità("Artigianato (Armi da Fuoco)", "INT");
        static public Data.Models.Abilità Artigianato_Armature = new Data.Models.Abilità("Artigianato (Armature)", "INT");
        static public Data.Models.Abilità Artigianato_Cucina = new Data.Models.Abilità("Artigianato (Cucina)", "INT");
        static public Data.Models.Abilità Artigianato_Droghe = new Data.Models.Abilità("Artigianato (Droghe)", "INT");
        static public Data.Models.Abilità Artigianato_Medicina = new Data.Models.Abilità("Artigianato (Medicina)", "INT");
        static public Data.Models.Abilità Artigianato_Chimica = new Data.Models.Abilità("Artigianato (Chimica)", "INT");
        static public Data.Models.Abilità ArtistaDellaFuga = new Data.Models.Abilità("Artista della Fuga", "DES");
        static public Data.Models.Abilità Camuffare = new Data.Models.Abilità("Camuffare", "CAR");
        static public Data.Models.Abilità Cavalcare = new Data.Models.Abilità("Cavalcare", "DES");
        static public Data.Models.Abilità Conoscenze_Locali = new Data.Models.Abilità("Conoscenze (Locali)", "INT");
        static public Data.Models.Abilità Conoscenze_Natura = new Data.Models.Abilità("Conoscenze (Natura)", "INT");
        static public Data.Models.Abilità Conoscenze_Geografia = new Data.Models.Abilità("Conoscenze (Geografia)", "INT");
        static public Data.Models.Abilità Conoscenze_VecchioMondo = new Data.Models.Abilità("Conoscenze (Vecchio Mondo)", "INT");
        static public Data.Models.Abilità Conoscenze_ZonaContaminata = new Data.Models.Abilità("Conoscenze (Zona Contaminata)", "INT");
        static public Data.Models.Abilità Diplomazia = new Data.Models.Abilità("Diplomazia", "CAR");
        static public Data.Models.Abilità Furtività = new Data.Models.Abilità("Furtività", "DES");
        static public Data.Models.Abilità Guarire = new Data.Models.Abilità("Guarire", "SAG");
        static public Data.Models.Abilità Informatica_DisattivareCongegni = new Data.Models.Abilità("Informatica / Disattivare Congegni", "INT");
        static public Data.Models.Abilità Intimidire = new Data.Models.Abilità("Intimidire", "CAR");
        static public Data.Models.Abilità Intrattenere = new Data.Models.Abilità("Intrattenere", "CAR");
        static public Data.Models.Abilità Intuizione = new Data.Models.Abilità("Intuizione", "SAG");
        static public Data.Models.Abilità Linguistica = new Data.Models.Abilità("Linguistica", "INT");
        static public Data.Models.Abilità Nuotare = new Data.Models.Abilità("Nuotare", "FOR");
        static public Data.Models.Abilità Percezione = new Data.Models.Abilità("Percezione", "SAG");
        static public Data.Models.Abilità Pilotare_Moto = new Data.Models.Abilità("Pilotare (Moto)", "DES");
        static public Data.Models.Abilità Pilotare_Auto = new Data.Models.Abilità("Pilotare (Auto)", "DES");
        static public Data.Models.Abilità Pilotare_Natanti = new Data.Models.Abilità("Pilotare (Natanti)", "DES");
        static public Data.Models.Abilità Professione = new Data.Models.Abilità("Professione", "SAG");
        static public Data.Models.Abilità Raggirare = new Data.Models.Abilità("Raggirare", "CAR");
        static public Data.Models.Abilità RapiditàDiMano = new Data.Models.Abilità("Rapidità di Mano", "DES");
        static public Data.Models.Abilità Scalare = new Data.Models.Abilità("Scalare", "FOR");
        static public Data.Models.Abilità Sopravvivenza = new Data.Models.Abilità("Sopravvivenza", "SAG");
        static public Data.Models.Abilità Valutare = new Data.Models.Abilità("Valutare", "INT");

        static public Dictionary<string, Data.Models.Abilità> NomeAbilità { get; private set; }

        static private void DicAddAbilità(Data.Models.Abilità abil) =>
            NomeAbilità.Add(abil.Nome.ToLower(), abil);

        static Abilità()
        {
            NomeAbilità = new Dictionary<string, Data.Models.Abilità>();

            DicAddAbilità(Acrobazia);
            DicAddAbilità(AddestrareAnimali);
            DicAddAbilità(Artigianato_ArmiDaMischia);
            DicAddAbilità(Artigianato_ArmiDaFuoco);
            DicAddAbilità(Artigianato_Armature);
            DicAddAbilità(Artigianato_Chimica);
            DicAddAbilità(Artigianato_Cucina);
            DicAddAbilità(Artigianato_Droghe);
            DicAddAbilità(Artigianato_Medicina);
            DicAddAbilità(ArtistaDellaFuga);
            DicAddAbilità(Camuffare);
            DicAddAbilità(Cavalcare);
            DicAddAbilità(Conoscenze_Geografia);
            DicAddAbilità(Conoscenze_Natura);
            DicAddAbilità(Conoscenze_Locali);
            DicAddAbilità(Conoscenze_VecchioMondo);
            DicAddAbilità(Conoscenze_ZonaContaminata);
            DicAddAbilità(Diplomazia);
            DicAddAbilità(Furtività);
            DicAddAbilità(Guarire);
            DicAddAbilità(Informatica_DisattivareCongegni);
            DicAddAbilità(Intimidire);
            DicAddAbilità(Intrattenere);
            DicAddAbilità(Intuizione);
            DicAddAbilità(Linguistica);
            DicAddAbilità(Nuotare);
            DicAddAbilità(Percezione);
            DicAddAbilità(Pilotare_Auto);
            DicAddAbilità(Pilotare_Moto);
            DicAddAbilità(Pilotare_Natanti);
            DicAddAbilità(Professione);
            DicAddAbilità(Raggirare);
            DicAddAbilità(RapiditàDiMano);
            DicAddAbilità(Scalare);
            DicAddAbilità(Sopravvivenza);
            DicAddAbilità(Valutare);
        }
    }
}
