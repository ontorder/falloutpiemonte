﻿using FP.Data;
using FP.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace FP.Repository
{
    public class Mappa
    {
        private string storageFilePath;

        // potrebbero essere float
        public int MaxX = int.MinValue;
        public int MaxY = int.MinValue;
        public int MinX = int.MaxValue;
        public int MinY = int.MaxValue;

        private Cella[] celle = null;
        private SortedList<int, SortedList<int, Cella>> dicCelle;
        private Context _context;

        public Mappa(Context context) =>
            _context = context;

        // il repository dovrebbe usare un driver per l'accesso all'xml
        public void FakeInitDriver(string xmlDocPath)
        {
            storageFilePath = xmlDocPath;

            var xdoc = new XmlDocument();
            xdoc.Load(xmlDocPath);

            var nodiCella = xdoc.SelectNodes("//cella");
            var list = new List<Cella>();
            dicCelle = new SortedList<int, SortedList<int, Cella>>();

            foreach (XmlNode nodoCella in nodiCella)
            {
                var parsedCella = Data.Parser.Cella.FromXml(nodoCella);

                var checkErr = CheckCella(parsedCella);
                if (checkErr != null)
                    throw checkErr;

                list.Add(parsedCella);
                if (!dicCelle.ContainsKey(parsedCella.X)) dicCelle[parsedCella.X] = new SortedList<int, Cella>();
                if (dicCelle.ContainsKey(parsedCella.X) && dicCelle[parsedCella.X].ContainsKey(parsedCella.Y))
                    throw new Exception($"cella {parsedCella.X}:{parsedCella.Y} duplicata!");
                dicCelle[parsedCella.X][parsedCella.Y] = parsedCella;

                if (parsedCella.X > MaxX) MaxX = parsedCella.X;
                if (parsedCella.Y > MaxY) MaxY = parsedCella.Y;
                if (parsedCella.X < MinX) MinX = parsedCella.X;
                if (parsedCella.Y < MinY) MinY = parsedCella.Y;
            }

            celle = list.ToArray();
        }

        public Cella[] GetCelle() => celle;

        public Cella GetCella(int x, int y) {
            if (!dicCelle.ContainsKey(x)) return null;
            if (!dicCelle[x].ContainsKey(y)) return null;
            return dicCelle[x][y];
        }

        public void VerificaCoordinate()
        {
            var celle = new Cella[2 + MaxY - MinY, 2 + MaxX - MinX];

            foreach(var itemcella in celle)
            {
                var coord_x = itemcella.X - MinX;
                var coord_y = itemcella.Y - MinY;

                if (celle[coord_y, coord_x] != null)
                {
                    throw new Exception("trovato errore in coordinate");
                }
                else
                {
                    celle[coord_y, coord_x] = itemcella;
                }
            }
        }

        public void VerificaOrdinali()
        {
            var settori = new Dictionary<string, HashSet<int>>();

            foreach (var itemcella in celle)
            {
                if (!settori.ContainsKey(itemcella.Settore))
                    settori.Add(itemcella.Settore, new HashSet<int>());

                if (settori[itemcella.Settore].Contains(itemcella.Ordinale))
                    throw new Exception("trovato errore in ordinali");
                else
                    settori[itemcella.Settore].Add(itemcella.Ordinale);
            }
        }

        public void Salva()
        {
            var xdoc = new XmlDocument();
            var root = xdoc.CreateElement("celle");
            xdoc.AppendChild(root);

            var settori_id = celle.Select(cella => cella.SettoreId).Distinct();
            var settori = settori_id
                .Select(sid => celle.First(cella => cella.SettoreId == sid))
                .Select(cella => new { cella.SettoreId, cella.Settore, cella.SettoreColore })
            ;

            foreach(var settore in settori)
            {
                var settoreCelle = celle.Where(cella => cella.SettoreId == settore.SettoreId);

                var nodeSettore = xdoc.CreateElement("settore");
                var attrId = xdoc.CreateAttribute("id");
                var attrNome = xdoc.CreateAttribute("nome");
                var attrColr = xdoc.CreateAttribute("colore");
                attrId.Value = settore.SettoreId.ToString();
                attrNome.Value = settore.Settore;
                attrColr.Value = settore.SettoreColore;
                nodeSettore.Attributes.Append(attrId);
                nodeSettore.Attributes.Append(attrNome);
                nodeSettore.Attributes.Append(attrColr);
                root.AppendChild(nodeSettore);

                foreach(var cella in settoreCelle)
                {
                    var nodeCella = Data.Parser.Cella.ToXml(xdoc, cella);
                    nodeSettore.AppendChild(nodeCella);
                }
            }

            xdoc.Save(storageFilePath);
        }

        private Exception CheckCella(Cella cella)
        {
            if (cella.Sottotipo != null)
            {
                if (!_context.Sottotipi.DicTerrenoSottotipi.ContainsKey(cella.Sottotipo))
                    return new KeyNotFoundException($"TerrenoSottotipo inesistente: '{cella.Sottotipo}'");

                var sottotipo = _context.Sottotipi.DicTerrenoSottotipi[cella.Sottotipo];
                cella.SottotipoId = sottotipo.Id;
            }

            if (cella.Strutture != null)
            {
                for(int j = 0; j < cella.Strutture.Count; ++j)
                {
                    if (!_context.Strutture.DicStrutture.ContainsKey(cella.Strutture[j].Nome))
                        return new KeyNotFoundException($"Struttura inesistente: '{cella.Strutture[j].Nome}'");

                    var found_struttura = _context.Strutture.DicStrutture[cella.Strutture[j].Nome];
                    cella.Strutture[j] = (found_struttura.Nome, found_struttura.Id);
                }
            }

            if (_context.Terreni.NomeTerreni.ContainsKey(cella.Terreno))
            {
                var terreno = _context.Terreni.NomeTerreni[cella.Terreno];
                cella.TerrenoId = terreno.Id;
            }
            else
                return new KeyNotFoundException($"Terreno inesistente: '{cella.Terreno}'");

            if (!String.IsNullOrEmpty(cella.Proprietario))
            {
                if (!_context.Fazioni.IdFazione.ContainsKey(cella.Proprietario))
                    throw new Exception($"cella {cella.X}:{cella.Y} ha proprietario inesistente");

                cella.ProprietarioId = _context.Fazioni.IdFazione[cella.Proprietario].Id;
            }

            return null;
        }
    }
}
