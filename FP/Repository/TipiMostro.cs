﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace FP.Repository
{
    public class TipiMostro
    {
        private string _xmlDocPath;
        private (int Id, string Nome)[] _tipiMostri;
        private Dictionary<int, string> _dicTipiMostri;
        private Dictionary<string, int> _dicTipiMostri2;

        public (int Id, string Nome)[] TipiMostri => _tipiMostri;
        public Dictionary<int, string> DicTipiMostri => _dicTipiMostri;
        public Dictionary<string, int> DicTipiMostriReverse => _dicTipiMostri2;

        public void Init(string xmlDocPath)
        {
            _xmlDocPath = xmlDocPath;

            var xdoc = new XmlDocument();
            xdoc.Load(_xmlDocPath);

            _tipiMostri = (
                from XmlNode nodoTipo
                in xdoc.DocumentElement.ChildNodes
                select (int.Parse(nodoTipo.Attributes["id"].Value), nodoTipo.Attributes["nome"].Value)
            ).ToArray();

            _dicTipiMostri = _tipiMostri.ToDictionary(_ => _.Id, _ => _.Nome);
            _dicTipiMostri2 = _tipiMostri.ToDictionary(_ => _.Nome, _ => _.Id);
        }
    }
}
