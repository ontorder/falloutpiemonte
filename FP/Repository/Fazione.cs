﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace FP.Repository
{
    public class Fazione
    {
        public Data.Models.Fazione[] Fazioni;
        public Dictionary<string, Data.Models.Fazione> IdFazione;

        private string _xmlFilePath;

        public void Init(string filePath)
        {
            _xmlFilePath = filePath;

            var xdoc = new XmlDocument();
            xdoc.Load(_xmlFilePath);

            var listaFazioni = new List<Data.Models.Fazione>();

            foreach (XmlNode nodeIncontro in xdoc.DocumentElement.ChildNodes)
            {
                var fazione = Data.Parser.Fazione.ParseFromXml(nodeIncontro);
                listaFazioni.Add(fazione);
            }

            Fazioni = listaFazioni.ToArray();
            IdFazione = listaFazioni.ToDictionary(faz => faz.Nome);
        }
    }
}
