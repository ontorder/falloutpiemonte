﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace FP.Repository
{
    public class MostroAmbienti
    {
        private readonly Data.Context _context;
        private string _xmlDocPath;

        public Data.Models.MostroAmbienti[] MostriAmbienti;
        public Dictionary<string, Data.Models.MostroAmbienti> TipoMostroAmbiente;

        public MostroAmbienti(Data.Context context)
        {
            _context = context;
        }

        public void Init(string xmlDocPath)
        {
            _xmlDocPath = xmlDocPath;

            var xdoc = new XmlDocument();
            xdoc.Load(_xmlDocPath);

            var list = new List<Data.Models.MostroAmbienti>();

            foreach(XmlNode nodoMa in xdoc.DocumentElement.ChildNodes)
            {
                if (nodoMa.Name != "mostro_ambienti") continue;

                var ma = Data.Parser.MostroAmbienti.ParseFromXml(nodoMa);
                ma.MostroTipoId = _context.TipiMostri.DicTipiMostriReverse[ma.MostroTipo];

                if (ma.Terreni != null)
                {
                    var terreni = new List<(string, int)>();
                    foreach(var terreno in ma.Terreni)
                        terreni.Add((terreno.Terreno, _context.Terreni.NomeTerreni[terreno.Terreno].Id));
                    ma.Terreni = terreni.ToArray();
                }

                if (ma.Strutture != null)
                {
                    var strutture = new List<(string, int)>();
                    foreach(var struttura in ma.Strutture)
                        strutture.Add((struttura.Struttura, _context.Strutture.DicStrutture[struttura.Struttura].Id));
                    ma.Strutture = strutture.ToArray();
                }

                list.Add(ma);
            }

            MostriAmbienti = list.ToArray();
            TipoMostroAmbiente = list.ToDictionary(_ => _.MostroTipo);
        }
    }
}
