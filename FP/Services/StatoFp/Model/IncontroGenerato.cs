﻿using FP.Data.Models;
using System;

namespace FP.Services.StatoFp.Model
{
    public class IncontroGenerato
    {
        public int Id;
        public DateTime Generato;
        public Scheda[] Mostri;
        public (string Id, int Qta)[] Tesoro;
        // campo per dire solo mostri, solo tesoro, ecc?
        public (int X, int Y) Cella;
    }
}
