﻿using FP.Services.StatoFp.Model;
using System;

namespace FP.Services.StatoFp
{
    partial class StatoFpService
    {
        public Action<IncontroGenerato> IncontroAddedEvent;
        public Action IncontroClearedEvent;

        public void AddIncontro(IncontroGenerato incontro)
        {
            incontro.Id = _incontriGenerati.Count;
            _incontriGenerati.Add(incontro.Id, incontro);
            IncontroAddedEvent(incontro);
        }

        public IncontroGenerato GetIncontro(int id) =>
            _incontriGenerati[id];

        public void ClearIncontri()
        {
            _incontriGenerati.Clear();
            IncontroClearedEvent();
        }
    }
}
