﻿namespace FP.Services
{
    static public class FpRandom
    {
        static public System.Random Rnd;

        static FpRandom()
        {
            Rnd = new System.Random((int)System.DateTime.Now.Ticks);
        }
    }
}
