﻿namespace FP.Services
{
    static public class Dadi
    {
        static public int Tira(Data.Models.Dadi dadi)
        {
            int totGenerato = 0;

            for (int i = 0; i < dadi.Numero; ++i)
                totGenerato += TiraD(dadi.Dimensione);
            return dadi.Costanti + totGenerato;
        }

        static private int TiraD(int diceSize)
        {
            return FpRandom.Rnd.Next(0, diceSize) + 1;
        }
    }
}
