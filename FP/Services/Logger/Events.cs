﻿namespace FP.Services
{
    partial class Logger
    {
        public delegate void DelegateLogEvent(string message, string context, System.DateTime timestamp, Levels level);
        public event DelegateLogEvent LogEvent;
    }
}
