﻿namespace FP.Services
{
    public interface ILogger
    {
        void Log(string message, string context = null, Logger.Levels level = Logger.Levels.Info);
    }
}