﻿using System;

namespace FP.Services
{
    partial class Logger
    {
        public void Log(string message, string context = null, Levels level = Levels.Info)
        {
            var timestamp = DateTime.Now;

            _logs.Enqueue((message, context, timestamp, level));

            while (_logs.Count > Config.Backlog)
                _logs.Dequeue();

            LogEvent?.Invoke(message, context, timestamp, level);
        }
    }
}
