﻿namespace FP.Services
{
    public partial class Logger : ILogger
    {
        public Logger(Configuration pConfig)
        {
            Config = pConfig;
            _logs = new System.Collections.Generic.Queue<(string, string, System.DateTime, Levels)>(Config.Backlog);
        }
    }
}
